package com.ibeans.jbehave.ext;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


public interface JBehaveExtConstants {

    LocalDateTime LOCAL_DATE_TIME_IS_NOW = LocalDateTime.now().withMillisOfSecond(0);

    LocalDate LOCAL_DATE_IS_NOW = LocalDate.now();

}
