package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.google.common.base.Function;
import org.jbehave.core.steps.ParameterConverters;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Field;
import java.lang.reflect.Type;


public class LocalDateConverter implements ParameterConverter<LocalDate>, Function<String, LocalDate> {

    public static final String NOW = "now";
    public static final String ANY = "any";
    public static final String CURRENT_YEAR = "current year";

    public static final DateTimeFormatter DEFAULT_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");

    private final DateTimeFormatter dateFormat;

    public LocalDateConverter() {
        this(DEFAULT_FORMAT);
    }

    public LocalDateConverter(DateTimeFormatter dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = field.getType();
        return accept(type);
    }

    @Override
    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return LocalDate.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    @Override
    public LocalDate convertValue(String value, Field field) {
        Class<?> type = field.getType();
        return (LocalDate) convertValue(value, type);
    }

    @Override
    public Object convertValue(String value, Type type) {
        try {
            if (NULL.equalsIgnoreCase(value)) {
                return null;
            }
            if (NOW.equalsIgnoreCase(value)
                    || ANY.equalsIgnoreCase(value)) {
                return LocalDate.now();
            }
            if (value != null
                    && value.indexOf(CURRENT_YEAR) >= 0) {
                value = value.replace(CURRENT_YEAR, String.valueOf(
                        LocalDate.now().getYear()));
            }
            return LocalDate.parse(value, dateFormat);
        } catch (Exception cause) {
            throw new ParameterConverters.ParameterConvertionFailed("Failed to convert value "
                    + value + " with date format "
                    + dateFormat.toString(), cause);
        }
    }

    @Override
    public LocalDate apply(String value) {
        return (LocalDate) convertValue(value, LocalDate.class);
    }

}
