package com.ibeans.jbehave.ext.reporter;

import java.util.function.Function;


public class ParameterConverter implements Function<String, String> {

    private static final String PARAMETER_REGEX = "(')(.+?)(')";
    private static final String SPAN_REPLACEMENT = "'<span style=\"color:#1976D2\">$2</span>'";

    @Override
    public String apply(String value) {
        return value.replaceAll(PARAMETER_REGEX, SPAN_REPLACEMENT);
    }

}
