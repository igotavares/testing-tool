package com.ibeans.jbehave.ext.reporter;

import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.steps.StepType;

import java.text.MessageFormat;
import java.util.function.Function;


public class Step {

    private Keywords keywords;
    private String step;
    private Function<String, String> converter;

    public Step(Keywords keywords, String step, Function<String, String> converter) {
        this.keywords = keywords;
        this.step = step;
        this.converter = converter;
    }

    public String getStepWithoutTag() {
        final StepType stepType = keywords.stepTypeFor(step);
        return keywords.stepWithoutStartingWord(step, stepType);
    }

    public String getTag() {
        return keywords.startingWord(step);
    }

    public String getStep() {
        String value = getStepWithoutTag();
        if (converter != null) {
            return converter.apply(value);
        }
        return value;
    }

    public String toString(String pattern) {
        return MessageFormat.format(pattern, getTag(), getStep());
    }

}
