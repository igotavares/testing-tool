package com.ibeans.jbehave.ext.steps.parameter.converter;

import static com.ibeans.jbehave.ext.JBehaveExtConstants.*;
import com.google.common.base.Function;
import org.jbehave.core.steps.ParameterConverters;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LocalDateTimeConverter implements ParameterConverter<LocalDateTime>, Function<String, LocalDateTime> {

    public static final String DATE_REGEX = "\\d{2}/\\d{2}/\\d{4}";
    public static final String NOW = "now";
    public static final String ANY = "any";
    public static final String NOW_DATE = "current date";
    public static final String CURRENT_YEAR = "current year";
    public static final String CURRENT_MONTH = "current month";
    public static final String PATTERN_DATE_WITH_OPERATIONS = "(\\d{2}/\\d{2}/\\d{4})(\\s+\\d{2}:\\d{2}:\\d{2})*(\\s*[+-]\\d+[dMyHms])*";
    public static final String PATTERN_OPERATIONS = "([+-])(\\d+)([dMyHms])";
    public static final String PATTERN_ADDITION = "+";
    public static final String PATTERN_SUBTRACTION = "-";

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");
    public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
    public static final String PATTERN_MONTH = "M";
    public static final String PATTERN_YEAR = "y";
    public static final String PATTERN_DAY = "d";
    public static final String PATTERN_HOUR = "H";
    public static final String PATTERN_MINUTE = "m";
    public static final String PATTERN_SECOND = "s";


    private final DateTimeFormatter dateFormat;

    public LocalDateTimeConverter() {
        this(DATE_TIME_FORMAT);
    }

    public LocalDateTimeConverter(DateTimeFormatter dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return LocalDateTime.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    public Object convertValue(String value, Type type) {
        try {
            if (NULL.equalsIgnoreCase(value)) {
                return null;
            }
            if (NOW.equalsIgnoreCase(value)
                    || ANY.equalsIgnoreCase(value)) {
                return LOCAL_DATE_TIME_IS_NOW;
            }
            if (value != null) {
                if (value.indexOf(CURRENT_YEAR) >= 0) {
                    value = value.replace(CURRENT_YEAR, String.valueOf(
                            LOCAL_DATE_IS_NOW.getYear()));
                }
                if (value.indexOf(CURRENT_MONTH) >= 0) {
                    value = value.replace(CURRENT_MONTH, String.valueOf(
                            LOCAL_DATE_IS_NOW.toString("MM")));
                }
                if (value.indexOf(NOW) >= 0) {
                    value = value.replace(NOW, String.valueOf(
                            LOCAL_DATE_IS_NOW.toString("dd/MM/yyyy")));
                }
                if (value.indexOf(NOW_DATE) >= 0) {
                    value = value.replace(NOW_DATE, String.valueOf(
                            LOCAL_DATE_IS_NOW.toString("dd/MM/yyyy")));
                }
            }
            Pattern p = Pattern.compile(PATTERN_DATE_WITH_OPERATIONS);
            Matcher m = p.matcher(value);
            LocalDateTime dateTime;
            if(m.find()) {
                String date = m.group(1);
                dateTime = LocalDateTime.parse(date, DATE_FORMAT);
                String time = m.group(2);
                if(time != null) {
                    date = date.concat(time);
                    dateTime = LocalDateTime.parse(date, DATE_TIME_FORMAT);
                }
                p = Pattern.compile(PATTERN_OPERATIONS);
                m = p.matcher(value);
                while (m.find()) {
                    final String operation = m.group(1);
                    final Integer number = new Integer(m.group(2));
                    final String letterComponent = m.group(3);
                    dateTime = applyOperationDateTime(dateTime, operation, number, letterComponent);
                }
                return dateTime.withMillisOfSecond(0);
            }
            return LocalDateTime.parse(value, dateFormat).withMillisOfSecond(0);
        } catch (Exception cause) {
            throw new ParameterConverters.ParameterConvertionFailed("Failed to convert value "
                    + value + " with date time format "
                    + dateFormat.toString(), cause);
        }
    }

    private LocalDateTime applyOperationDateTime(LocalDateTime dateTime, String operation, Integer number, String letterComponent) {
        if(PATTERN_ADDITION.equals(operation)) {
            if(PATTERN_DAY.equals(letterComponent)) {
                dateTime = dateTime.plusDays(number);
            } else if(PATTERN_MONTH.equals(letterComponent)) {
                dateTime = dateTime.plusMonths(number);
            } else if(PATTERN_YEAR.equals(letterComponent)) {
                dateTime = dateTime.plusYears(number);
            } else if(PATTERN_HOUR.equals(letterComponent)) {
                dateTime = dateTime.plusHours(number);
            } else if(PATTERN_MINUTE.equals(letterComponent)) {
                dateTime = dateTime.plusMinutes(number);
            } else if(PATTERN_SECOND.equals(letterComponent)) {
                dateTime = dateTime.plusSeconds(number);
            }
        } else if(PATTERN_SUBTRACTION.equals(operation)) {
            if(PATTERN_DAY.equals(letterComponent)) {
                dateTime = dateTime.minusDays(number);
            } else if(PATTERN_MONTH.equals(letterComponent)) {
                dateTime = dateTime.minusMonths(number);
            } else if(PATTERN_YEAR.equals(letterComponent)) {
                dateTime = dateTime.minusYears(number);
            } else if(PATTERN_HOUR.equals(letterComponent)) {
                dateTime = dateTime.minusHours(number);
            } else if(PATTERN_MINUTE.equals(letterComponent)) {
                dateTime = dateTime.minusMinutes(number);
            } else if(PATTERN_SECOND.equals(letterComponent)) {
                dateTime = dateTime.minusSeconds(number);
            }
        }
        return dateTime.withMillisOfSecond(0);
    }

    private boolean isDate(String value) {
        return value != null && value.matches(DATE_REGEX);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = field.getType();
        return accept(type);
    }

    @Override
    public LocalDateTime convertValue(String value, Field field) {
        Class<?> type = field.getType();
        return (LocalDateTime) convertValue(value, type);
    }

    @Override
    public LocalDateTime apply(String value) {
        return (LocalDateTime) convertValue(value, LocalDateTime.class);
    }

}
