package com.ibeans.jbehave.ext.steps.parameter;

import com.ibeans.jbehave.ext.parameter.ExamplesTableFactory;
import com.ibeans.jbehave.ext.steps.parameter.converter.*;
import org.jbehave.core.steps.SilentStepMonitor;
import org.jbehave.core.steps.StepMonitor;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.util.Arrays.asList;


public class ParameterConverters {

    public static final StepMonitor DEFAULT_STEP_MONITOR = new SilentStepMonitor();
    public static final Locale DEFAULT_NUMBER_FORMAT_LOCAL = Locale.ENGLISH;
    public static final String DEFAULT_LIST_SEPARATOR = ",";
    public static final boolean DEFAULT_THREAD_SAFETY = true;

    private static final String NEWLINES_PATTERN = "(\n)|(\r\n)";
    private static final String SYSTEM_NEWLINE = System.getProperty("line.separator");

    private final StepMonitor monitor;
    private final List<ParameterConverter> converters;
    private final boolean threadSafe;

    /**
     * Creates a non-thread-safe instance of ParameterConverters using default
     * dependencies, a SilentStepMonitor, English as Locale and "," as list
     * separator.
     */
    public ParameterConverters() {
        this(DEFAULT_STEP_MONITOR);
    }

    /**
     * Creates a ParameterConverters using given StepMonitor
     *
     * @param monitor the StepMonitor to use
     */
    public ParameterConverters(StepMonitor monitor) {
        this(monitor, DEFAULT_NUMBER_FORMAT_LOCAL, DEFAULT_LIST_SEPARATOR, DEFAULT_THREAD_SAFETY);
    }

    /**
     * Create a ParameterConverters with given thread-safety
     *
     * @param threadSafe the boolean flag to determine if access to
     *            {@link ParameterConverter} should be thread-safe
     */
    public ParameterConverters(boolean threadSafe) {
        this(DEFAULT_STEP_MONITOR, DEFAULT_NUMBER_FORMAT_LOCAL, DEFAULT_LIST_SEPARATOR, threadSafe);
    }

    public ParameterConverters(StepMonitor monitor, Locale locale, String listSeparator, boolean threadSafe) {
        this(monitor, new ArrayList<ParameterConverter>(), threadSafe);
        this.addConverters(defaultConverters(locale, listSeparator));
    }

    private ParameterConverters(StepMonitor monitor, List<ParameterConverter> converters, boolean threadSafe) {
        this.monitor = monitor;
        this.threadSafe = threadSafe;
        this.converters = (threadSafe ? new CopyOnWriteArrayList<ParameterConverter>(converters)
                : new ArrayList<ParameterConverter>(converters));
    }

    protected ParameterConverter[] defaultConverters(Locale locale, String listSeparator) {
        String escapedListSeparator = escapeRegexPunctuation(listSeparator);
        ExamplesTableFactory tableFactory = new ExamplesTableFactory(this);
        ParameterConverter[] defaultConverters = { new BooleanConverter(),
                new NumberConverter(NumberFormat.getInstance(locale)),
                new NumberListConverter(NumberFormat.getInstance(locale), escapedListSeparator),
                new StringListConverter(escapedListSeparator),
                new LocalDateConverter(),
                new DateTimeConverter(),
                new LocalDateTimeConverter(),
                new DateConverter(),
                new EnumConverter(),
                new EnumListConverter(),
                new ExamplesTableConverter(tableFactory),
                new ExamplesTableParametersConverter(tableFactory) };
        return defaultConverters;
    }

    private String escapeRegexPunctuation(String matchThis) {
        return matchThis.replaceAll("([\\[\\]\\{\\}\\?\\^\\.\\*\\(\\)\\+\\\\])", "\\\\$1");
    }

    public ParameterConverters addConverters(ParameterConverter... converters) {
        return addConverters(asList(converters));
    }

    public ParameterConverters addConverters(List<ParameterConverter> converters) {
        this.converters.addAll(0, converters);
        return this;
    }

    public Object convert(String value, Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        for (ParameterConverter converter : converters) {
            if (converter.accept(field)) {
                Object converted = converter.convertValue(value, field);
                monitor.convertedValueOfType(value, type, converted, converter.getClass());
                return converted;
            }
        }

        if (type == String.class) {
            return replaceNewlinesWithSystemNewlines(value);
        }

        throw new ParameterConvertionFailed("No parameter converter for " + type);
    }

    public Object convert(String value, Type type) {
        for (ParameterConverter converter : converters) {
            if (converter.accept(type)) {
                Object converted = converter.convertValue(value, type);
                monitor.convertedValueOfType(value, type, converted, converter.getClass());
                return converted;
            }
        }

        if (type == String.class) {
            return replaceNewlinesWithSystemNewlines(value);
        }

        throw new ParameterConvertionFailed("No parameter converter for " + type);
    }

    private Object replaceNewlinesWithSystemNewlines(String value) {
        return value.replaceAll(NEWLINES_PATTERN, SYSTEM_NEWLINE);
    }

    public ParameterConverters newInstanceAdding(ParameterConverter converter) {
        List<ParameterConverter> convertersForNewInstance = new ArrayList<ParameterConverter>(converters);
        convertersForNewInstance.add(converter);
        return new ParameterConverters(monitor, convertersForNewInstance, threadSafe);
    }



}
