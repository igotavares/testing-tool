package com.ibeans.jbehave.ext.support;


public abstract class Result<E> {

    enum Type {SUCCESS, FAULT}

    private Type type;
    protected E value;

    public Result(Type type, E value) {
        this.type = type;
        this.value = value;
    }

    public boolean isSuccessful() {
        return Type.SUCCESS.equals(type);
    }

    public boolean hasValue() {
        return value != null;
    }

    public E getValue() {
        return value;
    }

}
