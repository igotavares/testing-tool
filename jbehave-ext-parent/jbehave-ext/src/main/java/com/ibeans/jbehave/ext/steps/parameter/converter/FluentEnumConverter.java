package com.ibeans.jbehave.ext.steps.parameter.converter;

import java.lang.reflect.Field;


public class FluentEnumConverter extends org.jbehave.core.steps.ParameterConverters.FluentEnumConverter
        implements ParameterConverter {

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return convertValue(value, type);
    }

}
