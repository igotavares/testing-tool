package com.ibeans.jbehave.ext.reporter;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;

public class Converters implements Function<String, String> {

    private Collection<Function<String, String>> converters;

    public Converters(Function<String, String>... converters) {
        this.converters = Arrays.asList(converters);
    }

    public Converters(Collection<Function<String, String>> converters) {
        this.converters = converters;
    }

    @Override
    public String apply(String value) {
        String convertedValue = value;
        for (Function<String, String> converter : converters) {
            convertedValue = converter.apply(convertedValue);
        }
        return convertedValue;
    }
}
