package com.ibeans.jbehave.ext.support;

import java.util.List;


public class Fault extends Result<List<String>> {

    public Fault(List<String> messages) {
        super(Type.FAULT, messages);
    }

}
