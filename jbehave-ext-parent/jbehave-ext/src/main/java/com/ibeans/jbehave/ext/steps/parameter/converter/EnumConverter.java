package com.ibeans.jbehave.ext.steps.parameter.converter;

import java.lang.reflect.Field;


public class EnumConverter extends org.jbehave.core.steps.ParameterConverters.EnumConverter implements ParameterConverter {

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getType();
        return convertValue(value, type);
    }

}
