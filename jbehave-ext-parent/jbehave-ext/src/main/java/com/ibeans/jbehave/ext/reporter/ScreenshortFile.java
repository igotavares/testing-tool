package com.ibeans.jbehave.ext.reporter;

import org.joda.time.LocalDateTime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


public class ScreenshortFile {

    private final static String PNG_EXTENSION = ".png";
    private final static String HTML_EXTENSION = ".html";
    private final static String IMAGES_PATH = "images";
    private final static String NAME_PATTERN = "ddMMyyyyHHmmssSSS";

    private String sourcePage;
    private byte[] screenshot;
    private String extension;
    private String output;
    private String path;

    public ScreenshortFile(String output, String sourcePage, byte[] screenshot) {
        this(sourcePage, screenshot, output, IMAGES_PATH, PNG_EXTENSION);
    }

    public ScreenshortFile(String sourcePage, byte[] screenshot, String output, String path, String extension) {
        this.sourcePage = sourcePage;
        this.screenshot = screenshot;
        this.output = output;
        this.path = path;
        this.extension = extension;
    }

    public Screenshort genereteFile() {
        File dir = createDir();
        genereteDir(dir);
        String name = genereteNameFile();
        writeSourcePage(dir, name);
        writeScreenshot(dir, name);
        return new Screenshort(path, name + PNG_EXTENSION);
    }

    private void writeSourcePage(File dir, String name) {
        if (sourcePage != null) {
            File sourcePageFile = new File(dir, name + HTML_EXTENSION);
            write(sourcePage.getBytes(), sourcePageFile);
        }
    }

    private void writeScreenshot(File dir, String name) {
        File sourcePageFile = new File(dir, name + PNG_EXTENSION);
        write(screenshot, sourcePageFile);
    }

    private void write(byte[] bytes ,File file) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        } finally {
            close(fileOutputStream);
        }
    }

    private void close(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException cause) {
                throw new RuntimeException(cause);
            }
        }
    }

    protected void genereteDir(File dir) {
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    protected File createDir() {
        return new File(output + File.separator + path);
    }

    protected String genereteNameFile() {
        return LocalDateTime.now()
                    .toString(NAME_PATTERN);
    }

}
