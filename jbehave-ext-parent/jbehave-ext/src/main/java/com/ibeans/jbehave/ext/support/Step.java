package com.ibeans.jbehave.ext.support;


public interface Step<TYPE> {

    TYPE getIdentifier();

}
