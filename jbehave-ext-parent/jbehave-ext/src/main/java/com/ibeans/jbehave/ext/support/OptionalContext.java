package com.ibeans.jbehave.ext.support;

import java.util.Optional;


public class OptionalContext<E> implements Cleaned {

    private ThreadLocal<Optional<E>> value;

    public OptionalContext() {
        this.value = new ThreadLocal<>();
        this.value.set(Optional.empty());
    }

    public void set(Optional<E> value) {
        this.value.set(value);
    }

    public void set(E value) {
        set(Optional.ofNullable(value));
    }

    public E get() {
        return this.value.get().orElse(null);
    }

    public E orElseThrow(String message) {
        return this.value.get()
                .orElseThrow(() -> new IllegalArgumentException(message));
    }

    public void clear() {
        this.value.set(Optional.empty());
    }
}
