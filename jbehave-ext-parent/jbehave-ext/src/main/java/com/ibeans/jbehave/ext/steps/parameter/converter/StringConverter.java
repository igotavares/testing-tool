package com.ibeans.jbehave.ext.steps.parameter.converter;

import java.lang.reflect.Field;
import java.lang.reflect.Type;


public class StringConverter implements ParameterConverter {

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getType();
        return convertValue(value, type);
    }

    @Override
    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return String.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    @Override
    public Object convertValue(String value, Type type) {
        if (NULL.equalsIgnoreCase(value)){
            return null;
        }
        return value;
    }
}
