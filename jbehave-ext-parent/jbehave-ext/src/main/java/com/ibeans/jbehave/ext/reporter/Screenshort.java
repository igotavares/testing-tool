package com.ibeans.jbehave.ext.reporter;


public class Screenshort {

    private final static String SEPARATOR = "/";

    private String path;
    private String name;

    public Screenshort(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public String getImagePath() {
        return path + SEPARATOR + name;
    }

}
