package com.ibeans.jbehave.ext.steps.parameter.converter;

import org.jbehave.core.steps.ParameterConverters;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Arrays.asList;


public class NumberConverter extends org.jbehave.core.steps.ParameterConverters.NumberConverter implements ParameterConverter {

    public static final Locale DEFAULT_NUMBER_FORMAT_LOCAL = Locale.ENGLISH;

    private static final String NULL = "null";
    private static final String ZERO = String.valueOf("0");

    private static List<Class<?>> primitiveTypes = asList(new Class<?>[] { byte.class, short.class, int.class,
            float.class, long.class, double.class });

    private final NumberFormat numberFormat;
    private ThreadLocal<NumberFormat> threadLocalNumberFormat = new ThreadLocal<NumberFormat>();

    public NumberConverter() {
        this(NumberFormat.getInstance(DEFAULT_NUMBER_FORMAT_LOCAL));
    }

    public NumberConverter(NumberFormat numberFormat) {
        synchronized (this) {
            this.numberFormat = numberFormat;
            this.threadLocalNumberFormat.set((NumberFormat) this.numberFormat.clone());
        }
    }

    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return Number.class.isAssignableFrom((Class<?>) type) || primitiveTypes.contains(type);
        }
        return false;
    }

    public Object convertValue(String value, Type type) {
        if (NULL.equalsIgnoreCase(value)){
            if (primitiveTypes.contains(type)) {
                return convertValue(ZERO, type);
            }
            return null;
        }
        try {
            Number n = numberFormat().parse(value);
            if (type == Byte.class || type == byte.class) {
                return n.byteValue();
            } else if (type == Short.class || type == short.class) {
                return n.shortValue();
            } else if (type == Integer.class || type == int.class) {
                return n.intValue();
            } else if (type == Float.class || type == float.class) {
                return n.floatValue();
            } else if (type == Long.class || type == long.class) {
                return n.longValue();
            } else if (type == Double.class || type == double.class) {
                return n.doubleValue();
            } else if (type == BigInteger.class) {
                return BigInteger.valueOf(n.longValue());
            } else if (type == BigDecimal.class) {
                return new BigDecimal(canonicalize(value));
            } else if (type == AtomicInteger.class) {
                return new AtomicInteger(Integer.parseInt(value));
            } else if (type == AtomicLong.class) {
                return new AtomicLong(Long.parseLong(value));
            } else {
                return n;
            }
        } catch (NumberFormatException e) {
            throw new ParameterConverters.ParameterConvertionFailed(value, e);
        } catch (ParseException e) {
            throw new ParameterConverters.ParameterConvertionFailed(value, e);
        }
    }

    /**
     * Return NumberFormat instance with preferred locale threadsafe
     *
     * @return A threadlocal version of original NumberFormat instance
     */
    private NumberFormat numberFormat() {
        if (threadLocalNumberFormat.get() == null) {
            synchronized (this) {
                threadLocalNumberFormat.set((NumberFormat) numberFormat.clone());
            }
        }
        return threadLocalNumberFormat.get();
    }

    /**
     * Canonicalize a number representation to a format suitable for the
     * {@link BigDecimal(String)} constructor, taking into account the
     * settings of the currently configured DecimalFormat.
     *
     * @param value a localized number value
     * @return A canonicalized string value suitable for consumption by
     *         BigDecimal
     */
    private String canonicalize(String value) {
        char decimalPointSeparator = '.'; // default
        char minusSign = '-'; // default
        String rxNotDigits = "[\\.,]";
        StringBuilder builder = new StringBuilder(value.length());

        // override defaults according to numberFormat's settings
        if (numberFormat() instanceof DecimalFormat) {
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) numberFormat()).getDecimalFormatSymbols();
            minusSign = decimalFormatSymbols.getMinusSign();
            decimalPointSeparator = decimalFormatSymbols.getDecimalSeparator();
        }

        value = value.trim();
        int decimalPointPosition = value.lastIndexOf(decimalPointSeparator);
        int firstDecimalPointPosition = value.indexOf(decimalPointSeparator);

        if (firstDecimalPointPosition != decimalPointPosition) {
            throw new NumberFormatException("Invalid format, more than one decimal point has been found.");
        }

        boolean isNegative = value.charAt(0) == minusSign;

//        if (isNegative) {
//            builder.append('-'); // fixed "-" for BigDecimal constructor
//        }

        if (decimalPointPosition != -1) {
            String sf = value.substring(0, decimalPointPosition).replaceAll(rxNotDigits, "");
            String dp = value.substring(decimalPointPosition + 1).replaceAll(rxNotDigits, "");

            builder.append(sf);
            builder.append('.'); // fixed "." for BigDecimal constructor
            builder.append(dp);

        } else {
            builder.append(value.replaceAll(rxNotDigits, ""));
        }
        return builder.toString();
    }


    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getType();
        return convertValue(value, type);
    }

}
