package com.ibeans.jbehave.ext.support;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class MapContext implements Cleaned {

    private final String key;
    private final ThreadLocal<Map<Object, IteratorContext<Map<String,Object>>>> entities;
    private final ThreadLocal<Optional<Map<String,Object>>> current;

    public MapContext(String key) {
        this.key = key;
        this.entities = new ThreadLocal<>();
        this.current = new ThreadLocal<>();
        this.current.set(Optional.empty());
        this.entities.set(new LinkedHashMap<>());
    }

    public void set(Collection<Map<String, Object>> values) {
        Map<Object, List<Map<String,Object>>> separateds = separate(values);
        this.entities.set(separateds.keySet().stream()
                .collect(Collectors
                        .toMap(Function.identity(), keyValue -> new IteratorContext<>(separateds.get(keyValue)))));
    }

    private Map<Object, List<Map<String,Object>>> separate(Collection<Map<String, Object>> values) {
        Map<Object, List<Map<String,Object>>> separateds = new LinkedHashMap<>();
        for (Map<String, Object> value : values) {
            Object keyValue = value.get(key);
            separateds.putIfAbsent(keyValue, new ArrayList<>());
            separateds.get(keyValue).add(value);
        }
        return separateds;
    }

    public Map<String, Object> next(String key) {
        Optional<Map<String, Object>> value = Optional.ofNullable(key)
                .map(this::getContext)
                .map(IteratorContext::next);

        current.set(value);

        return value.orElse(null);
    }

    public Map<String, Object> get() {
        return current.get()
                .orElse(null);
    }

    public Map<String, Object> orElseThrow(String message) {
        return current.get()
                .orElseThrow(() -> new IllegalArgumentException(message));
    }

    public Map<String, Object> get(String key) {
        return getOptional(key)
                .orElse(null);
    }

    public Map<String, Object> orElseThrow(String key, String message) {
        return getOptional(key)
                .orElseThrow(() -> new IllegalArgumentException(message));
    }

    protected Optional<Map<String, Object>> getOptional(String key) {
        return Optional.ofNullable(key)
                .map(this::getContext)
                .map(IteratorContext::get);
    }

    private IteratorContext<Map<String,Object>> getContext(String key) {
        return Optional.ofNullable(entities)
                .map(ThreadLocal::get)
                .map(map -> map.get(key))
                .orElse(null);
    }

    public Optional<Map<String, Object>> get(String key, Predicate<Map<String, Object>> search) {
        return Optional.ofNullable(getContext(key))
                .map(context -> context.getValues())
                .map(values -> values
                        .stream()
                        .filter(search)
                        .findFirst()
                        .orElse(null));
    }

    @Override
    public void clear() {
        this.entities.remove();
        this.current.set(Optional.empty());
    }

}
