package com.ibeans.jbehave.ext.steps.parameter.converter;

import org.jbehave.core.steps.InjectableStepsFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class MethodReturningConverter extends org.jbehave.core.steps.ParameterConverters.MethodReturningConverter
        implements ParameterConverter {

    public MethodReturningConverter(Method method, Object instance) {
        super(method, instance);
    }

    public MethodReturningConverter(Method method, Class<?> stepsType, InjectableStepsFactory stepsFactory) {
        super(method, stepsType, stepsFactory);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return convertValue(value, type);
    }

}
