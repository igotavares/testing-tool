package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.google.common.base.Function;
import org.apache.commons.lang.BooleanUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Type;


public class BooleanConverter
        extends org.jbehave.core.steps.ParameterConverters.BooleanConverter
        implements ParameterConverter<Boolean>, Function<String, Boolean> {

    private static final String DO = "DO";
    private static final String DONT = "DONT";

    public BooleanConverter() {
    }

    public BooleanConverter(String trueValue, String falseValue) {
        super(trueValue, falseValue);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    @Override
    public Boolean convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getType();
        return (Boolean) convertValue(value, type);
    }

    @Override
    public Object convertValue(String value, Type type) {
        if (NULL.equalsIgnoreCase(value)){
            return null;
        }

        if (DO.equalsIgnoreCase(value) || DONT.equalsIgnoreCase(value) ){
            return  Boolean.valueOf(BooleanUtils.toBoolean(value.toUpperCase(), DO, DONT));
        }
        return super.convertValue(value, type);
    }

    @Override
    public Boolean apply(String value) {
        return (Boolean) convertValue(value, Boolean.class);
    }
}
