package com.ibeans.jbehave.ext.reporter;

import java.util.function.Function;


public class SeparatorConverter implements Function<String, String> {

    private static final String SEPARATOR_REGEX = System.getProperty("line.separator");
    private static final String BR_REPLACEMENT = "<br />";

    @Override
    public String apply(String value) {
        return value.replaceAll(SEPARATOR_REGEX, BR_REPLACEMENT);
    }

}
