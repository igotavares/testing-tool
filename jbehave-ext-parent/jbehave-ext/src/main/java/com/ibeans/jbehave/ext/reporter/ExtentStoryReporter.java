package com.ibeans.jbehave.ext.reporter;

import com.ibeans.jbehave.ext.exception.PageException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.ExceptionInfo;
import com.relevantcodes.extentreports.utils.ExceptionUtil;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.failures.BeforeOrAfterFailed;
import org.jbehave.core.failures.UUIDExceptionWrapper;
import org.jbehave.core.model.*;
import org.jbehave.core.reporters.StoryReporter;
import org.jbehave.core.steps.StepType;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by igo.tavares on 07/02/2016.
 */
public class ExtentStoryReporter implements StoryReporter {

    private static final String BEFORE_STORIES = "BeforeStories";
    private static final String AFTER_STORIES = "AfterStories";
    private final Keywords keywords;
    private ExtentReports extentReports;
    private ExtentTest storyExtentTest;
    private ExtentTest scenarioExtentTest;
    private ExtentTest exampleExtentTest;
    private String path;
    private ThreadLocal<Boolean> reportFailureTrace = new ThreadLocal<Boolean>();
    private ThreadLocal<Boolean> compressFailureTrace = new ThreadLocal<Boolean>();

    public ExtentStoryReporter(Keywords keywords, ExtentReports extentReports, String path) {
        this.keywords = keywords;
        this.extentReports = extentReports;
        this.path = path;
    }

    public ExtentStoryReporter doReportFailureTrace(boolean reportFailureTrace) {
        this.reportFailureTrace.set(Boolean.valueOf(reportFailureTrace));
        return this;
    }

    public ExtentStoryReporter doCompressFailureTrace(boolean compressFailureTrace) {
        this.compressFailureTrace.set(Boolean.valueOf(compressFailureTrace));
        return this;
    }

    public void storyNotAllowed(Story story, String s) {
        System.out.println("storyNotAllowed");
    }

    public void storyCancelled(Story story, StoryDuration storyDuration) {
        System.out.println("storyCancelled");
    }

    public void beforeStory(Story story, boolean storyExecuted) {
        if(!(BEFORE_STORIES.endsWith(story.getName()) || AFTER_STORIES.endsWith(story.getName()))) {
            this.storyExtentTest = extentReports.startTest(story.getDescription().asString());
        }
    }

    public void afterStory(boolean storyExecuted) {
        if (storyExtentTest != null) {
            extentReports.endTest(this.storyExtentTest);
            extentReports.flush();
        }
    }

    public void narrative(Narrative narrative) {
        if(!narrative.isEmpty()) {
            if(!narrative.isAlternative()) {
                this.storyExtentTest.log(LogStatus.INFO, Html.NARRATIVE.format(this.keywords.narrative(), this.keywords.inOrderTo(), narrative.inOrderTo(), this.keywords.asA(), narrative.asA(), this.keywords.iWantTo(), narrative.iWantTo()));
            } else {
                this.storyExtentTest.log(LogStatus.INFO, Html.NARRATIVE.format(this.keywords.narrative(), this.keywords.asA(), narrative.asA(), this.keywords.iWantTo(), narrative.iWantTo(), this.keywords.soThat(), narrative.soThat()));
            }
        }
    }

    public void lifecyle(Lifecycle lifecycle) {
        System.out.println("lifecyle");
    }

    public void scenarioNotAllowed(Scenario scenario, String s) {
        System.out.println("scenarioNotAllowed");
    }

    public void beforeScenario(String scenario) {
        this.scenarioExtentTest = extentReports.startTest(Html.SCENARIO.format(keywords.scenario(), scenario));
        this.storyExtentTest.appendChild(this.scenarioExtentTest);
    }

    public void scenarioMeta(Meta meta) {
        System.out.println("scenarioMeta");
    }

    public void afterScenario() {
        extentReports.endTest(this.scenarioExtentTest);
    }

    public void givenStories(GivenStories givenStories) {
        System.out.println("givenStories");
    }

    public void givenStories(List<String> list) {
        System.out.println("givenStories");
    }

    public void beforeExamples(List<String> steps, ExamplesTable examplesTable) {
        for (String step : steps) {
            this.scenarioExtentTest.log(LogStatus.INFO, Html.STEP.stepFormat(keywords, step));
        }
        this.scenarioExtentTest.log(LogStatus.INFO,
                Html.EXAMPLES.format(keywords.examplesTable(), asTable(examplesTable)));
    }

    private String asTable(ExamplesTable examplesTable) {
        StringBuilder tableHtml = new StringBuilder();
        StringBuilder headerHtml = new StringBuilder();
        StringBuilder bodyHtml = new StringBuilder();
        for (String header : examplesTable.getHeaders()) {
            headerHtml.append(keywords.examplesTableHeaderSeparator());
            headerHtml.append(header);
            headerHtml.append(keywords.examplesTableHeaderSeparator());
        }

        for (Map<String, String> row : examplesTable.getRows()) {
            bodyHtml.append(Html.BR.getTag());
            StringBuilder rowHtml = new StringBuilder();
            for (String header : examplesTable.getHeaders()) {
                rowHtml.append(keywords.examplesTableValueSeparator());
                rowHtml.append(row.get(header));
            }
            rowHtml.append(keywords.examplesTableValueSeparator());
            bodyHtml.append(rowHtml);
        }

        tableHtml.append(Html.SPAN.format(headerHtml));
        tableHtml.append(bodyHtml);
        return tableHtml.toString();
    }

    public void example(Map<String, String> example) {
        if (exampleExtentTest != null) {
            extentReports.endTest(exampleExtentTest);
        }
        exampleExtentTest = extentReports.startTest(Html.EXAMPLE.format(keywords.examplesTableRow(), example));
        scenarioExtentTest.appendChild(exampleExtentTest);
    }

    public void afterExamples() {
        if (exampleExtentTest != null) {
            extentReports.endTest(exampleExtentTest);
            exampleExtentTest = null;
        }
    }

    public ExtentTest getExtentTest() {
        if (exampleExtentTest != null) {
            return exampleExtentTest;
        }
        return scenarioExtentTest;
    }

    public void beforeStep(String step) {
        System.out.println("beforeStep");
    }

    public void successful(String step) {
        getExtentTest().log(LogStatus.PASS, Html.STEP.stepFormat(keywords, step));
    }

    public void ignorable(String step) {
        getExtentTest().log(LogStatus.SKIP, Html.STEP_SKIP.stepFormat(keywords, step));
    }

    public void pending(String step) {
        getExtentTest().log(LogStatus.SKIP, Html.STEP_SKIP.stepFormat(keywords, step));
    }

    public void notPerformed(String step) {
        getExtentTest().log(LogStatus.SKIP, Html.STEP_SKIP.stepFormat(keywords, step));
    }

    public void failed(String step, Throwable stepFailure) {
        if (stepFailure instanceof UUIDExceptionWrapper) {
            Throwable cause = ((UUIDExceptionWrapper) stepFailure).getCause();
            if (cause instanceof BeforeOrAfterFailed) {
                getExtentTest().log(LogStatus.ERROR, step, cause);
            } else if (cause instanceof PageException) {
                String url = ((PageException) cause).getUrl();
                String sourcePage = ((PageException) cause).getSourcePage();
                byte[] page = ((PageException) cause).getScreenshot();

                Screenshort screenshort = new ScreenshortFile(path, sourcePage, page)
                        .genereteFile();

                getExtentTest().log(LogStatus.ERROR, Html.STEP_PAGE_ERROR.stepFormat(keywords,
                        step,
                        url,
                        getExtentTest().addScreenCapture(screenshort.getImagePath()),
                        cause.getCause()));
            } else if (cause instanceof AssertionError) {
                getExtentTest().log(LogStatus.FAIL, Html.STEP_FAIL.stepFormat(keywords, stepFailure.getMessage(), cause));
            } else {
                getExtentTest().log(LogStatus.ERROR, Html.STEP_ERROR.stepError(keywords, step, cause));
            }
        } else {
            throw new ClassCastException(stepFailure + " should be an instance of UUIDExceptionWrapper");
        }
    }

    public void failedOutcomes(String s, OutcomesTable outcomesTable) {
        System.out.println("failedOutcomes");
    }

    public void restarted(String s, Throwable throwable) {
        System.out.println("restarted");
    }

    public void restartedStory(Story story, Throwable throwable) {
        System.out.println("restartedStory");
    }

    public void dryRun() {
        System.out.println("dryRun");
    }

    public void pendingMethods(List<String> list) {
        System.out.println("pendingMethods");
    }

    enum Html {
        START_PARAMETER("<span style=\"color:#00b300;font-weight:bold\">"),
        END_PARAMETER("</span>"),
        STEP("<span style=\"color:#5ba1cf;font-weight:bold\">{0}</span> {1}"),
        EXAMPLES("<span style=\"color:#5ba1cf;font-weight:bold\">{0}</span><br />{1}"),
        EXAMPLE("<span style=\"color:#5ba1cf;font-weight:bold\">{0}</span> {1}"),
        BR("<br />"),
        SPAN("<span style=\"font-weight:bold\">{0}</span>"),
        STEP_SKIP("<span style=\"color:#90a4ae;font-weight:bold\">{0}</span> {1}"),
        STEP_PAGE_ERROR("<span style=\"color:#E57373;font-weight:bold\">{0}</span> {1} <span style=\"color:#E57373;font-weight:bold\">(ERROR)</span><br /><span style=\"color:#E57373;font-weight:bold\">URL:{2}</span><br />{3}<br /><pre>{4}</pre>"),
        STEP_ERROR("<span style=\"color:#E57373;font-weight:bold\">{0}</span> {1} <span style=\"color:#E57373;font-weight:bold\">(ERROR)</span><br /><pre>{2}</pre>"),
        STEP_FAIL("<span style=\"color:#E57373;font-weight:bold\">{0}</span> {1} <span style=\"color:#E57373;font-weight:bold\">(FAILED)</span><br /><span style=\"color:#E57373;font-weight:bold\">{2}</span>"),
        SCENARIO("<span style=\"color:#5ba1cf;font-weight:bold\">{0}</span> {1}"),
        NARRATIVE("<span style=\"color:#5ba1cf;font-weight:bold\">{0}</span><br /><span style=\"color:#5ba1cf;font-weight:bold\">{1}</span> {2}<br /><span style=\"color:#5ba1cf;font-weight:bold\">{3}</span> {4}<br /><span style=\"color:#5ba1cf;font-weight:bold\">{5}</span> {6}");

        private final String tag;

        Html(String tag) {
            this.tag = tag;
        }

        public String getTag() {
            return this.tag;
        }

        public String format(Object... parameters) {
            return MessageFormat.format(tag, parameters);
        }

        public String format(Step step) {
            return step.toString(tag);
        }

        public String stepFormat(Keywords keywords, String value) {
            Converters converters = new Converters(new SeparatorConverter(), new ParameterConverter());

            Step step = new Step(keywords, value, converters);

            return step.toString(tag);
        }

        public String stepError(Keywords keywords, String step, Throwable cause) {
            Converters converters = new Converters(new SeparatorConverter(), new ParameterConverter());

            final StepType stepType = keywords.stepTypeFor(step);
            final String stepWithouStartingWord = keywords.stepWithoutStartingWord(step, stepType);
            final String stepTag = keywords.startingWord(step);

            ExceptionInfo exceptionInfo = ExceptionUtil.createExceptionInfo(cause , null);

            return format(stepTag, converters.apply(stepWithouStartingWord),
                    exceptionInfo.getStackTrace());
        }

        public String stepFormat(Keywords keywords, String step, String url, String page, Throwable cause) {
            Converters converters = new Converters(new SeparatorConverter(), new ParameterConverter());

            final StepType stepType = keywords.stepTypeFor(step);
            final String stepWithouStartingWord = keywords.stepWithoutStartingWord(step, stepType);
            final String stepTag = keywords.startingWord(step);

            ExceptionInfo exceptionInfo = ExceptionUtil.createExceptionInfo(cause , null);
            return format(stepTag, converters.apply(stepWithouStartingWord), url,
                    page, exceptionInfo.getStackTrace());
        }

        public String stepFormat(Keywords keywords, String step, Throwable cause) {
            Converters converters = new Converters(new SeparatorConverter(), new ParameterConverter());

            final StepType stepType = keywords.stepTypeFor(step);
            final String stepWithouStartingWord = keywords.stepWithoutStartingWord(step, stepType);
            final String stepTag = keywords.startingWord(step);

            return format(stepTag, converters.apply(stepWithouStartingWord),
                    cause.toString().replace("<", "&lt;").replace(">", "&gt;"));
        }

    }

}
