package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.google.common.base.Function;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.lang.reflect.Field;
import java.lang.reflect.Type;


public class MatcherConverter implements ParameterConverter<Matcher<?>>, Function<String, Matcher<?>> {

    public static final String ANY = "any";

    @Override
    public boolean accept(Field type) {
        if (type != null) {
            return accept(type.getType());
        }
        return false;
    }

    @Override
    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return Matcher.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    @Override
    public Matcher<?> convertValue(String value, Field field) {
        return null;
    }

    @Override
    public Matcher<?> apply(String value) {
        return null;
    }

    @Override
    public Object convertValue(String value, Type type) {
        if (ANY.equalsIgnoreCase(value)) {
            return Matchers.anything();
        }
        return Matchers.equalTo((Object) value);
    }
}
