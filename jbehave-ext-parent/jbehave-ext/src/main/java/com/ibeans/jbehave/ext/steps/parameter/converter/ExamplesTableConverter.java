package com.ibeans.jbehave.ext.steps.parameter.converter;


import com.ibeans.jbehave.ext.parameter.ExamplesTableFactory;
import org.jbehave.core.model.ExamplesTable;

import java.lang.reflect.Field;
import java.lang.reflect.Type;


public class ExamplesTableConverter implements ParameterConverter {

    private final ExamplesTableFactory factory;

    public ExamplesTableConverter() {
        this(new ExamplesTableFactory());
    }

    public ExamplesTableConverter(ExamplesTableFactory factory) {
        this.factory = factory;
    }

    public Object convertValue(String value, Type type) {
        return factory.createExamplesTable(value);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return convertValue(value, type);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return ExamplesTable.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

}
