package com.ibeans.jbehave.ext.support;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;


public class ListContext implements Cleaned {

    private final ThreadLocal<List<Map<String, Object>>> values;

    public ListContext() {
        this.values = new ThreadLocal<>();
        this.values.set(new ArrayList<>());
    }

    public void set(List<Map<String, Object>> values) {
        this.values.set(values);
    }

    public Optional<Map<String, Object>> findFirst(Predicate<Map<String, Object>> search) {
        return this.values.get()
                .stream()
                .filter(search)
                .findFirst();
    }

    @Override
    public void clear() {
        this.values.set(new ArrayList<>());
    }
}
