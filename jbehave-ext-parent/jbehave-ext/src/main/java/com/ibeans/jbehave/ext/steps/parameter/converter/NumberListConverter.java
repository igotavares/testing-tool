package com.ibeans.jbehave.ext.steps.parameter.converter;

import java.lang.reflect.Field;
import java.text.NumberFormat;


public class NumberListConverter  extends org.jbehave.core.steps.ParameterConverters.NumberListConverter implements ParameterConverter {

    public NumberListConverter() {
    }

    public NumberListConverter(NumberFormat numberFormat, String valueSeparator) {
        super(numberFormat, valueSeparator);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getType();
        return convertValue(value, type);
    }

}
