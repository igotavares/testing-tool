package com.ibeans.jbehave.ext.support;

import java.util.*;


public class IteratorContext<E> implements Cleaned {

    private ThreadLocal<Optional<E>> current;
    private ThreadLocal<Collection<E>> values;
    private ThreadLocal<Iterator<E>> entities;

    public IteratorContext(Collection<E> entities) {
        this();
        set(entities);
    }

    public IteratorContext() {
        this.current = new ThreadLocal<>();
        this.values = new ThreadLocal<>();
        this.entities = new ThreadLocal<>();
    }

    public boolean has() {
        return this.entities.get() != null;
    }

    public E next() {
        Iterator<E> entities = this.entities.get();
        if (entities != null && entities.hasNext()) {
            E current = entities.next();
            this.current.set(Optional.ofNullable(current));
            return current;
        }
        return null;
    }

    public E get() {
        Optional<E> value = current.get();
        if (value != null) {
            return value.get();
        }
        return null;
    }

    public E orElseThrow(String message) {
        Optional<E> value = current.get();
        if (value != null) {
            return value.orElseThrow(() -> new IllegalArgumentException(message));
        }
        return null;
    }

    public void set(Collection<E> entities) {
        if (entities != null) {
            this.values.set(entities);
            this.entities.set(entities.iterator());
        }
    }

    public void clear() {
        this.entities.remove();
        this.current.remove();
        this.values.remove();
    }

    public Collection<E> getValues() {
        return this.values.get();
    }
}
