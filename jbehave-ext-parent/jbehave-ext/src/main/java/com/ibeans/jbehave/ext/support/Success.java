package com.ibeans.jbehave.ext.support;

public class Success<E> extends Result<E> {

    public Success(E value) {
        super(Type.SUCCESS, value);
    }

}
