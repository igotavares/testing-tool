package com.ibeans.jbehave.ext.support;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;


public class SimpleContext<E> implements Cleaned {

    private ThreadLocal<List<E>> entities;

    public SimpleContext() {
        this.entities = new ThreadLocal<>();
    }

    public E get() {
        List<E> entities = this.entities.get();
        if (entities != null) {
            return entities.stream()
                    .reduce((a, b) -> b)
                    .orElse(null);
        }
        return null;
    }

    public E get(Predicate<E> predicate) {
        List<E> values = this.entities.get();
        if (values != null) {
            return values
                    .stream()
                    .filter(predicate)
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    public void set(E entity) {
        List<E> entities = this.entities.get();
        if (entities == null) {
            entities = new ArrayList<E>();
            this.entities.set(entities);
        }
        entities.add(entity);
    }

    public void clear() {
        this.entities.remove();
    }



}
