package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.google.common.base.Function;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.Date;


public class DateConverter extends org.jbehave.core.steps.ParameterConverters.DateConverter
        implements ParameterConverter<Date>, Function<String, Date> {

    public DateConverter() {
    }

    public DateConverter(DateFormat dateFormat) {
        super(dateFormat);
    }

    @Override
    public Date convertValue(String value, Field field) {
        Class<?> type = field.getType();
        return (Date) convertValue(value, type);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    @Override
    public Object convertValue(String value, Type type) {
        if (NULL.equalsIgnoreCase(value)){
            return null;
        }
        return super.convertValue(value, type);
    }

    @Override
    public Date apply(String value) {
        return (Date) convertValue(value, Date.class);
    }
}
