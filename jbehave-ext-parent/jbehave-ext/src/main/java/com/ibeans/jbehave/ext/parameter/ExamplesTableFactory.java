package com.ibeans.jbehave.ext.parameter;

import com.ibeans.jbehave.ext.steps.parameter.ParameterConverters;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.ResourceLoader;
import org.jbehave.core.model.TableTransformers;

import static org.apache.commons.lang.StringUtils.isBlank;

public class ExamplesTableFactory {

    private Keywords keywords;
    private final ResourceLoader resourceLoader;
    private final ParameterConverters parameterConverters;
    private final TableTransformers tableTransformers;

    public ExamplesTableFactory() {
        this(new LocalizedKeywords());
    }

    public ExamplesTableFactory(Keywords keywords) {
        this(keywords, new LoadFromClasspath(), new ParameterConverters(), new TableTransformers());
    }

    public ExamplesTableFactory(ResourceLoader resourceLoader) {
        this(new LocalizedKeywords(), resourceLoader, new ParameterConverters(), new TableTransformers());
    }

    public ExamplesTableFactory(ParameterConverters parameterConverters) {
        this(new LocalizedKeywords(), new LoadFromClasspath(), parameterConverters, new TableTransformers());
    }

    public ExamplesTableFactory(TableTransformers tableTransformers) {
        this(new LocalizedKeywords(), new LoadFromClasspath(), new ParameterConverters(), tableTransformers);
    }

    public ExamplesTableFactory(Keywords keywords, ResourceLoader resourceLoader,
                                ParameterConverters parameterConverters) {
        this(keywords, resourceLoader, parameterConverters, new TableTransformers());
    }

    public ExamplesTableFactory(Keywords keywords, ResourceLoader resourceLoader,
                                ParameterConverters parameterConverters, TableTransformers tableTranformers) {
        this.keywords = keywords;
        this.resourceLoader = resourceLoader;
        this.parameterConverters = parameterConverters;
        this.tableTransformers = tableTranformers;
    }

    public ExamplesTable createExamplesTable(String input) {
        String tableAsString;
        if (isBlank(input) || isTable(input)) {
            tableAsString = input;
        } else {
            tableAsString = resourceLoader.loadResourceAsText(input);
        }
        return new ExamplesTable(tableAsString, keywords.examplesTableHeaderSeparator(),
                keywords.examplesTableValueSeparator(), keywords.examplesTableIgnorableSeparator(),
                parameterConverters, tableTransformers);
    }

    protected boolean isTable(String input) {
        return input.contains(keywords.examplesTableHeaderSeparator());
    }

    public void useKeywords(Keywords keywords){
        this.keywords = keywords;
    }

    public Keywords keywords() {
        return this.keywords;
    }

}
