package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.google.common.base.Function;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;

public class BigDecimalConverter implements ParameterConverter<BigDecimal>,
        Function<String, BigDecimal> {

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    @Override
    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return BigDecimal.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    @Override
    public BigDecimal convertValue(String value, Field field) {
        return (BigDecimal) convertValue(value, field.getType());
    }

    @Override
    public BigDecimal apply(String value) {
        return (BigDecimal) convertValue(value, BigDecimal.class);
    }

    @Override
    public Object convertValue(String value, Type type) {
        if (NULL.equalsIgnoreCase(value)){
            return null;
        }
        return new BigDecimal(value);
    }


}
