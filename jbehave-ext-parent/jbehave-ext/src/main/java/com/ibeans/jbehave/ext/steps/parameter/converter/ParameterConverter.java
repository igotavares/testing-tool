package com.ibeans.jbehave.ext.steps.parameter.converter;

import org.jbehave.core.steps.ParameterConverters;

import java.lang.reflect.Field;


public interface ParameterConverter<T> extends ParameterConverters.ParameterConverter {

    String NULL = "null";

    boolean accept(Field field);

    T convertValue(String value, Field field);

}
