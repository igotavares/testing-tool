package com.ibeans.jbehave.ext.steps.parameter;

import java.lang.reflect.Field;


public interface Parameters extends org.jbehave.core.steps.Parameters {


    <T> T valueAs(String name, Field field);


    <T> T valueAs(String name, Field field, T defaultValue);

}
