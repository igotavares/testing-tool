package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.ibeans.jbehave.ext.annotation.AsParameters;
import com.ibeans.jbehave.ext.parameter.ExamplesTableFactory;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;


public class ExamplesTableParametersConverter implements ParameterConverter {


    private final ExamplesTableFactory factory;

    public ExamplesTableParametersConverter() {
        this(new ExamplesTableFactory());
    }

    public ExamplesTableParametersConverter(ExamplesTableFactory factory) {
        this.factory = factory;
    }

    @Override
    public Object convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return convertValue(value, type);
    }

    public Object convertValue(String value, Type type) {
        List<?> rows = factory.createExamplesTable(value).getRowsAs(argumentClass(type));
        if (type instanceof ParameterizedType) {
            return rows;
        }
        return rows.iterator().next();
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

    public boolean accept(Type type) {
        if (type instanceof ParameterizedType) {
            Class<?> rawClass = rawClass(type);
            Class<?> argumentClass = argumentClass(type);
            if (rawClass.isAnnotationPresent(AsParameters.class)
                    || argumentClass.isAnnotationPresent(AsParameters.class)) {
                return true;
            }
        } else if (type instanceof Class) {
            return ((Class<?>) type).isAnnotationPresent(AsParameters.class);
        }
        return false;
    }

    private Class<?> rawClass(Type type) {
        return (Class<?>) ((ParameterizedType) type).getRawType();
    }

    private Class<?> argumentClass(Type type) {
        if (type instanceof ParameterizedType) {
            return (Class<?>) ((ParameterizedType) type).getActualTypeArguments()[0];
        } else {
            return (Class<?>) type;
        }
    }

}
