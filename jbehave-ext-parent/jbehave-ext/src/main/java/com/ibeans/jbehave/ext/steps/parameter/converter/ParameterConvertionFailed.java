package com.ibeans.jbehave.ext.steps.parameter.converter;


public class ParameterConvertionFailed extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6553740476074561329L;

	public ParameterConvertionFailed(String message) {
        super(message);
    }

    public ParameterConvertionFailed(String message, Throwable cause) {
        super(message, cause);
    }

}
