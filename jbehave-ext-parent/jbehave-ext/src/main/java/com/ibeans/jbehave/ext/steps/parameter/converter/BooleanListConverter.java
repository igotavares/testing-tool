package com.ibeans.jbehave.ext.steps.parameter.converter;

import java.lang.reflect.Field;
import java.util.List;

public class BooleanListConverter extends org.jbehave.core.steps.ParameterConverters.BooleanListConverter implements ParameterConverter<List<Boolean>> {

    public BooleanListConverter() {
    }

    public BooleanListConverter(String valueSeparator) {
        super(valueSeparator);
    }

    public BooleanListConverter(String valueSeparator, String trueValue, String falseValue) {
        super(valueSeparator, trueValue, falseValue);
    }

    @Override
    public List<Boolean> convertValue(String value, Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return (List<Boolean>) convertValue(value, type);
    }

    @Override
    public boolean accept(Field field) {
        Class<?> type = (Class<?>) field.getGenericType();
        return accept(type);
    }

}
