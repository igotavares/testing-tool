package com.ibeans.jbehave.ext.steps.parameter.converter;

import com.ibeans.jbehave.ext.JBehaveExtTestConstants;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalDate;
import org.junit.Test;

import static org.junit.Assert.*;

public class LocalDateTimeConverterTest {

    private LocalDateTimeConverter localDateTimeConverter;

    public LocalDateTimeConverterTest() {
        this.localDateTimeConverter = new LocalDateTimeConverter();
    }

    @Test
    public void givenDateWithoutTime_shouldConvertToDateTime() {
        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_23_07_2016,
                LocalDateTime.class);

        LocalDateTime expected = LocalDateTime.parse(JBehaveExtTestConstants.DATE_TIME_IS_23_07_2016_00_00_00,
                JBehaveExtTestConstants.DEFAULT_FORMAT);

        assertEquals(expected, actual);
    }

    @Test
    public void givenDateWithoutTimeAndNowYear_shouldConvertToDateTime() {
        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_23_07_CURRENT_YEAR,
                LocalDateTime.class);

        LocalDateTime expected = new LocalDateTime(
                LocalDate.now().getYear(), 7, 23, 0, 0, 0);

        assertEquals(expected, actual);
    }

    @Test
    public void givenDateWithoutTimeAndNowYearWithOperation_shouldConvertToDateTime() {
        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_23_07_CURRENT_YEAR_ADDING_1_YEAR,
                LocalDateTime.class);

        LocalDateTime expected = new LocalDateTime(
                LocalDate.now().getYear() + 1, 7, 23, 0, 0, 0);

        assertEquals(expected, actual);
    }

    @Test
    public void givenDateTime_shouldConvertToDateTime() {
        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_23_07_2016_12_59_59,
                LocalDateTime.class);

        LocalDateTime expected = LocalDateTime.parse(JBehaveExtTestConstants.DATE_TIME_IS_23_07_2016_12_59_59,
                JBehaveExtTestConstants.DEFAULT_FORMAT);

        assertEquals(expected, actual);
    }

    @Test
    public void givenDateTimeIsNull_shouldConvertToNull() {
        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_NULL,
                LocalDateTime.class);

        assertNull(actual);
    }

    @Test
    public void givenDateWithNowYear_shouldConvertToDateTime() {
        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_23_07_CURRENT_YEAR_13_59_59,
                LocalDateTime.class);

        LocalDateTime expected = new LocalDateTime(
                LocalDate.now().getYear(), 7, 23, 13, 59, 59);

        assertEquals(expected, actual);
    }

    @Test
    public void givenNowDateTimeWithNowYear_shouldConvertToDateTime() {
        LocalDateTime expected = LocalDateTime.now();

        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_NOW,
                LocalDateTime.class);

        assertTrue(expected.isEqual(actual) || expected.isBefore(actual));
    }

    @Test
    public void givenNowDateTimeWithHoursAddingOneYear_shouldConvertToDateTime() {
        LocalDateTime expected = LocalDateTime.now();
        expected = expected.withYear(2018);
        expected = expected.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(0);

        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_NOW_23_59_59_ADDING_1_YEAR,
                LocalDateTime.class);

        assertEquals(expected, actual);
    }

    @Test
    public void givenNowDateTimeWithHoursAddingOneMonth_shouldConvertToDateTime() {
        LocalDateTime expected = LocalDateTime.now();
        expected = expected.plusMonths(1);
        expected = expected.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(0);

        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_NOW_23_59_59_ADDING_1_MONTH,
                LocalDateTime.class);

        assertEquals(expected, actual);
    }

    @Test
    public void givenNowDateTimeWithHoursRemovingOneMonth_shouldConvertToDateTime() {
        LocalDateTime expected = LocalDateTime.now();
        expected = expected.minusMonths(1);
        expected = expected.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(0);

        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_NOW_23_59_59_REMOVING_1_MONTH,
                LocalDateTime.class);

        assertEquals(expected, actual);
    }

    @Test
    public void givenNowDateTimeAddingTwoMonths_shouldConvertToDateTime() {
        LocalDateTime expected = LocalDateTime.now();
        expected = expected
                .withDayOfMonth(23)
                .withMonthOfYear(9)
                .withYear(2017)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);

        LocalDateTime actual = (LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_23_07_2017_ADDING_2_MONTH,
                LocalDateTime.class);

        assertEquals(expected, actual);
    }

    @Test
    public void givenNowDate_135959_shouldConvertToDateTime() {
        LocalDateTime actual = ((LocalDateTime) localDateTimeConverter.convertValue(
                JBehaveExtTestConstants.DATE_TIME_IS_CURRENT_DATE_13_59_59,
                LocalDateTime.class));

        LocalDateTime expected = LocalDateTime.now()
                .withHourOfDay(13)
                .withMinuteOfHour(59)
                .withSecondOfMinute(59);

        assertEquals(expected, actual);
    }

}