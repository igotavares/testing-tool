package com.ibeans.jbehave.ext.steps.parameter.converter;

import org.junit.Test;

import java.math.BigDecimal;


public class NumberConverterTest {

    @Test
    public void shouldConveted() throws Exception {
        System.out.print(new BigDecimal("-38807102.90572536"));
        new NumberConverter().convertValue("-38807102.90572536", BigDecimal.class);
    }
}