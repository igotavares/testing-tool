package com.ibeans.jbehave.ext;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public interface JBehaveExtTestConstants {

    String DATE_TIME_IS_NULL = "null";
    String DATE_TIME_IS_NOW = "now";
    String DATE_TIME_IS_NOW_23_59_59_ADDING_1_YEAR = "now 23:59:59 +1y";
    String DATE_TIME_IS_NOW_23_59_59_ADDING_1_MONTH = "now 23:59:59 +1M";
    String DATE_TIME_IS_NOW_23_59_59_REMOVING_1_MONTH = "now 23:59:59 -1M";
    String DATE_TIME_IS_23_07_2017_ADDING_2_MONTH = "23/07/2017 +2M";
    String DATE_TIME_IS_CURRENT_DATE_13_59_59 = "current date 13:59:59";
    String DATE_TIME_IS_23_07_2016_12_59_59 = "23/07/2016 13:59:59";
    String DATE_TIME_IS_23_07_2016_00_00_00 = "23/07/2016 00:00:00";
    String DATE_TIME_IS_23_07_2016 = "23/07/2016";
    String DATE_TIME_IS_23_07_CURRENT_YEAR = "23/07/current year";
    String DATE_TIME_IS_23_07_CURRENT_YEAR_ADDING_1_YEAR = "23/07/current year +1y";
    String DATE_TIME_IS_23_07_CURRENT_YEAR_13_59_59 = "23/07/current year 13:59:59";
    DateTimeFormatter DEFAULT_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

}
