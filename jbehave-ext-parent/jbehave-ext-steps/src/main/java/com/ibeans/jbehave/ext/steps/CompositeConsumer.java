package com.ibeans.jbehave.ext.steps;

import java.util.Collection;


public class CompositeConsumer implements Consumer {

    private Collection<Consumer> consumers;

    public CompositeConsumer(Collection<Consumer> consumers) {
        this.consumers = consumers;
    }

    @Override
    public void accept() {
        consumers.stream().forEach(Consumer::accept);
    }

}
