package com.ibeans.jbehave.ext.steps;

import com.ibeans.jbehave.ext.spring.annotation.Steps;
import com.ibeans.jbehave.ext.support.Cleaned;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import java.util.Map;


@Steps
public class ScenarioSteps {

    @Inject
    private Consumer scenarioConsumer;

    @Inject
    private DateContext dateContext;

    @Inject
    private ApplicationContext applicationContext;

    @BeforeScenario
    public void beforeScenario() {
        dateContext.now();

        scenarioConsumer.accept();
    }

    @AfterScenario
    public void afterScenario() {
        clearContext();

        scenarioConsumer.accept();
    }

    private void clearContext() {
        Map<String, Cleaned> contexts = applicationContext.getBeansOfType(Cleaned.class);
        if (contexts != null) {
            contexts.values().stream().forEach(Cleaned::clear);
        }
    }


}
