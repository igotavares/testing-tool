package com.ibeans.jbehave.ext.steps;

import com.ibeans.jbehave.ext.spring.annotation.Context;
import com.ibeans.jbehave.ext.support.Cleaned;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


@Context
public class DateContext implements Cleaned {

    private ThreadLocal<LocalDate> date;

    private ThreadLocal<LocalDateTime> dateTime;

    public DateContext() {
        this.date = new ThreadLocal<LocalDate>();
        this.dateTime = new ThreadLocal<LocalDateTime>();
    }

    public void now() {
        date.set(LocalDate.now());
        dateTime.set(LocalDateTime.now());
    }

    public LocalDate getDate() {
        return date.get();
    }

    public LocalDateTime getDateTime() {
        return dateTime.get();
    }

    @Override
    public void clear() {
        date.remove();
        dateTime.remove();
    }

}
