package com.ibeans.jbehave.ext.steps;

import com.ibeans.jbehave.ext.spring.annotation.Steps;
import com.ibeans.jbehave.ext.support.Result;
import com.google.common.collect.Lists;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.model.ExamplesTable;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@Steps
public class MessageResultSteps {

    private static final String RESULT_CAN_NOT_BE_NULL = "Result can not be null";

    @Inject
    private ResultContext context;

    @Then("I should see: $messages")
    public void thenIShouldSeeTheFollowingMessages(ExamplesTable messages) {
        thenIShouldSeeA(toList(messages));
    }

    @Then("I should see a '$message' message")
    public void thenIShouldSeeTheFollowingMessages(String message) {
        thenIShouldSeeA(Lists.newArrayList(message));
    }

    @Then("I should see a '$message' messages")
    public void thenIShouldSeeA(List<String> messages) {
        Result<?> result = context.get();

        assertNotNull(RESULT_CAN_NOT_BE_NULL, result);

        assertEquals(messages, result.getValue());
    }


    private List<String> toList(ExamplesTable examplesTable) {
        List<String> result = Lists.newArrayList();
        for (Map<String,String> row : examplesTable.getRows()) {
            result.add(row.get("message"));
        }
        return result;
    }

}
