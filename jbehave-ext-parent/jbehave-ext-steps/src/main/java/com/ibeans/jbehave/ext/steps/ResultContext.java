package com.ibeans.jbehave.ext.steps;

import com.ibeans.jbehave.ext.spring.annotation.Context;
import com.ibeans.jbehave.ext.support.Result;
import com.ibeans.jbehave.ext.support.SimpleContext;


@Context
public class ResultContext extends SimpleContext<Result<?>> {

    public <E> E getValue() {
        Result<?> result = get();
        if (result != null) {
            return (E) result.getValue();
        }
        return null;
    }

    public Result<?> getResult() {
        return get();
    }

}
