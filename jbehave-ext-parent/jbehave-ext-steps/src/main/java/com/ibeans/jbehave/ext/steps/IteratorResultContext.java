package com.ibeans.jbehave.ext.steps;

import com.ibeans.jbehave.ext.spring.annotation.Context;
import com.ibeans.jbehave.ext.support.IteratorContext;

import java.util.Map;


@Context
public class IteratorResultContext extends IteratorContext<Map<String, Object>> {
}
