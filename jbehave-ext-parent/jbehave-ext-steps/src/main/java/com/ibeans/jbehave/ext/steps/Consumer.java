package com.ibeans.jbehave.ext.steps;


@FunctionalInterface
public interface Consumer {

    void accept();

}
