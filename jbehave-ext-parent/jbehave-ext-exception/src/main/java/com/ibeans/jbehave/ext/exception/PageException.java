package com.ibeans.jbehave.ext.exception;


public class PageException extends RuntimeException {

    private static final long serialVersionUID = 1091583381436860669L;

    private final String url;
    private final String sourcePage;
    private final byte[] screenshot;

    public PageException(String url, String sourcePage, byte[] screenshot, Exception cause) {
        super(cause);
        this.url = url;
        this.sourcePage = sourcePage;
        this.screenshot = screenshot;
    }

    public String getUrl() {
        return this.url;
    }

    public String getSourcePage() {
        return sourcePage;
    }

    public byte[] getScreenshot() {
        return screenshot;
    }

}
