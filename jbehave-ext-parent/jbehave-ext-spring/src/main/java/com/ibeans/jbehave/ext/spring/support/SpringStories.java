package com.ibeans.jbehave.ext.spring.support;

import com.ibeans.extentreports.ext.ExtentReports;
import com.ibeans.jbehave.ext.reporter.ExtentStoryReporter;
import com.ibeans.jbehave.ext.steps.parameter.converter.*;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.io.*;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.FilePrintStreamFactory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.ParameterControls;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
public abstract class SpringStories extends JUnitStories {

    private static final String STORY_TIMEOUT = "6000";

    @Autowired
    private ApplicationContext applicationContext;

    public SpringStories() {
        Embedder embedder = new Embedder();
        embedder.useEmbedderControls(embedderControls());
        useEmbedder(embedder);
    }

    @Override
    public void run() throws Throwable {
        checkHealth();
        super.run();
    }

    protected void checkHealth() {

    }

    private EmbedderControls embedderControls() {
        return new EmbedderControls()
                .useStoryTimeouts(STORY_TIMEOUT);
    }

    public Configuration configuration() {
        return new MostUsefulConfiguration()
                .useStoryPathResolver(storyPathResolver())
                .useStoryLoader(storyLoader())
                .useStoryReporterBuilder(storyReporterBuilder())
                .useParameterControls(parameterControls())
                .useParameterConverters(parameterConverters());
    }

    private ParameterConverters parameterConverters() {
        return new ParameterConverters()
                .addConverters(new DateConverter(),
                        new LocalDateConverter(),
                        new LocalDateTimeConverter(),
                        new DateTimeConverter(),
                        new NumberConverter(),
                        new StringConverter(),
                        new ExamplesTableParametersConverter(),
                        new BooleanConverter());
    }

    private ParameterControls parameterControls() {
        return new ParameterControls();
    }

    private StoryLoader storyLoader() {
        return new LoadFromClasspath();
    }

    private StoryPathResolver storyPathResolver() {
        return new UnderscoredCamelCaseResolver();
    }

    private StoryReporterBuilder storyReporterBuilder() {
        return new StoryReporter();
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new SpringStepsFactory(configuration(), applicationContext);
    }

    protected List<String> storyPaths() {
        final String searchInDirectory = CodeLocations.codeLocationFromClass(
                codeLocationClass()).getFile();
        return new StoryFinder().findPaths(searchInDirectory,
                Arrays.asList("**/*.story"), null);
    }

    protected abstract Class<?> codeLocationClass();

    public static class StoryReporter extends StoryReporterBuilder {
        public StoryReporter() {
            withFormats(org.jbehave.core.reporters.Format.CONSOLE,
                    EXTENT_REPORTS);
        }
    }

    private static final String PATH = "target/report/";
    private static final String NAME_FILE = "index.html";

    protected static ExtentReports report = new ExtentReports(PATH, NAME_FILE);

    public static final Format EXTENT_REPORTS = new Format("EXTENT_REPORTS") {

        public org.jbehave.core.reporters.StoryReporter createStoryReporter(FilePrintStreamFactory factory, StoryReporterBuilder storyReporterBuilder) {
            return new ExtentStoryReporter(storyReporterBuilder.keywords(), report, PATH)
                    .doReportFailureTrace(storyReporterBuilder.reportFailureTrace())
                    .doCompressFailureTrace(storyReporterBuilder.compressFailureTrace());
        }

    };

}
