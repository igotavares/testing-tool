package com.ibeans.jbehave.ext.spring.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;


@Target(value= ElementType.TYPE)
@Retention(value= RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Steps {
}
