package com.ibeans.extentreports.ext;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.NetworkMode;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;


public class ExtentReports extends com.relevantcodes.extentreports.ExtentReports {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1340155075430371311L;

	private static final String DEFAULT_IMAGE_PATH = "images";

    private String imagePath;

    public ExtentReports(String path, String fileName) {
        this(path, fileName, DEFAULT_IMAGE_PATH, null, null, null, null);
    }

    public ExtentReports(String path, String fileName, String imagePath, Boolean replaceExisting, DisplayOrder displayOrder, NetworkMode networkMode, Locale locale) {
        super(path + File.separator + fileName, replaceExisting, displayOrder, networkMode, locale);
        this.imagePath = imagePath;
        if (getReplaceExisting()) {
            File file = new File(path + File.separator + imagePath);
            try {
                FileUtils.deleteDirectory(file);
            } catch (IOException cause) {
                throw new RuntimeException("Error delete the images directory", cause);
            }
        }
    }


    public synchronized com.relevantcodes.extentreports.ExtentTest startTest(String testName, String description) {
        if (testList == null) {
            testList = new ArrayList<com.relevantcodes.extentreports.ExtentTest>();
        }

        ExtentTest test = new ExtentTest(testName, description);

        updateTestQueue(test);

        return test;
    }

}
