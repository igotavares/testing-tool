package com.ibeans.extentreports.ext;


public class ExtentTest extends com.relevantcodes.extentreports.ExtentTest {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8913114208023582782L;

	/**
     * <p/>
     * Creates a test node as a top-most level test
     *
     * @param testName    Test name
     * @param description
     */
    public ExtentTest(String testName, String description) {
        super(testName, description);
    }
}
