package com.ibeans.health.check;

import com.bnpparibas.cardif.extentreports.ext.ExtentReports;
import com.ibeans.jbehave.ext.reporter.ParameterConverter;
import com.ibeans.websphere.jmx.client.Application;
import com.ibeans.websphere.jmx.client.ApplicationSearch;
import com.ibeans.websphere.jmx.client.Applications;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class HealthCheck {

    private final ExtentReports report;
    private final ApplicationSearch search;

    public HealthCheck(ExtentReports report, ApplicationSearch search) {
        this.report = report;
        this.search = search;
    }

    public void check(String... applictionNames) {
        ExtentTest healthCkeckExtentTest = report.startTest("Health Check");
        ExtentTest applicationExtentTest = report.startTest("Applications");

        try {

            Applications applications = Applications.build(search, applictionNames);

            applications.forEachStated(application -> Optional.ofNullable(application)
                    .map(Application::toString)
                    .map(new ParameterConverter())
                    .ifPresent(result -> applicationExtentTest.log(LogStatus.PASS, result)));

            applications.forEachNotStated(application -> Optional.ofNullable(application)
                    .map(Application::toString)
                    .map(new ParameterConverter())
                    .ifPresent(result -> applicationExtentTest.log(LogStatus.FAIL, result)));

            applications.forEachNotFound(applicationName ->
                    Optional.ofNullable("\'" + applicationName + "\' Application Not Found!")
                            .map(new ParameterConverter())
                            .ifPresent(result -> applicationExtentTest.log(LogStatus.ERROR, result)));

            assertThat(applications.areAllStated() && applications.notHasApplication(), is(Boolean.TRUE));
        } catch (Exception cause) {
            applicationExtentTest.log(LogStatus.ERROR, cause);
            throw cause;
        } finally {
            healthCkeckExtentTest.appendChild(applicationExtentTest);
            report.endTest(healthCkeckExtentTest);
            report.flush();
        }
    }

}
