package com.ibeans.websphere.jmx.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.remote.JMXConnector;


@Component
public class JMXConnectorFactory {

    @Autowired
    private EnvironmentFactory environmentFactory;

    @Autowired
    private JMXServiceURLFactory jmxServiceURLFactory;

    public JMXConnector create() {
        try {
            return javax.management.remote.JMXConnectorFactory.newJMXConnector(
                    jmxServiceURLFactory.create(),
                    environmentFactory.create());
        } catch (Exception cause) {
            throw new RuntimeException("Error while trying create connect", cause);
        }
    }

}
