package com.ibeans.websphere.jmx.client;

import com.ibm.websphere.jmx.connector.rest.ConnectorSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.remote.JMXConnector;
import java.util.HashMap;
import java.util.Map;

@Component
public class EnvironmentFactory {

    @Autowired
    private User user;

    public Map<String, Object> create() {
        HashMap<String, Object> environment = new HashMap<>();
        environment.put("jmx.remote.protocol.provider.pkgs", "com.ibm.ws.jmx.connector.client");
        environment.put(ConnectorSettings.DISABLE_HOSTNAME_VERIFICATION, Boolean.TRUE);
        environment.put(JMXConnector.CREDENTIALS, user.toCredentials());
        return environment;
    }

}
