package com.ibeans.websphere.jmx.client;

import java.util.List;


public interface ApplicationSearch {

    List<Application> get(String[] names);

}
