package com.ibeans.websphere.jmx.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.remote.JMXServiceURL;


@Component
public class JMXServiceURLFactory {

    @Autowired
    private Server applicationServer;

    public JMXServiceURL create() {
        try {
            return new JMXServiceURL(applicationServer.toRestUrl());
        } catch (Exception cause) {
            throw new RuntimeException("Error creationg JMX Service Url by " + applicationServer + " server", cause);
        }
    }

}
