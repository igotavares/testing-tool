package com.ibeans.websphere.jmx.client;

import com.google.common.base.MoreObjects;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class Server {

    private final String host;
    private final String port;

    public String toRestUrl() {
        return "service:jmx:rest://" + host + ":" + port + "/IBMJMXConnectorREST";
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("host", host)
                .add("port", port)
                .toString();
    }
}
