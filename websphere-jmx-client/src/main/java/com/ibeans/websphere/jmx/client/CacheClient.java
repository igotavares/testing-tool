package com.ibeans.websphere.jmx.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import java.util.List;
import java.util.stream.Stream;


@Component
public class CacheClient {

    @Autowired
    private com.ibeans.websphere.jmx.client.JMXConnectorFactory jmxConnectorFactory;

    public void clearAll(List<String> cacheNames) {
        try(JMXConnector connector = jmxConnectorFactory.create()) {
            connector.connect();

            MBeanServerConnection serverConnection = connector.getMBeanServerConnection();

            cacheNames.stream()
                    .flatMap(applicationName -> search(serverConnection, applicationName))
                    .forEach(cache -> clearAll(serverConnection, cache));

        } catch(Exception cause) {
            throw new RuntimeException(cause);
        }
    }

    private Stream<ObjectName> search(MBeanServerConnection serverConnection, String applicationName) {
        try {
            ObjectName cacheObjectName = new ObjectName("net.sf.ehcache:type=CacheManager,name=" + applicationName);
            return serverConnection.queryNames(cacheObjectName, null).stream();
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

    private void clearAll(MBeanServerConnection serverConnection, ObjectName cache) {
        try {
            serverConnection.invoke(cache, "clearAll", null, null);
        } catch (Exception cause) {
            throw new RuntimeException("Error while executing cleanAll method", cause);
        }
    }

}
