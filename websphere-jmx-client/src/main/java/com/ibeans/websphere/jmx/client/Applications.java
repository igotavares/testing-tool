package com.ibeans.websphere.jmx.client;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;


public class Applications {

    private final List<String> names;
    private final List<Application> applications;

    public Applications(List<String> names, List<Application> applications) {
        this.names = names;
        this.applications = applications;
    }

    public void forEachStated(Consumer<Application> action) {
        applications.stream()
                .filter(Application::isStarted)
                .forEach(action);
    }

    public void forEachNotStated(Consumer<Application> action) {
        applications.stream()
                .filter(Application::isNotStarted)
                .forEach(action);
    }

    public void forEachNotFound(Consumer<String> action) {
        applicationsNotFound().forEach(action);
    }

    private Stream<String> applicationsNotFound() {
        return names.stream()
                .filter(this::notHasApplication);
    }

    private boolean notHasApplication(String name) {
        return !applications.stream()
                .filter(application -> application.is(name))
                .findFirst()
                .isPresent();
    }


    public void forEach(Consumer<Application> action) {
        applications.stream()
                .forEach(action);
    }

    public boolean areAllStated() {
        return applications.stream()
                .filter(Application::isNotStarted)
                .findFirst()
                .map(application -> false)
                .orElse(true);

    }

    public boolean notHasApplication() {
        return !applicationsNotFound()
                .findFirst()
                .isPresent();
    }

    public static Applications build(ApplicationSearch search, String[] names) {
        List<Application> applications = search.get(names);
        return new Applications(Arrays.asList(names), applications);
    }

}
