package com.ibeans.websphere.jmx.client;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@RequiredArgsConstructor
@Getter
public class Application {

    private static final String STARTED = "STARTED";
    private static final String APPLICATION_NAME_PATTERN = "(.*?)-(\\d.*)";
    private static final String NAME_GROUP = "$1";
    private static final String VERSION_GROUP = "$2";

    private final String name;
    private final String version;
    private final String state;

    public boolean isNotStarted() {
        return !isStarted();
    }

    public boolean isStarted() {
        return STARTED.equals(state);
    }

    public boolean is(List<String> names) {
        return names != null
                && names.stream()
                    .filter(this::is)
                    .findFirst()
                    .isPresent();
    }

    public boolean is(String name) {
        return this.name != null
                && this.name.equalsIgnoreCase(name);
    }

    @Override
    public String toString() {
        return "'" + name + "-" + version + "' Application is '" + state + "'";
    }

    public static Application create(String fullName, String state) {
        String name = getGroup(fullName, NAME_GROUP);
        String version = getVersion(fullName);
        return new Application(name, version, state);
    }

    private static String getName(String value) {
        return getGroup(value, NAME_GROUP);
    }

    private static String getVersion(String value) {
        if (hasVersion(value)) {
            return getGroup(value, VERSION_GROUP);
        }
        return null;
    }

    private static boolean hasVersion(String value) {
        return value != null
                && value.matches(APPLICATION_NAME_PATTERN);
    }

    private static String getGroup(String value, String group) {
        if (value != null) {
            return value.replaceAll(APPLICATION_NAME_PATTERN, group);
        }
        return null;
    }

    public Map<String, String> toMap() {
        Map<String, String> application = new LinkedHashMap<>();
        application.put("name", name);
        application.put("version", version);
        application.put("state", state);
        return application;
    }

}
