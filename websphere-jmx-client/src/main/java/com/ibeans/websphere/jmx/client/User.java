package com.ibeans.websphere.jmx.client;


import com.google.common.base.MoreObjects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


@Component("webSphereUser")
public class User {

    @Autowired
    private Environment env;

    public String getUsername() {
        return env.getProperty("websphere.user.username");
    }

    public String getPassword() {
        return env.getProperty("websphere.user.password");
    }

    public String[] toCredentials() {
        return new String[] {getUsername(), getPassword()};
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("username", getUsername())
                .add("password", getPassword())
                .toString();
    }
}
