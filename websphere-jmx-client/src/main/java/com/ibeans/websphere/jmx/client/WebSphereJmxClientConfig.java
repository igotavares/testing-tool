package com.ibeans.websphere.jmx.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;


@Configuration
@ComponentScan
@PropertySource("classpath:websphere-jmx-client.properties")
public class WebSphereJmxClientConfig {

    @Autowired
    private Environment env;

    @Profile("default")
    @Bean
    public Server applicationServer() {
        return new Server(env.getProperty("websphere.server.host"),
                env.getProperty("websphere.server.port"));
    }

    @Profile("IC01")
    @Bean(name = "applicationServer")
    public Server serverCI01() {
        return new Server(env.getProperty("websphere.server.cyborg.host"),
                env.getProperty("websphere.server.ci01.port"));
    }

    @Profile("IC02")
    @Bean(name = "applicationServer")
    public Server serverCI02() {
        return new Server(env.getProperty("websphere.server.cyborg.host"),
                env.getProperty("websphere.server.ci02.port"));
    }

    @Profile("IC03")
    @Bean(name = "applicationServer")
    public Server serverCI03() {
        return new Server(env.getProperty("websphere.server.cyborg.host"),
                env.getProperty("websphere.server.ci03.port"));
    }

    @Profile("IC04")
    @Bean(name = "applicationServer")
    public Server serverCI04() {
        return new Server(env.getProperty("websphere.server.cyborg.host"),
                env.getProperty("websphere.server.ci04.port"));
    }

    @Profile("IC05")
    @Bean(name = "applicationServer")
    public Server serverCI05() {
        return new Server(env.getProperty("websphere.server.cyborg.host"),
                env.getProperty("websphere.server.ci05.port"));
    }

}
