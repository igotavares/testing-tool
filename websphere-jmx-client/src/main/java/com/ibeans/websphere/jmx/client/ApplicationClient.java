package com.ibeans.websphere.jmx.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class ApplicationClient implements ApplicationSearch {

    @Autowired
    private com.ibeans.websphere.jmx.client.JMXConnectorFactory jmxConnectorFactory;

    @Override
    public List<Application> get(String[] names) {
        return getStates(Arrays.asList(names));
    }

    public List<Application> getStates(List<String> applicationNames) {
        return getAllStates(application -> application.is(applicationNames));
    }

    public List<Application> getAllStates() {
        return getAllStates(application -> true);
    }

    public List<Application> getAllStates(Predicate<Application> filter) {
        try(JMXConnector connector = jmxConnectorFactory.create()) {
            connector.connect();

            MBeanServerConnection serverConnection = connector.getMBeanServerConnection();

            return search(serverConnection)
                    .map(application -> Application.create(
                            getName(application),
                            getState(serverConnection, application)))
                    .filter(filter)
                    .sorted(Comparator.comparing(Application::getName))
                    .collect(Collectors.toList());

        } catch(Exception cause) {
            throw new RuntimeException(cause);
        }
    }

    private Stream<ObjectName> search(MBeanServerConnection serverConnection) {
        try {
            ObjectName cacheObjectName = new ObjectName("WebSphere:service=com.ibm.websphere.application.ApplicationMBean,name=*");
            return serverConnection.queryNames(cacheObjectName, null).stream();
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

    private String getState(MBeanServerConnection serverConnection, ObjectName application) {
        try {
            return (String) serverConnection.getAttribute(application, "State");
        } catch (Exception cause) {
            throw new RuntimeException("Error while getting state attribute", cause);
        }
    }

    private String getName(ObjectName application) {
        return application.getKeyProperty("name");
    }


}
