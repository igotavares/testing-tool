package com.ibeans.jdbc.ext.support;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.math.BigDecimal;


public enum DataType {

    LOCAL_DATE_TIME {
        public static final String SYSDATE = "SYSDATE";

        @Override
        public LocalDateTime to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    String convertedValue = (String) value;
                    if (SYSDATE.equalsIgnoreCase(convertedValue)) {
                        return LocalDateTime.now();
                    }
                }
                throw new IllegalArgumentException(value.getClass()
                        .getSimpleName() + " Value Type not supported");
            }
            return null;
        }
    },
    LOCAL_DATE {
        public static final String SYSDATE = "SYSDATE";

        @Override
        public LocalDate to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    String convertedValue = (String) value;
                    if (SYSDATE.equalsIgnoreCase(convertedValue)) {
                        return LocalDate.now();
                    }
                }
                throw new IllegalArgumentException(value.getClass()
                        .getSimpleName() + " Value Type not supported");
            }
            return null;
        }
    } ,
    STRING {
        @Override
        public String to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    return (String) value;
                }
                return value.toString();
            }
            return null;
        }
    },
    NUMBER {
        @Override
        public Number to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    return new BigDecimal((String) value);
                }
                throw new RuntimeException("value is not string");
            }
            return null;
        }
    };

    public abstract <E> E to(Object value);

}
