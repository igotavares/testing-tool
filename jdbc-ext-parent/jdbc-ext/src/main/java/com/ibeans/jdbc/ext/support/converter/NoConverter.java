package com.ibeans.jdbc.ext.support.converter;

import java.util.function.Function;


public class NoConverter implements Function<Object, Object> {

    @Override
    public Object apply(Object value) {
        return value;
    }



}
