package com.ibeans.jdbc.ext.support.column;

import com.ibeans.jdbc.ext.support.Column;
import com.ibeans.jdbc.ext.support.converter.NoConverter;

import java.util.List;


public class SequenceColumn extends Column {

    private SequenceSearch search;

    public SequenceColumn(String sequenceName, String name, List<String> tables, SequenceSearch search) {
        super(sequenceName, name, tables, new NoConverter());
        this.search = search;
    }

    @Override
    public Object getValue() {
        return search.nextSequence(getIdentifier());
    }

    @Override
    public Object getNoConvertedValue() {
        return getValue();
    }

}
