package com.ibeans.jdbc.ext.support.converter;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalDate;

import java.util.function.Function;
import java.util.function.Predicate;


public enum JodaConverter implements Function<Object, Object>, Predicate<Object> {

    LOCAL_DATE(LocalDate.class) {
        @Override
        public Object apply(Object value) {
            if (value != null && value instanceof LocalDate) {
                return ((LocalDate) value).toDate();
            }
            return null;
        }
    },
    LOCAL_DATE_TIME(LocalDateTime.class) {
        @Override
        public Object apply(Object value) {
            if (value != null && value instanceof LocalDateTime) {
                return ((LocalDateTime) value).toDate();
            }
            return null;
        }
    };

    private Class<?> type;

    JodaConverter(Class<?> type) {
        this.type = type;
    }

    @Override
    public boolean test(Object value) {
        return this.type.isInstance(value);
    }

    public static Object to(Object value) {
        if (value == null) {
            return null;
        }
        for (JodaConverter converter : values()) {
            if (converter.test(value.getClass())) {
                return converter.apply(value);
            }
        }
        return value;
    }



}
