package com.ibeans.jdbc.ext.support.column;

import java.util.List;


public class ValueSearchCache implements ValueSearch {

    private Boolean firstTime = Boolean.TRUE;
    private Object value;
    private List<Object> options;
    private final ValueSearch search;

    public ValueSearchCache(ValueSearch search) {
        this.search = search;
    }

    @Override
    public Object queryForObject(String sql, Object[] parameter) {
        if (firstTime) {
            value = search.queryForObject(sql, parameter);
            firstTime = Boolean.FALSE;
        }
        return value;
    }

    @Override
    public Object queryForObject(String sql) {
        if (firstTime) {
            value = search.queryForObject(sql);
            firstTime = Boolean.FALSE;
        }
        return value;
    }

    @Override
    public List<Object> queryForList(String sql) {
        if (firstTime) {
            options = search.queryForList(sql);
            firstTime = Boolean.FALSE;
        }
        return options;
    }
}
