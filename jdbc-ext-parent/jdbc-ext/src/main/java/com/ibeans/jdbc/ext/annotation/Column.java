package com.ibeans.jdbc.ext.annotation;

import com.ibeans.jdbc.ext.support.DataType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface Column {

    String sql() default "";

    String name();

    String defaultValue() default "";

    DataType type() default DataType.STRING;

    String sqlOptions() default "";

    Table[] table() default {};

    EnumMap[] map() default {};

    Json[] json() default {};

}
