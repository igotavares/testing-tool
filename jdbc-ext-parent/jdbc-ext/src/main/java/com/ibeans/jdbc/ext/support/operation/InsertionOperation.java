package com.ibeans.jdbc.ext.support.operation;

import com.ibeans.jdbc.ext.support.Sql;

import java.util.function.Function;


public class InsertionOperation implements Function<Sql, Object> {

    private final Updated inserted;
    private final Function<Sql, Object> result;

    public InsertionOperation(Updated inserted, Function<Sql, Object> result) {
        this.inserted = inserted;
        this.result = result;
    }

    @Override
    public Object apply(Sql sql) {
        insert(sql);
        return result.apply(sql);
    }

    private void insert(Sql sql) {
        if (sql.hasArgs()) {
            inserted.update(sql.toSql(), sql.getParameter());
        } else {
            inserted.update(sql.toSql());
        }
    }

}
