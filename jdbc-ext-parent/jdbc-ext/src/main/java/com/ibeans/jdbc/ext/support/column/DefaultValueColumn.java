package com.ibeans.jdbc.ext.support.column;


import com.ibeans.jdbc.ext.support.DataType;
import com.ibeans.jdbc.ext.support.converter.NoConverter;

import java.util.List;


public class DefaultValueColumn extends ValueColumn {

    private DataType type;

    public DefaultValueColumn(String name, List<String> tables, Object value, DataType type) {
        super(name, name, tables, value, new NoConverter());
        this.type = type;
    }

    @Override
    public Object getValue() {
        return type.to(super.getValue());
    }

    @Override
    public Object getNoConvertedValue() {
        return type.to(super.getNoConvertedValue());
    }

}
