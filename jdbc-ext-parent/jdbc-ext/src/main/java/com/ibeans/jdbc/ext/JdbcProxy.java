package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.handler.HandlerChain;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


public class JdbcProxy implements InvocationHandler {

    private HandlerChain handlerChain;

    public JdbcProxy(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        this.handlerChain = new HandlerChain(jdbcAdapter, proxyClass);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String name = method.getName();
        if("equals".equals(name)) {
            return proxy == args[0];
        }
        if("hashCode".equals(name)) {
            return System.identityHashCode(proxy);
        }
        if("toString".equals(name)) {
            return proxy.getClass().getName() + "@" +
                    Integer.toHexString(System.identityHashCode(proxy)) +
                    ", with InvocationHandler " + this;
        }
        return handlerChain.invoke(proxy, method, args);

    }

}
