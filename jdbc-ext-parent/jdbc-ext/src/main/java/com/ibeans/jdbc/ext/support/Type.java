package com.ibeans.jdbc.ext.support;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.math.BigDecimal;
import java.util.Date;


public enum Type {

    DEFAULT {
        @Override
        public Object to(Object value) {
            return value;
        }
    },
    STRING {
        @Override
        public String to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    return (String) value;
                }
                return value.toString();
            }
            return null;
        }
    },
    BOOLEAN {
        @Override
        public Boolean to(Object value) {
            if(value != null) {
                if (value instanceof Boolean) {
                    return (Boolean) value;
                }
                if (value instanceof Number) {
                    return parser(((Number) value).intValue());
                }
            }
            return null;
        }

        private Boolean parser(Integer value) {
            if (value == 1) {
                return true;
            }
            if (value == 0) {
                return false;
            }
            throw new IllegalArgumentException("Value " + value + " is not Boolean");
        }

    },
    LONG {
        @Override
        public Long to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    return Long.valueOf((String) value);
                }
                if (value instanceof Long) {
                    return (Long) value;
                }
                if (value instanceof Number) {
                    return ((Number) value).longValue();
                }
                throw new RuntimeException("value is not Long");
            }
            return null;
        }
    },
    INTEGER {
        @Override
        public Integer to(Object value) {
            if(value != null) {
                if (value instanceof String) {
                    return Integer.valueOf((String) value);
                }
                if (value instanceof Integer) {
                    return (Integer) value;
                }
                if (value instanceof Number) {
                    return ((Number) value).intValue();
                }
                throw new RuntimeException("value is not Long");
            }
            return null;
        }
    },
    BIG_DECIMAL {
        @Override
        public BigDecimal to(Object value) {
            if(value != null) {
                if (value instanceof BigDecimal) {
                    return (BigDecimal) value;
                }
            }
            return null;
        }
    },
    DATE {
        @Override
        public Date to(Object value) {
            if(value != null) {
                if (value instanceof Date) {
                    return (Date) value;
                }
                throw new IllegalArgumentException(value.getClass()
                        .getSimpleName() + " Value Type not supported");
            }
            return null;
        }
    },
    LOCAL_DATE {
        @Override
        public LocalDate to(Object value) {
            if(value != null) {
                if (value instanceof LocalDate) {
                    return (LocalDate) value;
                }
                if (value instanceof Date) {
                    return new LocalDate(value);
                }
                throw new IllegalArgumentException(value.getClass()
                        .getSimpleName() + " Value Type not supported");
            }
            return null;
        }
    },
    LOCAL_DATE_TIME {
        @Override
        public LocalDateTime to(Object value) {
            if(value != null) {
                if (value instanceof LocalDateTime) {
                    return (LocalDateTime) value;
                }
                if (value instanceof Date) {
                    return new LocalDateTime(value);
                }
                throw new IllegalArgumentException(value.getClass()
                        .getSimpleName() + " Value Type not supported");
            }
            return null;
        }
    };

    public abstract <E> E to(Object value);

}
