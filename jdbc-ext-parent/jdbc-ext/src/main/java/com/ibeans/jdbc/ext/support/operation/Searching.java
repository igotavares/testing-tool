package com.ibeans.jdbc.ext.support.operation;

import com.ibeans.jdbc.ext.support.RowMapper;

import java.util.List;
import java.util.Map;


public interface Searching {

    List<Map<String, Object>> query(String sql, RowMapper rowMapper);

    List<Map<String, Object>> query(String sql, Object[] args, RowMapper rowMapper);

}
