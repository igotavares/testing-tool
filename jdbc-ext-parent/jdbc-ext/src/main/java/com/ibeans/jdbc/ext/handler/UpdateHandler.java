package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.annotation.Update;
import com.ibeans.jdbc.ext.support.Sql;
import com.ibeans.jdbc.ext.support.operation.UpdateOperation;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.function.Function;


public class UpdateHandler extends AbstractHandler {

    public UpdateHandler(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        super(jdbcAdapter, proxyClass);
    }

    @Override
    public boolean test(Method method) {
        return method.getDeclaredAnnotation(Update.class) != null;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Sql sql = updateSQL(method);

        getJdbcAdapter().update(sql.toSql(), args);

        return null;
    }

    private Sql updateSQL(Method method) {
        return Optional.of(Update.class)
                .map(method::getDeclaredAnnotation)
                .map(Update::value)
                .map(sql -> new Sql(createUpdateOperation(), sql))
                .get();
    }

    private Function<Sql, Object> createUpdateOperation() {
        return new UpdateOperation(getJdbcAdapter());
    }

}
