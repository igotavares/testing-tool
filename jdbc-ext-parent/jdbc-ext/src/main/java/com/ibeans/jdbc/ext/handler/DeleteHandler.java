package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.annotation.Delete;
import com.ibeans.jdbc.ext.support.DeleteSql;
import com.ibeans.jdbc.ext.support.Sql;
import com.ibeans.jdbc.ext.support.operation.DeletionOperation;

import java.lang.reflect.Method;
import java.util.Optional;


public class DeleteHandler extends AbstractHandler {

    public DeleteHandler(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        super(jdbcAdapter, proxyClass);
    }

    @Override
    public boolean test(Method method) {
        return Optional.of(Delete.class)
                .map(method::getDeclaredAnnotation)
                .isPresent();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Optional<Delete> delete = Optional.of(Delete.class).map(method::getDeclaredAnnotation);

        delete(delete, method, args);

        return null;
    }

    protected void delete(Optional<Delete> delete, Method method, Object[] args) {
        Sql sql = createSQL(delete, method, args);

        sql.execute();
    }

    private Sql createSQL(Optional<Delete> delete, Method method, Object[] args) {
        Optional<String> deleteSql = getDeleteSql(delete);
        if (deleteSql.isPresent()) {
            return createSql(deleteSql, args);
        }
        return createDeleteSql(method, args);
    }

    private Optional<String> getDeleteSql(Optional<Delete> delete) {
        return delete.map(Delete::value)
                .filter(this::notEmpty);
    }

    private Sql createSql(Optional<String> deleteSql, Object[] args) {
        Sql sql = new Sql(createDeletionOperation(), deleteSql.get());
        sql.addParameters(toDefaultParameter(args));
        return sql;
    }

    private Sql createDeleteSql(Method method, Object[] args) {
        DeleteSql deleteSql = new DeleteSql(createDeletionOperation(), getNameTable());
        deleteSql.addConditions(createConditions(method, args));
        return deleteSql;
    }

    private Boolean notEmpty(String value) {
        return value != null && !value.isEmpty();
    }

    private DeletionOperation createDeletionOperation() {
        return new DeletionOperation(getJdbcAdapter());
    }

}
