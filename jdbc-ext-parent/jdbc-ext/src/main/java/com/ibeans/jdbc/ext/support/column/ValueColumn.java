package com.ibeans.jdbc.ext.support.column;

import com.ibeans.jdbc.ext.support.Column;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class ValueColumn extends Column {

    private final Object value;

    public ValueColumn(Object value, Function<Object, Object> converter) {
        super(null, null, new ArrayList<>(), converter);
        this.value = value;
    }

    public ValueColumn(String identifier, String name, Object value, Function<Object, Object> converter) {
        super(identifier, name, new ArrayList<>(), converter);
        this.value = value;
    }

    public ValueColumn(String identifier, String name, List<String> tables, Object value, Function<Object, Object> converter) {
        super(identifier, name, tables, converter);
        this.value = value;
    }

    @Override
    public Object getValue() {
        return getConverter().apply(this.value);
    }

    @Override
    public Object getNoConvertedValue() {
        return this.value;
    }

}
