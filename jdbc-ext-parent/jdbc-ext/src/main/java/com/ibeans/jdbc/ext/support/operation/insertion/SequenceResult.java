package com.ibeans.jdbc.ext.support.operation.insertion;

import com.ibeans.jdbc.ext.support.Column;
import com.ibeans.jdbc.ext.support.Sql;
import com.ibeans.jdbc.ext.support.column.SequenceColumn;

import java.util.function.Function;


public class SequenceResult implements Function<Sql, Object> {

    @Override
    public Object apply(Sql sql) {
        return sql.getColumns().stream()
                .filter((column -> column instanceof SequenceColumn))
                .map(Column::getValue)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Sequence not found!"));
    }

}
