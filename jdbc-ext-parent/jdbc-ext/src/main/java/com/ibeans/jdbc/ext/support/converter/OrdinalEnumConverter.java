package com.ibeans.jdbc.ext.support.converter;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class OrdinalEnumConverter extends EnumConverter {

    public OrdinalEnumConverter(Class<? extends Enum<?>> enumClass, String from, String to) {
        super(enumClass, from, to);
    }

    @Override
    public boolean test(Object value) {
        return value != null && value instanceof String;
    }

    @Override
    protected boolean search(Enum enumValue, Object value) {
        return enumValue.name().equalsIgnoreCase((String) value);
    }

    @Override
    protected Object converter(Enum enumValue) {
        return enumValue.ordinal();
    }

    @Override
    protected String getOptions() {
        return Stream.of(getEnumClass().getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.joining(","));
    }

}
