package com.ibeans.jdbc.ext.support;

import com.ibeans.jdbc.ext.support.operation.SearchOperation;


public class QuerySql extends Sql {

    private Orderings orderings;

    public QuerySql(SearchOperation searchOperation, String table, Orderings orderings) {
        super(searchOperation, table);
        this.orderings = orderings;
    }

    @Override
    public String toSql() {
        return "SELECT * FROM " + super.toSql() + orderings.toSql();
    }



}
