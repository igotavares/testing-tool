package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.annotation.Column;
import com.ibeans.jdbc.ext.annotation.Table;
import com.ibeans.jdbc.ext.support.Condition;
import com.ibeans.jdbc.ext.support.DefaultParameter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public abstract class AbstractHandler implements Handler {

    private final JdbcAdapter jdbcAdapter;
    private final Class<?> proxyClass;

    public AbstractHandler(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        this.jdbcAdapter = jdbcAdapter;
        this.proxyClass = proxyClass;
    }

    public JdbcAdapter getJdbcAdapter() {
        return jdbcAdapter;
    }

    protected String getNameTable() {
        return Optional.of(Table.class)
                .map(proxyClass::getDeclaredAnnotation)
                .map(Table::value)
                .orElse(null);
    }

    protected List<DefaultParameter> toDefaultParameter(Object[] args) {
        if (args != null) {
            return Stream.of(args)
                    .map(DefaultParameter::new)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    protected List<Condition> createConditions(Method method, Object[] arg) {
        List<Condition> conditions = new ArrayList<>();
        Annotation[][] annotations = method.getParameterAnnotations();
        IntStream.range(0, annotations.length).forEach(index -> {
            Object parameter = arg[index];
            conditions.addAll(Stream.of(annotations[index]).map(annotation -> {
                if (((Column) annotation).sql().isEmpty()) {
                    return Condition.equalsTo(((Column) annotation).name(), parameter);
                }
                return Condition.in(((Column) annotation).name(), ((Column) annotation).sql(), parameter);
            }).collect(Collectors.toList()));
        });
        return conditions;
    }

}
