package com.ibeans.jdbc.ext.support.column;

import com.ibeans.jdbc.ext.exception.EmptyResultException;
import com.ibeans.jdbc.ext.support.Column;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class QueryColumn extends Column {

    private static final String SEPARATOR = ",";

    private final String sql;
    private final String sqlOptions;
    private final Object[] parameters;
    private final ValueSearch search;

    public QueryColumn(String identifier, String name, List<String> tables, String sql, String sqlOptions, Object parameter, ValueSearch search, Function<Object, Object> converter) {
        this(identifier, name, tables, sql, sqlOptions, new Object[]{parameter}, search, converter);
    }

    public QueryColumn(String identifier, String name, List<String> tables, String sql, Object[] parameters, ValueSearch search, Function<Object, Object> converter) {
        this(identifier, name, tables, sql, null, parameters, search, converter);
    }

    public QueryColumn(String identifier, String name, List<String> tables, String sql, String sqlOptions, Object[] parameters, ValueSearch search, Function<Object, Object> converter) {
        super(identifier, name, tables, converter);
        this.sql = sql;
        this.sqlOptions = sqlOptions;
        this.parameters = parameters;
        this.search = search;
    }

    @Override
    public Object getValue() {
        try {
            if (notHasParameter()) {
                return search.queryForObject(sql);
            }
            if (areNotNull()) {
                return search.queryForObject(sql, parameters);
            }
        } catch (EmptyResultException cause) {
            throwOptionsException();
            throw cause;
        }
        return null;
    }

    @Override
    public Object getNoConvertedValue() {
        return getValue();
    }

    private void throwOptionsException() {
        if (hasSqlOptions()) {
            List<Object> values = search.queryForList(sqlOptions);
            if (!values.isEmpty()) {
                throw new IllegalArgumentException("value not found for " + toString(parameters)
                        + ", but there are this values " + toString(values));
            }
        }
    }

    private String toString(List<Object> values) {
        return toString(values.stream());
    }

    private String toString(Object[] values) {
        return toString(Stream.of(values));
    }

    private String toString(Stream<Object> values) {
        return values.map(Object::toString)
                .collect(Collectors.joining(SEPARATOR));
    }

    private boolean notHasParameter() {
        return parameters != null && parameters.length == 0;
    }

    private boolean areNotNull() {
        return parameters != null && areNotNull(parameters);
    }

    private boolean hasSqlOptions() {
        return this.sqlOptions != null
                && !this.sqlOptions.isEmpty();
    }

    private boolean areNotNull(Object[] values) {
        for (Object value : values) {
            if (value == null) {
                return false;
            }
        }
        return true;
    }


}
