package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.google.common.collect.Lists;

import java.lang.reflect.Method;
import java.util.List;


public class HandlerChain implements Handler {

    private List<Handler> handlers;

    public HandlerChain(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        this.handlers = Lists.newArrayList(
                new InsertHandler(jdbcAdapter, proxyClass),
                new QueryHandler(jdbcAdapter, proxyClass),
                new DeleteHandler(jdbcAdapter, proxyClass),
                new UpdateHandler(jdbcAdapter, proxyClass),
                new DeletesHandler(jdbcAdapter, proxyClass));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        for (Handler handler : handlers) {
            if (handler.test(method)) {
                return handler.invoke(proxy, method, args);
            }
        }
        return null;
    }

    @Override
    public boolean test(Method method) {
        return false;
    }
}
