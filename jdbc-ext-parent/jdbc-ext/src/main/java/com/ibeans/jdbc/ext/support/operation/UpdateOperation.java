package com.ibeans.jdbc.ext.support.operation;

import com.ibeans.jdbc.ext.support.Sql;

import java.util.function.Function;


public class UpdateOperation implements Function<Sql, Object> {

    private final Updated updated;

    public UpdateOperation(Updated updated) {
        this.updated = updated;
    }

    @Override
    public Object apply(Sql sql) {
        update(sql);
        return null;
    }

    private void update(Sql sql) {
        if (sql.hasArgs()) {
            updated.update(sql.toSql(), sql.getParameter());
        } else {
            updated.update(sql.toSql());
        }
    }
}
