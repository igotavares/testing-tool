package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.annotation.*;
import com.ibeans.jdbc.ext.annotation.EnumMap;
import com.ibeans.jdbc.ext.handler.factory.RowMapperFactory;
import com.ibeans.jdbc.ext.support.InsertSql;
import com.ibeans.jdbc.ext.support.RowMapper;
import com.ibeans.jdbc.ext.support.Sql;
import com.ibeans.jdbc.ext.support.column.*;
import com.ibeans.jdbc.ext.support.converter.EnumConverterChain;
import com.ibeans.jdbc.ext.support.converter.NoConverter;
import com.ibeans.jdbc.ext.support.operation.InsertionOperation;
import com.ibeans.jdbc.ext.support.operation.insertion.MapResult;
import com.ibeans.jdbc.ext.support.operation.insertion.NullResult;
import com.ibeans.jdbc.ext.support.operation.insertion.RowMapperResult;
import com.ibeans.jdbc.ext.support.operation.insertion.SequenceResult;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class InsertHandler extends AbstractHandler {

    public InsertHandler(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        super(jdbcAdapter, proxyClass);
    }

    @Override
    public boolean test(Method method) {
        return method.getDeclaredAnnotation(Insert.class) != null;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return insert(method, args);
    }

    private Object insert(Method method, Object[] args) {
        Sql insert = createInsert(method, args);

        return insert.execute();
    }

    private Sql createInsert(Method method, Object[] args) {
        InsertionOperation operation = createInsertionOperation(method);
        Optional<String> insertSql = getInsertSql(method);
        if (insertSql.isPresent()) {
            return createSql(operation, insertSql, args);
        }
        return createInsertSql(method, operation, args);
    }

    private Sql createSql(InsertionOperation operation, Optional<String> insertSql, Object[] args) {
        Sql sql = new Sql(operation, insertSql.get());
        sql.addParameters(toDefaultParameter(args));
        return sql;
    }

    private Sql createInsertSql(Method method, InsertionOperation operation, Object[] args) {
        InsertSql insertSql = new InsertSql(operation, getNameTable());
        List<com.ibeans.jdbc.ext.support.Column> columns = new ArrayList<>();
        columns.addAll(createColumns(method, args));
        columns.addAll(createSequenceColumns(method));
        columns.addAll(createMethodColumn(method, columns));
        insertSql.addColumns(columns);
        return insertSql;
    }

    private Optional<String> getInsertSql(Method method) {
        return Optional.of(Insert.class)
                .map(method::getDeclaredAnnotation)
                .map(Insert::value)
                .filter(this::isNotEmpty);
    }

    private boolean isNotEmpty(String value) {
        return value != null && !value.isEmpty();
    }

    private InsertionOperation createInsertionOperation(Method method) {
        return new InsertionOperation(getJdbcAdapter(), createResult(method));
    }

    private Function<Sql, Object> createResult(Method method) {
        if (Long.class.isAssignableFrom(method.getReturnType())) {
            return new SequenceResult();
        }
        if (Map.class.isAssignableFrom(method.getReturnType())) {
            RowMapper rowMapper = createRowMapper(method);
            if (rowMapper.hasMapper()) {
                return new RowMapperResult(rowMapper);
            }
            return new MapResult();
        }
        return new NullResult();
    }


    protected RowMapper createRowMapper(Method method) {
        return RowMapperFactory.create(method);
    }

    private Collection<? extends com.ibeans.jdbc.ext.support.Column> createMethodColumn(Method method, List<com.ibeans.jdbc.ext.support.Column> columns) {
        Column column = method.getDeclaredAnnotation(Column.class);
        if (column != null) {
            return Arrays.asList(createMethodColumn(column, columns));
        }
        Columns columnsA = method.getDeclaredAnnotation(Columns.class);
        if (columnsA != null) {
            return Stream.of(columnsA.value())
                    .map(value -> createMethodColumn(value, columns))
                    .collect(Collectors.toList());

        }
        return new ArrayList<>();
    }

    private com.ibeans.jdbc.ext.support.Column createMethodColumn(Column column, List<com.ibeans.jdbc.ext.support.Column> columns) {
        if (!column.defaultValue().isEmpty()) {
            return new DefaultValueColumn(column.name(), getTables(column), column.defaultValue(), column.type());
        }
        return new NamedQueryColumn(column.name(), column.sql(), getTables(column), columns, getJdbcAdapter(), createConverter(column.map()));
    }

    private List<com.ibeans.jdbc.ext.support.Column> createSequenceColumns(Method method) {
        List<com.ibeans.jdbc.ext.support.Column> sequenceColumns = new ArrayList<>();

        Sequence sequence = method.getDeclaredAnnotation(Sequence.class);
        sequenceColumns.addAll(createSequenceColumns(sequence));

        Sequences sequences = method.getDeclaredAnnotation(Sequences.class);
        sequenceColumns.addAll(createSequenceColumns(sequences));

        return sequenceColumns;
    }

    private List<com.ibeans.jdbc.ext.support.Column> createSequenceColumns(Sequences sequences) {
        List<com.ibeans.jdbc.ext.support.Column> sequenceColumns = new ArrayList<>();
        if (sequences != null) {
            for (Sequence sequence : sequences.value()) {
                sequenceColumns.addAll(createSequenceColumns(sequence));
            }
        }
        return sequenceColumns;
    }

    private List<com.ibeans.jdbc.ext.support.Column> createSequenceColumns(Sequence sequence) {
        List<com.ibeans.jdbc.ext.support.Column> sequenceColumns = new ArrayList<>();
        if (sequence != null) {
            SequenceSearch search = new SequenceSearchCache(getJdbcAdapter());
            for (Column column : sequence.columns()) {
                sequenceColumns.add(new SequenceColumn(sequence.name(), column.name(), getTables(column), search));
            }
        }
        return sequenceColumns;
    }

    private List<com.ibeans.jdbc.ext.support.Column> createColumns(Method method, Object[] arguments) {
        Parameter[] parameters = method.getParameters();
        List<com.ibeans.jdbc.ext.support.Column> convertedColumns = new ArrayList<>();
        IntStream.range(0, parameters.length).forEach(index -> {
                    Parameter parameter = parameters[index];
                    Object argument = arguments[index];

                    Optional<Param> param = Optional.of(Param.class)
                            .map(parameter::getDeclaredAnnotation);

                    Optional<Stream<Column>> columns = Optional.of(Columns.class)
                            .map(parameter::getDeclaredAnnotation)
                            .map(Columns::value)
                            .map(Stream::of);

                    if (columns.isPresent()) {
                        columns.ifPresent(columnStream -> columnStream
                                .forEach(column -> {
                                    convertedColumns.add(createColumn(param, column, argument));
                                }));

                        return;
                    }
                    Column column = Optional.of(Column.class).map(parameter::getDeclaredAnnotation).orElse(null);
                    convertedColumns.add(createColumn(param, column, argument));
                });
        return convertedColumns;
    }

    private com.ibeans.jdbc.ext.support.Column createColumn(Optional<Param> param, Column column, Object argument) {
        String identifier = getIdentifier(param, column);
        return create(identifier, param, column, argument, new ValueSearchCache(getJdbcAdapter()));
    }

    public String getIdentifier(Optional<Param> param, Column column) {
        return param.map(Param::value).orElse(column != null ? column.name() : null);
    }

    private com.ibeans.jdbc.ext.support.Column create(String identifier, Optional<Param> param, Column column, Object argument, ValueSearch search) {
        if (param.isPresent() && column == null) {
            return new NoColumn(param.map(Param::value).get(),
                                argument,
                                param.map(Param::map).map(this::createConverter).orElse(null));
        }
        if (column == null) {
            return new ValueColumn(argument, new NoConverter());
        }
        if (!column.sql().isEmpty()) {
            return new QueryColumn(identifier, column.name(), getTables(column), column.sql(), column.sqlOptions(), argument, search, createConverter(column.map()));
        }
        return new ValueColumn(identifier, column.name(), getTables(column), argument, createConverter(column.map()));
    }


    private Function<Object, Object> createConverter(EnumMap[] enumMaps) {
        return Stream.of(enumMaps)
                .findFirst()
                .map(enumMap -> (Function<Object, Object>)
                        new EnumConverterChain(enumMap.map(), enumMap.from(), enumMap.to()))
                .orElse(new NoConverter());
    }

    private static List<String> getTables(Column column) {
        return Arrays.stream(column.table())
                .map(table -> table.value())
                .collect(Collectors.toList());
    }

}
