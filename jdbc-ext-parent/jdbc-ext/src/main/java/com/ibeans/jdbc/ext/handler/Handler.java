package com.ibeans.jdbc.ext.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.function.Predicate;



public interface Handler extends Predicate<Method>, InvocationHandler {

}
