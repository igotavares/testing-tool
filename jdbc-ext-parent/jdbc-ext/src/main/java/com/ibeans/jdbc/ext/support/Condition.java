package com.ibeans.jdbc.ext.support;


public class Condition implements Parameter {

    private static final String NO_SQL = null;

    private final String column;

    private final String type;

    private final String sql;

    private final Object value;

    public Condition(String column, String type, String sql, Object value) {
        this.column = column;
        this.type = type;
        this.sql = sql;
        this.value = value;
    }

    public static Condition equalsTo(String column, Object value) {
        return equalsTo(column, NO_SQL, value);
    }

    public static Condition equalsTo(String column, String sql, Object value) {
        return new Condition(column, "=", sql, value);
    }

    public static Condition in(String column, String sql, Object value) {
        return new Condition(column, "IN" , "(" + sql + ")", value);
    }

    @Override
    public String toString() {
        return  column + " " + type +  " " + (hasSql() ? sql : "?");
    }

    public Boolean hasSql() {
        return sql != null && !sql.isEmpty();
    }

    @Override
    public Boolean isParameter() {
        return true;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public Object getNoConvertedValue() {
        return this.value;
    }
}
