package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.annotation.OrderBy;
import com.ibeans.jdbc.ext.annotation.Query;
import com.ibeans.jdbc.ext.handler.factory.RowMapperFactory;
import com.ibeans.jdbc.ext.support.*;
import com.ibeans.jdbc.ext.support.operation.SearchOperation;

import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class QueryHandler extends AbstractHandler {

    private static final int FIRST = 0;

    public QueryHandler(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        super(jdbcAdapter, proxyClass);
    }

    @Override
    public boolean test(Method method) {
        Query query = method.getDeclaredAnnotation(Query.class);
        return query != null;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        return query(method, args);
    }

    private Object query(Method method, Object[] args) {
        Sql sql = createQuerySQL(method, args);

        return sql.execute();
    }

    private Function<List<Map<String, Object>>, Object> createResult(Method method) {
        if (List.class.isAssignableFrom(method.getReturnType())) {
            return (result) -> result;
        }
        if (Map.class.isAssignableFrom(method.getReturnType())) {
            return (result) -> {if (!result.isEmpty()) {
                                    return result.get(FIRST);
                                }
                                return null;
            };
        }
        return (result) -> null;
    }

    private Sql createQuerySQL(Method method, Object[] args) {
        SearchOperation searchOperation = new SearchOperation(getJdbcAdapter(), createRowMapper(method), createResult(method));

        return Optional.of(Query.class)
                .map(method::getDeclaredAnnotation)
                .map(query -> {
                    if (query.value().isEmpty()) {
                        return createQuerySql(method, args, searchOperation, query);
                    }
                    return createSql(args, searchOperation, query);
                }).get();
    }

    private Sql createQuerySql(Method method, Object[] args, SearchOperation searchOperation, Query query) {
        QuerySql querySql = new QuerySql(searchOperation, getNameTable(), createOrderings(query.orderBy()));
        querySql.addConditions(createConditions(method, args));
        return querySql;
    }

    private Sql createSql(Object[] args, SearchOperation searchOperation, Query query) {
        Sql sql = new Sql(searchOperation, query.value());
        sql.addParameters(toDefaultParameter(args));
        return sql;
    }

    private Orderings createOrderings(OrderBy[] orders) {
        return new Orderings(Stream.of(orders)
                .map(this::createOrding)
                .collect(Collectors.toList()));
    }

    private Ordering createOrding(OrderBy orderBy) {
        return new Ordering(orderBy.columns(), orderBy.type());
    }

    protected RowMapper createRowMapper(Method method) {
        return RowMapperFactory.create(method);
    }


}
