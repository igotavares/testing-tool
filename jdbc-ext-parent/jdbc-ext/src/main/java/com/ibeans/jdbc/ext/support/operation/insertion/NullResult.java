package com.ibeans.jdbc.ext.support.operation.insertion;

import com.ibeans.jdbc.ext.support.Sql;

import java.util.function.Function;


public class NullResult implements Function<Sql, Object> {

    @Override
    public Object apply(Sql sql) {
        return null;
    }

}
