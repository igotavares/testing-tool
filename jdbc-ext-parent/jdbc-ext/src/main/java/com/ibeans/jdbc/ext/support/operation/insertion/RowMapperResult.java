package com.ibeans.jdbc.ext.support.operation.insertion;

import com.ibeans.jdbc.ext.support.Column;
import com.ibeans.jdbc.ext.support.RowMapper;
import com.ibeans.jdbc.ext.support.Sql;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;


public class RowMapperResult  implements Function<Sql, Object> {

    private RowMapper rowMapper;

    public RowMapperResult(RowMapper rowMapper) {
        this.rowMapper = rowMapper;
    }

    @Override
    public Map<String, Object> apply(Sql sql) {
        return rowMapper.toMap(toMap(sql.getColumns()));
    }

    private Map<String,Object> toMap(List<Column> columns) {
        Map<String, Object> convertedColumns = new HashMap<>();
        columns.stream().forEach(column
                -> convertedColumns.put(column.getName(), column.getNoConvertedValue()));
        return convertedColumns;
    }
}
