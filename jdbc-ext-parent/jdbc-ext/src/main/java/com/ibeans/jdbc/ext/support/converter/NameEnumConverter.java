package com.ibeans.jdbc.ext.support.converter;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class NameEnumConverter extends EnumConverter {

    public NameEnumConverter(Class<? extends Enum<?>> enumClass, String from, String to) {
        super(enumClass, from, to);
    }

    @Override
    protected boolean search(Enum enumValue, Object value) {
        return  enumValue.ordinal() == ((Number) value).intValue();
    }

    @Override
    protected Object converter(Enum enumValue) {
        return enumValue.name();
    }

    @Override
    public boolean test(Object value) {
        return value != null && value instanceof Number;
    }

    @Override
    protected String getOptions() {
        return Stream.of(getEnumClass().getEnumConstants())
                .map(Enum::ordinal)
                .map(String::valueOf)
                .collect(Collectors.joining(","));
    }

}
