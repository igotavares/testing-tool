package com.ibeans.jdbc.ext.support.column;

import com.google.common.collect.Lists;
import com.ibeans.jdbc.ext.support.Column;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class NamedQueryColumn extends Column {

    private static final String PARAMETER_PATTERN = "(:)(\\w+)";
    private static final Integer PARAMETER_NAME = 2;
    private static final String PARAMETER = "?";

    private String namedQuery;
    private List<Column> columns;
    private final ValueSearch search;

    private QueryColumn queryColumn;

    public NamedQueryColumn(String name, String namedQuery, List<String> tables, List<Column> columns, ValueSearch search, Function<Object, Object> converter) {
        super(name, name, tables, converter);
        this.namedQuery = namedQuery;
        this.columns = columns;
        this.search = search;
    }

    @Override
    public Object getValue() {
        if (queryColumn == null) {
            queryColumn = createQueryColumn();
        }
        return queryColumn.getValue();
    }

    @Override
    public Object getNoConvertedValue() {
        return getValue();
    }

    private QueryColumn createQueryColumn() {
        return new QueryColumn(getIdentifier(), getName(), getTables(), toSql(), toParameters(), new ValueSearchCache(search), getConverter());
    }

    private String toSql() {
        return namedQuery.replaceAll(PARAMETER_PATTERN, PARAMETER);
    }

    private Object[] toParameters() {
        List<Object> parameters = Lists.newArrayList();

        Map<String, Object> valueByName = new LinkedHashMap<>();
        for (Column column : columns) {
            if (!this.equals(column) && !(column instanceof NamedQueryColumn)) {
                valueByName.put(column.getIdentifier(), column.getValue());
            }
        }

        Matcher matcher = Pattern
                .compile(PARAMETER_PATTERN)
                .matcher(namedQuery);

        while(matcher.find()) {
            String parameterName = matcher.group(PARAMETER_NAME);
            Object value = valueByName.get(parameterName.toUpperCase());
            parameters.add(value);
        }

        return parameters.toArray();
    }

}
