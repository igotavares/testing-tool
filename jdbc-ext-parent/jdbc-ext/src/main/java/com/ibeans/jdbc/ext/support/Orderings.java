package com.ibeans.jdbc.ext.support;


import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;


public class Orderings  {

    private static final String ORDER_BY = " ORDER BY ";
    private static final String DELIMITER = ", ";
    private static final String EMPTY = "";

    private final Collection<Ordering> orderings;

    public Orderings() {
        this(new ArrayList<>());
    }

    public Orderings(Collection<Ordering> orderings) {
        this.orderings = orderings;
    }

    private boolean hasOrdering() {
        return !orderings.isEmpty();
    }

    public String toSql() {
        if (hasOrdering()) {
            return ORDER_BY + orderings.stream().map(Ordering::toSql).collect(Collectors.joining(DELIMITER));
        }
        return EMPTY;
    }

}
