package com.ibeans.jdbc.ext.handler.factory;

import com.ibeans.jdbc.ext.annotation.EnumMap;
import com.ibeans.jdbc.ext.support.Mapper;
import com.ibeans.jdbc.ext.support.RowMapper;
import com.ibeans.jdbc.ext.support.converter.EnumConverterChain;
import com.ibeans.jdbc.ext.support.converter.NoConverter;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class RowMapperFactory {

    private Method method;

    private RowMapperFactory(Method method) {
        this.method = method;
    }

    public static RowMapper create(Method method) {
        return new RowMapperFactory(method).create();
    }

    public RowMapper create() {
        return new RowMapper(toMappers());
    }

    private List<Mapper> toMappers() {
        return Stream.of(getMappers())
                .map(this::createMapper)
                .collect(Collectors.toList());
    }

    private com.ibeans.jdbc.ext.annotation.Mapper[] getMappers() {
        return Optional.of(com.ibeans.jdbc.ext.annotation.RowMapper.class)
                .map(method::getDeclaredAnnotation)
                .map(com.ibeans.jdbc.ext.annotation.RowMapper::value)
                .orElse(new com.ibeans.jdbc.ext.annotation.Mapper[]{});
    }

    private Mapper createMapper(com.ibeans.jdbc.ext.annotation.Mapper mapper) {
        return new Mapper(getSql(mapper.sql()),
                mapper.name(),
                mapper.columns(),
                mapper.type(),
                createConverter(mapper.map()));
    }

    private Function<Object, Object> createConverter(EnumMap[] enumMaps) {
        return Stream.of(enumMaps)
                .findFirst()
                .map(enumMap -> (Function<Object, Object>)
                        new EnumConverterChain(enumMap.map(), enumMap.from(), enumMap.to()))
                .orElse(new NoConverter());
    }

    private String getSql(String sql) {
        if (sql == null || sql.isEmpty()) {
            return null;
        }
        return sql;
    }




}
