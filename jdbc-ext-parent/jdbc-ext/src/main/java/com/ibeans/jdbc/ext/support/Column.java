package com.ibeans.jdbc.ext.support;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;


@RequiredArgsConstructor
@Getter
public abstract class Column implements Parameter {

    private final String identifier;
    private final String name;
    private final List<String> tables;
    private final Function<Object, Object> converter;

    public boolean hasTables() {
        return tables != null && !tables.isEmpty();
    }

    public String getIdentifier() {
        if (identifier != null) {
            return identifier.toUpperCase();
        }
        return null;
    }

    public Boolean isParameter() {
        return true;
    }

    public boolean isNotEmptyIdentifier() {
        return identifier != null && !identifier.isEmpty();
    }

    public void forEachTable(Consumer<String> action) {
        tables.stream().forEach(action);
    }

}
