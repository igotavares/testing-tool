package com.ibeans.jdbc.ext.support.operation;


public interface Updated {

    void update(String sql);

    void update(String sql, Object[] args);

}
