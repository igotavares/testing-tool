package com.ibeans.jdbc.ext.support;


public class DefaultParameter implements Parameter {

    private final Object value;

    public DefaultParameter(Object value) {
        this.value = value;
    }

    @Override
    public Boolean isParameter() {
        return true;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public Object getNoConvertedValue() {
        return getValue();
    }
}
