package com.ibeans.jdbc.ext.support.column;

import com.ibeans.jdbc.ext.support.Column;
import com.ibeans.jdbc.ext.support.converter.NoConverter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class JsonColumn extends Column {

    private Map<String, Object> value;

    public JsonColumn(String identifier, String name, List<String> tables) {
        super(identifier, name, tables, new NoConverter());
        this.value = new LinkedHashMap();
    }

    public void add() {
    }

    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public Object getNoConvertedValue() {
        return null;
    }

}
