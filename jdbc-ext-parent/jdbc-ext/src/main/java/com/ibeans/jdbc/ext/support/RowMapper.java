package com.ibeans.jdbc.ext.support;

import lombok.Getter;
import lombok.ToString;

import java.util.*;
import java.util.function.Function;


@Getter
@ToString
public class RowMapper {

    private List<Mapper> mappers;

    public RowMapper() {
        this.mappers = new ArrayList<>();
    }

    public RowMapper(List<Mapper> mappers) {
        this.mappers = mappers;
    }

    public void addMappers(Collection<Mapper> mappers) {
        this.mappers.addAll(mappers);
    }

    public void addMapper(String name, String column, Type type, Function<Object, Object> converter) {
        addMapper(null, name, column, type, converter);
    }

    public void addMapper(String name, String[] columns, Type type, Function<Object, Object> converter) {
        addMapper(null, name, columns, type, converter);
    }

    public void addMapper(String sql, String name, String column, Type type, Function<Object, Object> converter) {
        addMapper(sql, name, new String[]{column}, type, converter);
    }

    public void addMapper(String sql, String name, String[] columns, Type type, Function<Object, Object> converter) {
        mappers.add(new Mapper(sql, name, columns, type, converter));
    }

    public Map<String,Object> toMap(Map<String,Object> result) {
        Map<String,Object> convertedResult = new LinkedHashMap<>();
        mappers.stream().forEach( mapper
                -> convertedResult.put(mapper.getName(), mapper.getValue(result)));
        return convertedResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RowMapper rowMapper = (RowMapper) o;
        return Objects.equals(mappers, rowMapper.mappers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mappers);
    }


    public Boolean hasMapper() {
        return !mappers.isEmpty();
    }
}
