package com.ibeans.jdbc.ext.support;


public interface Parameter {

    Boolean isParameter();

    Object getValue();

    Object getNoConvertedValue();

}
