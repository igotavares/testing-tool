package com.ibeans.jdbc.ext.exception;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class IncorrectResultException extends RuntimeException {

    private final String sql;
    private final Object[] parameters;

    public IncorrectResultException(String sql, Throwable cause) {
        this(sql, new Object[]{}, cause);
    }

    public IncorrectResultException(String sql, Object[] parameters, Throwable cause) {
        super(cause);
        this.sql = sql;
        this.parameters = parameters;
    }

    @Override
    public String getMessage() {
        return "Incorrect Result for the (" + sql + ")SQL" + (hasParameter() ? parametersToString() : "");
    }

    private boolean hasParameter() {
        return parameters != null && parameters.length != 0;
    }

    private String parametersToString() {
        return Stream.of(parameters)
                .map(Object::toString)
                .collect(Collectors.joining(", "," with ",""));
    }

}
