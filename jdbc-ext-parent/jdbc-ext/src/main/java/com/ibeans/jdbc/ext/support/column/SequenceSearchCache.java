package com.ibeans.jdbc.ext.support.column;


public class SequenceSearchCache implements SequenceSearch {

    private Boolean firstTime = Boolean.TRUE;
    private Long value;
    private final SequenceSearch search;

    public SequenceSearchCache(SequenceSearch search) {
        this.search = search;
    }

    public Long nextSequence(String name) {
        if (firstTime) {
            value = search.nextSequence(name);
            firstTime = Boolean.FALSE;
        }
        return value;
    }

}
