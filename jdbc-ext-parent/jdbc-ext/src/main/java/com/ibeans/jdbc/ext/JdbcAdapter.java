package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.support.column.SequenceSearch;
import com.ibeans.jdbc.ext.support.column.ValueSearch;
import com.ibeans.jdbc.ext.support.operation.Searching;
import com.ibeans.jdbc.ext.support.operation.Updated;


public interface JdbcAdapter extends ValueSearch, SequenceSearch, Searching, Updated {

}
