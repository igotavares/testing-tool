package com.ibeans.jdbc.ext.support.column;


public interface SequenceSearch {

    Long nextSequence(String name);

}
