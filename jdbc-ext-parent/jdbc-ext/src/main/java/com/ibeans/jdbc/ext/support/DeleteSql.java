package com.ibeans.jdbc.ext.support;

import java.util.function.Function;


public class DeleteSql extends Sql {

    public DeleteSql(Function<Sql, ?> operation, String table) {
        super(operation, table);
    }

    @Override
    public String toSql() {
        return "DELETE FROM " + super.toSql();
    }


}
