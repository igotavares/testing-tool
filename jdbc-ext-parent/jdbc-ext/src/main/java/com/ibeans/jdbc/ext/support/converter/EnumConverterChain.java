package com.ibeans.jdbc.ext.support.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class EnumConverterChain implements Function<Object, Object> {

    private List<EnumConverter> converters;

    public EnumConverterChain(Class<? extends Enum<?>> enumClass, String from, String to) {
        this.converters = new ArrayList<>();
        converters.add(new PropertyEnumConverter(enumClass, from, to));
        converters.add(new OrdinalEnumConverter(enumClass, from, to));
        converters.add(new NameEnumConverter(enumClass, from, to));
    }

    @Override
    public Object apply(Object value) {
        return converters.stream()
                .filter(converter -> converter.test(value))
                .map(converter -> converter.apply(value))
                .findFirst()
                .orElse(value);
    }

}
