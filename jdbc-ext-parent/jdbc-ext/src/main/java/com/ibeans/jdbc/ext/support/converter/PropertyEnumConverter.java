package com.ibeans.jdbc.ext.support.converter;

import java.lang.reflect.Method;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class PropertyEnumConverter extends EnumConverter {

    public PropertyEnumConverter(Class<? extends Enum<?>> enumClass, String from, String to) {
        super(enumClass, from, to);
    }

    @Override
    protected boolean search(Enum enumValue, Object value) {
        return getFrom(enumValue).equals(value);
    }

    @Override
    protected Object converter(Enum enumValue) {
        return get(enumValue, getTo());
    }

    protected String getOptions() {
        return Stream.of(getEnumClass().getEnumConstants())
                .map(this::getFrom)
                .map(Object::toString)
                .collect(Collectors.joining(","));
    }

    private Object getFrom(final Enum<?> type) {
        return get(type, getFrom());
    }

    private Object get(final Enum<?> type, final String attribute) {
        final String methodName = "get" + String.valueOf(attribute.charAt(0)).toUpperCase() + attribute.substring(1);
        try {
            Method method = getEnumClass().getMethod(methodName, new Class[] {});
            return method.invoke(type, new Object[] {});
        } catch (Exception cause) {
            throw new IllegalArgumentException("Error getting value " + attribute + " of the enum attribute " + type);
        }
    }


    @Override
    public boolean test(Object value) {
        return value != null && hasTo();
    }
}
