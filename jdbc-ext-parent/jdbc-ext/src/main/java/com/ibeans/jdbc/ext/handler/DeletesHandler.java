package com.ibeans.jdbc.ext.handler;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.annotation.Deletes;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.stream.Stream;


public class DeletesHandler extends AbstractHandler {

    private DeleteHandler deleteHandler;

    public DeletesHandler(JdbcAdapter jdbcAdapter, Class<?> proxyClass) {
        super(jdbcAdapter, proxyClass);
        deleteHandler = new DeleteHandler(jdbcAdapter, proxyClass);
    }

    @Override
    public boolean test(Method method) {
        Deletes delete = method.getDeclaredAnnotation(Deletes.class);
        return delete != null;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Optional.of(Deletes.class)
                .map(method::getDeclaredAnnotation)
                .map(Deletes::value)
                .ifPresent(deletes -> Stream.of(deletes)
                        .forEach(delete ->  deleteHandler.delete(Optional.of(delete), method, args)));

        return null;
    }

}
