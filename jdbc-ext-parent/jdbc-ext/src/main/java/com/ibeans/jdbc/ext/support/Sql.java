package com.ibeans.jdbc.ext.support;

import com.ibeans.jdbc.ext.support.column.SequenceColumn;
import com.ibeans.jdbc.ext.support.converter.JodaConverter;
import com.google.common.collect.Lists;
import lombok.Getter;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Sql {

    @Getter
    private final String sql;

    private final Function<Sql, ?> operation;

    @Getter
    private final List<Column> columns;

    private final List<Condition> conditions;

    private final List<Parameter> parameters;

    public Sql(Function<Sql, ?> operation, String sql) {
        this.sql = sql;
        this.operation = operation;
        this.columns = Lists.newArrayList();
        this.conditions = Lists.newArrayList();
        this.parameters = Lists.newArrayList();
    }

    public void addColumns(Collection<Column> columns) {
        addParameters(columns);
        this.columns.addAll(columns);
    }

    public void addConditions(Collection<Condition> conditions) {
        addParameters(conditions);
        this.conditions.addAll(conditions);
    }

    public void addParameters(Collection<? extends Parameter> parameters) {
        if (parameters != null) {
            this.parameters.addAll(parameters.stream()
                    .filter(Parameter::isParameter)
                    .collect(Collectors.toList()));
        }
    }

    public String toSql() {
        return sql + toWhereSQL();
    }

    public String toWhereSQL() {
        if (!conditions.isEmpty()) {
            return " WHERE " + conditions.stream()
                    .map(Condition::toString)
                    .collect(Collectors.joining(" AND "));
        }
        return "";
    }

    public boolean hasArgs() {
        return getParameter().length > 0;
    }

    public Object[] getParameter() {
        return parameters.stream()
                .map(Parameter::getValue)
                .map(JodaConverter::to)
                .toArray();
    }

    public <T> T execute() { return (T) operation.apply(this); }

    public Long getSequence() {
        return columns.stream()
                .filter((column -> column instanceof SequenceColumn))
                .map(column -> (Long) column.getValue())
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Sequence not found!"));
    }

}
