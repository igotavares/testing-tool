package com.ibeans.jdbc.ext.support.operation;

import com.ibeans.jdbc.ext.support.Sql;

import java.util.function.Function;


public class NullOperation implements Function<Sql, Object> {

    @Override
    public Object apply(Sql sql) {
        return null;
    }

}
