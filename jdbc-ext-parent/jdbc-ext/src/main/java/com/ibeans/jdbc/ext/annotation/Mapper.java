package com.ibeans.jdbc.ext.annotation;

import com.ibeans.jdbc.ext.support.Type;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE})
public @interface Mapper {

    String sql() default "";
    String[] columns();
    String name();
    Type type() default Type.DEFAULT;
    EnumMap[] map() default {};

}
