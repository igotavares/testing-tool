package com.ibeans.jdbc.ext.support;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;


@Getter
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public class Mapper {

    private final String sql;
    private final String name;
    private final String[] columns;
    private final Type type;
    private final Function<Object, Object> converter;

    public boolean hasSql() {
        return sql != null;
    }

    public String getColumn() {
        if (columns.length == 1) {
            return columns[0];
        }
        throw new RuntimeException("There should only be one column");
    }

    public Object getValue(Map<String, Object> values) {
        if (values != null) {
            return Stream.of(columns)
                    .filter(column -> values.get(column) != null)
                    .findFirst()
                    .map(values::get)
                    .map(type::to)
                    .orElse(null);
        }
        return null;
    }

}
