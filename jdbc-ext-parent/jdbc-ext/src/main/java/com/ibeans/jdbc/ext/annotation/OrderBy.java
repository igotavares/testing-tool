package com.ibeans.jdbc.ext.annotation;

import com.ibeans.jdbc.ext.support.OrderingType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE})
public @interface OrderBy {

    String[] columns();
    OrderingType type() default OrderingType.ASC;

}
