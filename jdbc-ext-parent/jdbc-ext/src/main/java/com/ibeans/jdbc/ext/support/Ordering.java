package com.ibeans.jdbc.ext.support;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Ordering {

    private static final String NO_PREFIX = "";
    private static final String SEPARATOR = " ";
    private static final String DELIMITER = ", ";
    private String[] columns;
    private OrderingType orderingType;

    public Ordering(String[] columns, OrderingType orderingType) {
        this.columns = columns;
        this.orderingType = orderingType;
    }

    public String toSql() {
        return Stream.of(columns).collect(
                Collectors.joining(DELIMITER, NO_PREFIX, SEPARATOR + orderingType.name()));
    }

}
