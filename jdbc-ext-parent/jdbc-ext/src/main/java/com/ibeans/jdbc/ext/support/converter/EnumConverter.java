package com.ibeans.jdbc.ext.support.converter;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;


public abstract class EnumConverter implements Function<Object, Object>, Predicate<Object> {

    private final Class<? extends Enum<?>> enumClass;
    private final String from;
    private final String to;

    public EnumConverter(Class<? extends Enum<?>> enumClass, String from, String to) {
        this.enumClass = enumClass;
        this.from = from;
        this.to = to;
    }

    public Class<? extends Enum<?>> getEnumClass() {
        return enumClass;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    protected boolean hasFrom() {
        return isNotEmpty(from);
    }

    protected boolean hasTo() {
        return isNotEmpty(to);
    }

    private boolean isNotEmpty(String value) {
        return value != null && !value.isEmpty();
    }

    @Override
    public Object apply(Object value) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(enumValue -> search(enumValue, value))
                .map(this::converter)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("value not found by " + value + ", but there are this values " + getOptions()));
    }

    protected abstract boolean search(Enum enumValue, Object value);

    protected abstract Object converter(Enum enumValue);

    protected abstract String getOptions();


}
