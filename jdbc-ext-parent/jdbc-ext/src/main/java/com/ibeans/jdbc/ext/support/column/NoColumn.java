package com.ibeans.jdbc.ext.support.column;

import java.util.function.Function;


public class NoColumn extends ValueColumn {

    private static final String NAME_IS_NULL = null;

    public NoColumn(String identifier, Object value, Function<Object, Object> converter) {
        super(identifier, NAME_IS_NULL, value, converter);
    }

    @Override
    public Boolean isParameter() {
        return false;
    }

}
