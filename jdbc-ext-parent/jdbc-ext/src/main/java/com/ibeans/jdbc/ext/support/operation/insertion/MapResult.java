package com.ibeans.jdbc.ext.support.operation.insertion;

import com.ibeans.jdbc.ext.support.Column;
import com.ibeans.jdbc.ext.support.Sql;
import com.ibeans.jdbc.ext.support.column.SequenceColumn;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


public class MapResult implements Function<Sql, Object> {

    @Override
    public Map<String, Object> apply(Sql sql) {
        return sql.getColumns()
                .stream().filter((column -> column instanceof SequenceColumn))
                .collect(Collectors.toMap(Column::getName, Column::getNoConvertedValue));
    }

}
