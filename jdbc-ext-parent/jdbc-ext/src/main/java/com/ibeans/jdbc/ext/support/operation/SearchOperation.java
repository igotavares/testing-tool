package com.ibeans.jdbc.ext.support.operation;

import com.ibeans.jdbc.ext.support.RowMapper;
import com.ibeans.jdbc.ext.support.Sql;

import java.util.List;
import java.util.Map;
import java.util.function.Function;


public class SearchOperation implements Function<Sql, Object> {

    private final Searching searching;
    private RowMapper rowMapper;
    private Function<List<Map<String, Object>>, Object> result;

    public SearchOperation(Searching searching, RowMapper rowMapper, Function<List<Map<String, Object>>, Object> result) {
        this.searching = searching;
        this.rowMapper = rowMapper;
        this.result = result;
    }

    @Override
    public Object apply(Sql sql) {
        return result.apply(search(sql));
    }

    private List<Map<String, Object>> search(Sql sql) {
        if (sql.hasArgs()) {
            return searching.query(sql.toSql(), sql.getParameter(), rowMapper);
        }
        return searching.query(sql.toSql(), rowMapper);
    }

}
