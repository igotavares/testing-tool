package com.ibeans.jdbc.ext.support;

import com.ibeans.jdbc.ext.support.operation.InsertionOperation;

import java.util.stream.Collectors;


public class InsertSql extends Sql {

    public InsertSql(InsertionOperation operation, String table) {
        super(operation, table);
    }

    @Override
    public String toSql() {
        return "INSERT INTO " + super.toSql() + " (" + toColumns() + ") VALUES (" + toValues() + ")" ;
    }

    public String toColumns() {
        return getColumns().stream()
                .filter(Column::isParameter)
                .map(Column::getName)
                .collect(Collectors.joining(","));
    }

    public String toValues() {
        return getColumns().stream()
                .filter(Column::isParameter)
                .map((column) -> "?")
                .collect(Collectors.joining(","));
    }

}
