package com.ibeans.jdbc.ext.support;


public enum OrderingType {
    ASC,
    DESC
}
