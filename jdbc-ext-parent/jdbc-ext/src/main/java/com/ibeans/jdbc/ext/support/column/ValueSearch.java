package com.ibeans.jdbc.ext.support.column;

import java.util.List;


public interface ValueSearch {

    Object queryForObject(final String sql, final Object[] parameter);

    Object queryForObject(String sql);

    List<Object> queryForList(String sqlOptions);

}
