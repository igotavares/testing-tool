package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.annotation.Update;


public interface UpdateOperation {

    @Update("UPDATE JDBC SET NAME = ? WHERE CODE = ?")
    void update(String name, String code);

}
