package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.support.RowMapper;
import com.ibeans.jdbc.ext.support.Type;
import com.ibeans.jdbc.ext.support.converter.NoConverter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.ibeans.jdbc.ext.JdbcExtContants.CODE_IS_A;
import static com.ibeans.jdbc.ext.JdbcExtContants.NAME_IS_B;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class SearchOperationTest extends AbstractOperationTest<SearchOperation> {

    @Before
    public void context() throws Exception {
        context(SearchOperation.class);
    }

    @Test
    public void whenQueryWithOrdering_thenShouldQuerySQLAndMapper() {
        operation.findWithOrdering();

        String sqlExpected = "SELECT * FROM JDBC ORDER BY CODE, NAME ASC, VALUE DESC";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("value", "VALUE", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), argThat(expected));
    }

    @Test
    public void whenQuerySql_thenShouldQuerySQLAndMapper() {
        operation.findSQLAll();

        String sqlExpected = "SELECT * FROM JDBCSQL";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), argThat(expected));
    }

    @Test
    public void givenCodeIsA_whenQuery_thenShouldQuerySQLAndMapper() {
        operation.find(CODE_IS_A);

        String sqlExpected = "SELECT * FROM JDBC WHERE CODE = ?";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), eq(new Object[]{CODE_IS_A}), argThat(expected));
    }

    @Test
    public void givenCodeIsAAndNameIsB_whenQuery_thenShouldQuerySQLAndMapper() {
        operation.find(CODE_IS_A, NAME_IS_B);

        String sqlExpected = "SELECT * FROM JDBC WHERE CODE = ? AND NAME = ?";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), eq(new Object[]{CODE_IS_A, NAME_IS_B}), argThat(expected));
    }

    @Test
    public void givenCodeIsA_whenQuerySQL_thenShouldQuerySQLAndMapper() {
        operation.findSQL(CODE_IS_A);

        String sqlExpected = "SELECT * FROM JDBCSQL WHERE CODE = ?";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), eq(new Object[]{CODE_IS_A}), argThat(expected));
    }

    @Test
    public void givenReturn_whenQuerySql_thenShouldQuerySQLAndMapper() {
        Map<String, Object> result = Maps.newLinkedHashMap();
        result.put("code", "A");
        result.put("name", "B");

        when(jdbcAdapterMock.query(any(), any())).thenReturn(Lists
                .newArrayList(result));

        List<Map<String, Object>> actual = operation.findSQLAll();

        Map<String, Object> expected = Maps.newLinkedHashMap();
        expected.put("code", "A");
        expected.put("name", "B");

        assertEquals(Lists.newArrayList(expected), actual);
    }

    @Test
    public void whenQuery_thenShouldQuerySQLAndMapper() {
        operation.findAll();

        String sqlExpected = "SELECT * FROM JDBC";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), argThat(expected));
    }

    @Test
    public void whenQueryWithSubSelect_thenShouldQuerySQLAndMapper() {
        operation.findSub(NAME_IS_B);

        String sqlExpected = "SELECT * FROM JDBC WHERE CODE IN (SELECT CODE FROM SUBJDBC WHERE NAME = ?)";

//        RowMapper rowMapperExpected = new RowMapper();
//        rowMapperExpected.addMapper("code", "CODE", Type.DEFAULT, new NoConverter());
//        rowMapperExpected.addMapper("name", "NAME", Type.DEFAULT, new NoConverter());
//
//        verify(jdbcAdapterMock).query(sqlExpected, new Object[]{NAME_IS_B}, rowMapperExpected);

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), new Object[]{NAME_IS_B}, argThat(expected));
    }

    @Test
    public void whenFindMapperWithSql_thenShouldQuerySQLAndMapper() {
        operation.findMapperWithSql();

        String sqlExpected = "SELECT * FROM JDBC";

        Matcher<RowMapper> expected = hasProperty("mappers", contains(
                createMapperMatcher("SELECT NAME FROM SQLJDBC WHERE CODE = ?", "code", "CODE", Type.DEFAULT, NoConverter.class),
                createMapperMatcher("name", "NAME", Type.DEFAULT, NoConverter.class)
        ));

        verify(jdbcAdapterMock).query(eq(sqlExpected), argThat(expected));
    }

    @Test
    public void givenReturn_whenQuery_thenShouldQuerySQLAndMapper() {
        Map<String, Object> result = Maps.newLinkedHashMap();
        result.put("code", "A");
        result.put("name", "B");

        when(jdbcAdapterMock.query(any(), any())).thenReturn(Lists
                .newArrayList(result));

        List<Map<String, Object>> actual = operation.findAll();

        Map<String, Object> expected = Maps.newLinkedHashMap();
        expected.put("code", "A");
        expected.put("name", "B");

        assertEquals(Lists.newArrayList(expected), actual);
    }

    @Test
    public void givenReturnMap_whenFind_thenShouldQuerySQLAndMapper() {
        Map<String, Object> result = Maps.newLinkedHashMap();
        result.put("code", "A");
        result.put("name", "B");

        when(jdbcAdapterMock.query(any(), any())).thenReturn(Lists
                .newArrayList(result));

        Map<String, Object> actual = operation.findMap();

        Map<String, Object> expected = Maps.newLinkedHashMap();
        expected.put("code", "A");
        expected.put("name", "B");

        assertEquals(expected, actual);
    }

    @Test
    public void shouldFindWithEnumColumn() {
        Map<String, Object> result = Maps.newLinkedHashMap();
        result.put("code", "A");
        result.put("name", "B");

        when(jdbcAdapterMock.query(any(), any())).thenReturn(Lists
                .newArrayList(result));

        Map<String, Object> actual = operation.findWithEnumColumn();

        Map<String, Object> expected = Maps.newLinkedHashMap();
        expected.put("code", "A");
        expected.put("name", "B");

        assertEquals(expected, actual);
    }

    public Matcher createMapperMatcher(String name, String column, Type type,
                                       Class<? extends Function<Object, Object>> converterClass) {
        return createMapperMatcher(null, name, new String[]{column}, type, converterClass);
    }

    public Matcher createMapperMatcher(String sql, String name, String column, Type type,
                                       Class<? extends Function<Object, Object>> converterClass) {
        return createMapperMatcher(sql, name, new String[]{column}, type, converterClass);
    }

    public Matcher createMapperMatcher(String sql, String name, String[] columns, Type type,
                                       Class<? extends Function<Object, Object>> converterClass) {
        return allOf(
                hasProperty("sql", equalTo(sql)),
                hasProperty("name", equalTo(name)),
                hasProperty("columns", equalTo(columns)),
                hasProperty("type", equalTo(type)),
                hasProperty("converter", isA(converterClass))
        );
    }

}
