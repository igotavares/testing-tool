package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.annotation.Column;
import com.ibeans.jdbc.ext.annotation.Delete;
import com.ibeans.jdbc.ext.annotation.Deletes;
import com.ibeans.jdbc.ext.annotation.Table;


@Table("JDBC")
public interface DeletionOperation extends Deleted {

    @Delete("DELETE FROM JDBCSQL")
    void deleteSql();

    @Delete("DELETE FROM JDBCSQL WHERE CODE = ?")
    void deleteSql(String code);

    @Delete
    void delete(@Column(name = "CODE") String code);

    @Delete
    void delete(@Column(name = "CODE") String code, @Column(name = "NAME") String name);

    @Deletes({@Delete, @Delete("DELETE FROM JDBCSQL")})
    void deletes();

}
