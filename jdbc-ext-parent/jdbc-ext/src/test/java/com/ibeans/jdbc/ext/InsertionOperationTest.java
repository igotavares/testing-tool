package com.ibeans.jdbc.ext;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

import static com.ibeans.jdbc.ext.JdbcExtContants.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class InsertionOperationTest extends AbstractOperationTest<InsertionOperation> {

    @Before
    public void context() throws Exception {
        context(InsertionOperation.class);
    }

    @Test
    public void givenCodeIsAAndNameIsB_whenInsertSQL_thenShouldInsert() {
        operation.insertSQL(CODE_IS_A, NAME_IS_B);

        String sqlExpected = "INSERT INTO JDBC (CODE, NAME) VALUES (?, ?)" ;

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, NAME_IS_B});
    }

    @Test
    public void givenCodeIsAAndNameIsB_whenInsert_thenShouldInsert() {
        operation.insert(CODE_IS_A, NAME_IS_B);

        String sqlExpected = "INSERT INTO JDBC (CODE,NAME) VALUES (?,?)" ;

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, NAME_IS_B});
    }

    @Test
    public void whenInsertWithInsert_thenShouldInsert() {
        when(jdbcAdapterMock.nextSequence(any())).thenReturn(ID_IS_ONE);

        operation.insertWithSequence(CODE_IS_A, NAME_IS_B);

        String sqlExpected = "INSERT INTO JDBC (CODE,NAME,ID) VALUES (?,?,?)" ;

        verify(jdbcAdapterMock).nextSequence("SEQ_ID");
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, NAME_IS_B, ID_IS_ONE});
    }

    @Test
    public void whenInsertWithMethodColumn_thenShouldInsert() {
        operation.insertWithMethodColumn(CODE_IS_A, NAME_IS_B);

        String sqlExpected = "INSERT INTO JDBC (CODE,NAME,STATUS) VALUES (?,?,?)" ;

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, NAME_IS_B, STATUS_IS_ZERO});
    }

    @Test
    public void whenInsertWithMethodColumns_thenShouldInsert() {
        when(jdbcAdapterMock.queryForObject(any())).thenReturn(DESCRIPTION_IS_C);

        operation.insertWithMethodColumns(CODE_IS_A);

        String queryExpected = "SELECT DESCRIPTION FROM TABLE_TWO" ;
        String sqlExpected = "INSERT INTO JDBC (CODE,STATUS,DESCRIPTION) VALUES (?,?,?)" ;

        verify(jdbcAdapterMock).queryForObject(queryExpected);
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, STATUS_IS_ZERO, DESCRIPTION_IS_C});
    }

    @Test
    public void whenInsertWithFind_thenShouldFindObject() {
        operation.insertWithFind(NAME_IS_A, JDBC_NAME_IS_B);

        verify(jdbcAdapterMock).queryForObject("SELECT CODE FROM NAME = ?", new Object[]{NAME_IS_A});
    }

    @Test
    public void givenNameIsNull_whenInsertWithFind_thenShouldNotFindObject() {
        operation.insertWithFind(NAME_IS_NULL, JDBC_NAME_IS_B);

        verify(jdbcAdapterMock,never()).queryForObject("SELECT CODE FROM NAME = ?", new Object[]{NAME_IS_A});
    }

    @Test
    public void givenFindC_whenInsertWithFind_thenShouldFindObject() {
        when(jdbcAdapterMock.nextSequence(any())).thenReturn(ID_IS_ONE);
        when(jdbcAdapterMock.queryForObject(any(), any())).thenReturn(CODE_IS_C);

        operation.insertWithFind(NAME_IS_A, JDBC_NAME_IS_B);

        String sqlExpected = "INSERT INTO JDBC (CODE,NAME,ID) VALUES (?,?,?)" ;

        verify(jdbcAdapterMock).nextSequence("SEQ_ID");
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_C, NAME_IS_B, ID_IS_ONE});
    }

    @Test
    @Ignore
    public void whenInsertWithTwoTable_thenShouldInsert() {
        when(jdbcAdapterMock.nextSequence(any())).thenReturn(ID_IS_ONE);
        when(jdbcAdapterMock.queryForObject(any(), any())).thenReturn(CODE_IS_A);

        operation.insertWithTwoTable(CODE_IS_A, NAME_IS_B);

        String sqlExpected = "INSERT INTO JDBC (CODE,ID) VALUES (?,?)" ;
        String sqlTwoExpected = "INSERT INTO TWO_JDBC (NAME,ID) VALUES (?,?)" ;

        verify(jdbcAdapterMock).nextSequence("SEQ_ID");
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, ID_IS_ONE});
        verify(jdbcAdapterMock).update(sqlTwoExpected, new Object[]{NAME_IS_B, ID_IS_ONE});
    }

    @Test
    public void whenInsertWithInsert_thenShouldReturnOne() {
        when(jdbcAdapterMock.nextSequence(any())).thenReturn(ID_IS_ONE);

        Long actual = operation.insertWithSequence(CODE_IS_A, NAME_IS_B);

        Long expected = 1L;

        assertEquals(expected, actual);
    }

    @Test
    public void whenInsert() {
        when(jdbcAdapterMock.queryForObject(any(), any())).thenReturn(DESCRIPTION_IS_C);

        String sqlExpected = "INSERT INTO JDBC (CODE,NAME,DESCRIPTION) VALUES (?,?,?)" ;
        String sqlTwoExpected = "SELECT DESCRIPTION FROM TABLE_TWO WHERE CODE = ? AND NAME = ?" ;

        operation.insertWithColumn(CODE_IS_A, NAME_IS_B);

        verify(jdbcAdapterMock).queryForObject(sqlTwoExpected, new Object[]{CODE_IS_A, NAME_IS_B});
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, NAME_IS_B, DESCRIPTION_IS_C});
    }

    @Test
    public void whenSave() {
        when(jdbcAdapterMock.nextSequence("SEQ_ID")).thenReturn(ID_IS_ONE);
        when(jdbcAdapterMock.nextSequence("SEQ_STATIC")).thenReturn(STATIC_IS_TWO);

        String sqlExpected = "INSERT INTO JDBC (ID,STATIC) VALUES (?,?)" ;

        Map<String,Object> result = operation.save();

        verify(jdbcAdapterMock).nextSequence("SEQ_ID");
        verify(jdbcAdapterMock).nextSequence("SEQ_STATIC");

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{ID_IS_ONE, STATIC_IS_TWO});

        Map<String, Object> expected = new HashedMap();
        expected.put("ID", ID_IS_ONE);
        expected.put("STATIC", STATIC_IS_TWO);

        assertEquals(expected, result);
    }

    @Test
    public void whenSaveWithRowMapper() {
        when(jdbcAdapterMock.nextSequence("SEQ_ID")).thenReturn(ID_IS_ONE);
        when(jdbcAdapterMock.nextSequence("SEQ_STATIC")).thenReturn(STATIC_IS_TWO);

        String sqlExpected = "INSERT INTO JDBC (CODE,ID,STATIC) VALUES (?,?,?)" ;

        Map<String,Object> result = operation.saveWithRowMapper(CODE_IS_A);

        verify(jdbcAdapterMock).nextSequence("SEQ_ID");
        verify(jdbcAdapterMock).nextSequence("SEQ_STATIC");

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, ID_IS_ONE, STATIC_IS_TWO});

        Map<String, Object> expected = new HashedMap();
        expected.put("pk", ID_IS_ONE);
        expected.put("code", CODE_IS_A);

        assertEquals(expected, result);
    }

    @Test
    public void whenInsertWithValueParameter() {
        when(jdbcAdapterMock.queryForObject(any(), any())).thenReturn(DESCRIPTION_IS_C);

        String sqlExpected = "INSERT INTO JDBC (CODE,DESCRIPTION) VALUES (?,?)" ;
        String sqlTwoExpected = "SELECT DESCRIPTION FROM TABLE_TWO WHERE CODE = ? AND NAME = ?" ;

        operation.insertWithParam(CODE_IS_A, NAME_IS_B);

        verify(jdbcAdapterMock).queryForObject(sqlTwoExpected, new Object[]{CODE_IS_A, NAME_IS_B});
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, DESCRIPTION_IS_C});
    }

    @Test
    public void givenValueParameterIsNull_whenInsertWithValueParameter() {
        String sqlExpected = "INSERT INTO JDBC (CODE,DESCRIPTION) VALUES (?,?)" ;
        String sqlTwoExpected = "SELECT DESCRIPTION FROM TABLE_TWO WHERE CODE = ? AND NAME = ?" ;

        operation.insertWithParam(CODE_IS_A, NAME_IS_NULL);

        verify(jdbcAdapterMock, never()).queryForObject(sqlTwoExpected, new Object[]{CODE_IS_A, NAME_IS_B});
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{CODE_IS_A, NAME_IS_NULL});
    }

    @Test
    public void whenInsertTwoColumns() {
        when(jdbcAdapterMock.queryForObject(any(), any())).thenReturn(DESCRIPTION_IS_C);

        String sqlExpected = "INSERT INTO JDBC (NAME,DESCRIPTION) VALUES (?,?)" ;
        String sqlTwoExpected = "SELECT DESCRIPTION FROM JDBC WHERE NAME = ?" ;

        operation.insertTwoColumns(NAME_IS_B);

        verify(jdbcAdapterMock).queryForObject(sqlTwoExpected, new Object[]{NAME_IS_B});
        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{NAME_IS_B, DESCRIPTION_IS_C});
    }


    @Test
    public void whenInsertEumColumn() {
        String sqlExpected = "INSERT INTO JDBC (CODE,NAME) VALUES (?,?)" ;

        operation.insertWithEnum("TYPE_TWO", 0);

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{1, "TYPE_ONE"});
    }

    @Test
    public void whenInsertPropertyEumColumn() {
        String sqlExpected = "INSERT INTO JDBC (CODE,NAME) VALUES (?,?)" ;

        operation.insertWithPropertyEnum("TWO", 1);

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{2, "ONE"});
    }

}
