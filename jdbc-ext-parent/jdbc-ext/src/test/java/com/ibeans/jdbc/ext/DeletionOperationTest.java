package com.ibeans.jdbc.ext;

import org.junit.Before;
import org.junit.Test;

import static com.ibeans.jdbc.ext.JdbcExtContants.CODE_IS_A;
import static com.ibeans.jdbc.ext.JdbcExtContants.NAME_IS_B;
import static org.mockito.Mockito.verify;


public class DeletionOperationTest extends AbstractOperationTest<DeletionOperation> {

    @Before
    public void context() throws Exception {
        context(DeletionOperation.class);
    }

    @Test
    public void thenShouldGenerateDeleteSQL() {
        operation.delete();

        String expected = "DELETE FROM JDBC";

        verify(jdbcAdapterMock).update(expected);
    }

    @Test
    public void shouldGenerateDeleteSQL() {
        operation.deleteSql();

        String expected = "DELETE FROM JDBCSQL";

        verify(jdbcAdapterMock).update(expected);
    }

    @Test
    public void givenCodeIsA_whenDeleteSQL_thenShouldDeleteSQL() {
        operation.deleteSql(CODE_IS_A);

        String expected = "DELETE FROM JDBCSQL WHERE CODE = ?";

        verify(jdbcAdapterMock).update(expected, new Object[]{CODE_IS_A});
    }

    @Test
    public void givenCodeIsA_whenDelete_thenShouldDeleteSQL() {
        operation.delete(CODE_IS_A);

        String expected = "DELETE FROM JDBC WHERE CODE = ?";

        verify(jdbcAdapterMock).update(expected, new Object[]{CODE_IS_A});
    }

    @Test
    public void givenCodeIsAAndNameIsB_whenDelete_thenShouldDeleteSQL() {
        operation.delete(CODE_IS_A, NAME_IS_B);

        String expected = "DELETE FROM JDBC WHERE CODE = ? AND NAME = ?" ;

        verify(jdbcAdapterMock).update(expected, new Object[]{CODE_IS_A, NAME_IS_B});
    }

    @Test
    public void shouldDeletesSQL() {
        operation.deletes();

        String expected = "DELETE FROM JDBC" ;
        String twoExpected = "DELETE FROM JDBCSQL" ;

        verify(jdbcAdapterMock).update(expected);
        verify(jdbcAdapterMock).update(twoExpected);
    }


}
