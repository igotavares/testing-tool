package com.ibeans.jdbc.ext;

import java.math.BigDecimal;


public interface JdbcExtContants {

    BigDecimal STATUS_IS_ZERO = BigDecimal.ZERO;
    String CODE_IS_A = "A";
    String CODE_IS_C = "C";
    String NAME_IS_A = "A";
    String NAME_IS_B = "B";
    String NAME_IS_NULL = null;
    String JDBC_NAME_IS_B = "B";
    Long ID_IS_ONE = 1L;
    Long STATIC_IS_TWO = 2L;
    String DESCRIPTION_IS_C = "C";
    
}
