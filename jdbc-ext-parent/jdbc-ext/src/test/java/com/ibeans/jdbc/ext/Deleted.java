package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.annotation.Delete;


public interface Deleted {

    @Delete
    void delete();

}
