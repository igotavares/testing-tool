package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.annotation.*;
import com.ibeans.jdbc.ext.support.OrderingType;
import com.ibeans.jdbc.ext.support.Type;

import java.util.List;
import java.util.Map;


@Table("JDBC")
public interface SearchOperation {

    @Query("SELECT * FROM JDBCSQL")
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")
    })
    List<Map<String, Object>> findSQLAll();

    @Query("SELECT * FROM JDBCSQL WHERE CODE = ?")
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")
    })
    List<Map<String, Object>> findSQL(String code);

    @Query
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")
    })
    List<Map<String, Object>> findAll();

    @Query(orderBy = {@OrderBy(columns = {"CODE", "NAME"}), @OrderBy(columns = "VALUE", type = OrderingType.DESC)})
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name"),
            @Mapper(columns = "VALUE", name = "value")
    })
    List<Map<String, Object>> findWithOrdering();

    @Query
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")
    })
    List<Map<String, Object>> find(@Column(name = "CODE") String code);

    @Query
    @RowMapper({
            @Mapper(columns = "CODE", name = "code", sql = "SELECT NAME FROM SQLJDBC WHERE CODE = ?"),
            @Mapper(columns = "NAME", name = "name")
    })
    List<Map<String, Object>> findMapperWithSql();

    @Query
    @RowMapper(value = {
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")})
    List<Map<String, Object>> find(@Column(name = "CODE") String code,
                                   @Column(name = "NAME") String name);

    @Query
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")
    })
    Map<String, Object> findMap();



    @Query
    @RowMapper({
            @Mapper(columns = "CODE", name = "code"),
            @Mapper(columns = "NAME", name = "name")
    })
    List<Map<String, Object>> findSub(
            @Column(name = "CODE", sql = "SELECT CODE FROM SUBJDBC WHERE NAME = ?") String name);


    @Query
    @RowMapper({
            @Mapper(name = "code", columns = "NAME", map = @EnumMap(map = TypeEnum.class)),
            @Mapper(name = "name", columns = "CODE", map = @EnumMap(map = TypeEnum.class), type = Type.INTEGER)
    })
    Map<String, Object> findWithEnumColumn();

    @Query
    @RowMapper({
            @Mapper(name = "code", columns = "NAME", map = @EnumMap(map = TypeEnum.class, from = "name", to = "code")),
            @Mapper(name = "name", columns = "CODE", map = @EnumMap(map = TypeEnum.class, from = "code", to = "name"), type = Type.INTEGER)
    })
    Map<String, Object> findWithPropertyEnumColumn();

    enum TypeEnum {
        TYPE_ONE(1, "ONE"),
        TYPE_TWO(2, "TWO");

        private final Integer code;
        private final String name;

        TypeEnum(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

}
