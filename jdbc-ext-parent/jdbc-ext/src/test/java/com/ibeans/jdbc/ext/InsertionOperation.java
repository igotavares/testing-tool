package com.ibeans.jdbc.ext;

import com.ibeans.jdbc.ext.annotation.*;
import com.ibeans.jdbc.ext.support.DataType;

import java.util.Map;


@Table("JDBC")
public interface InsertionOperation {

    @Insert("INSERT INTO JDBC (CODE, NAME) VALUES (?, ?)")
    void insertSQL(String code, String name);

    @Insert
    @Column(name = "STATUS", defaultValue = "0", type = DataType.NUMBER)
    void insertWithMethodColumn(@Column(name = "CODE") String code, @Column(name = "NAME") String name);

    @Insert
    @Columns({@Column(name = "STATUS", defaultValue = "0", type = DataType.NUMBER),
            @Column(name = "DESCRIPTION", sql = "SELECT DESCRIPTION FROM TABLE_TWO")})
    void insertWithMethodColumns(@Column(name = "CODE") String code);

    @Insert
    void insert(@Column(name = "CODE") String code,
                @Column(name = "NAME") String name);

    @Insert
    void insertTwoColumns(@Columns({
            @Column(name = "NAME"),
            @Column(name = "DESCRIPTION", sql = "SELECT DESCRIPTION FROM JDBC WHERE NAME = ?")}) String name);

    @Insert
    @Column(name = "DESCRIPTION", sql = "SELECT DESCRIPTION FROM TABLE_TWO WHERE CODE = :code AND NAME = :name")
    void insertWithColumn(@Column(name = "CODE") String code,
                          @Column(name = "NAME") String name);

    @Insert
    @Column(name = "DESCRIPTION", sql = "SELECT DESCRIPTION FROM TABLE_TWO WHERE CODE = :code AND NAME = :name")
    void insertWithParam(@Column(name = "CODE") @Param("code") String code, @Param("name") String name);

    @Insert
    @Sequence(name = "SEQ_ID", columns = @Column(name = "ID"))
    Long insertWithSequence(@Column(name = "CODE") String code, @Column(name = "NAME") String name);

    @Insert
    @Sequence(name = "SEQ_ID", columns = @Column(name = "ID"))
    Long insertWithFind(@Column(name = "CODE", sql = "SELECT CODE FROM NAME = ?") String name,
                        @Column(name = "NAME") String jdbcName);

    @Insert
    @Sequences({@Sequence(name = "SEQ_ID", columns = {@Column(name = "ID"),
            @Column(name =  "JDBC_ID", table = @Table(value = "TWO_JDBC"))}),
            @Sequence(name = "SEQ_ID", columns = {@Column(name =  "ID", table = @Table(value = "TWO_JDBC"))})})
    Long insertWithTwoTable(@Column(name =  "CODE") String code,
                            @Column(name =  "NAME", table = @Table(value = "TWO_JDBC")) String name);

    @Insert
    @Sequences({@Sequence(name = "SEQ_ID", columns = @Column(name = "ID")),
            @Sequence(name = "SEQ_STATIC", columns = @Column(name =  "STATIC"))})
    Map<String, Object> save();

    @Insert
    @Sequences({@Sequence(name = "SEQ_ID", columns = @Column(name = "ID")),
            @Sequence(name = "SEQ_STATIC", columns = @Column(name =  "STATIC"))})
    @RowMapper({@Mapper(name = "pk", columns = "ID"),
            @Mapper(name = "code", columns = "CODE")})
    Map<String, Object> saveWithRowMapper(@Column(name =  "CODE") String code);


    @Insert
    void insertWithEnum(@Column(name = "CODE", map = @EnumMap(map = Type.class)) String name,
                        @Column(name = "NAME", map = @EnumMap(map = Type.class)) Integer code);

    @Insert
    void insertWithPropertyEnum(@Column(name = "CODE", map = @EnumMap(map = Type.class, from = "name", to = "code")) String name,
                        @Column(name = "NAME", map = @EnumMap(map = Type.class, from = "code", to = "name")) Integer code);


    @Insert
    void insertWithJson(@Column(name = "TEMPLATE", json = @Json(key = @JsonKey("name"))) String name);

    @Insert
    void insertTwoColumnWithJson(@Column(name = "TEMPLATE", json = @Json(key = @JsonKey("name"))) String name,
                                 @Column(name = "FAS", json = @Json(key = @JsonKey("age"))) Integer age);

    @Insert
    void insertWithJsonKeis(@Column(name = "TEMPLATE", json = @Json(key = {@JsonKey("person"),
                                                                           @JsonKey("name")})) String name);

    @Insert
    void insertWithJsons(@Column(name = "TEMPLATE", json = @Json(key = @JsonKey("name"))) String name,
                         @Column(name = "TEMPLATE", json = @Json(key = @JsonKey("age"))) Integer age);


    @Insert
    void insertWithJsonValue(@Column(name = "TEMPLATE", json = {
            @Json(key = {@JsonKey("person"), @JsonKey("age")}, value = "1"),
            @Json(key = {@JsonKey("person"), @JsonKey("name")})}) String name);

    enum Type {
        TYPE_ONE(1, "ONE"),
        TYPE_TWO(2, "TWO");

        private final Integer code;
        private final String name;

        Type(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

}
