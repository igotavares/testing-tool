package com.ibeans.jdbc.ext;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Proxy;


@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractOperationTest<E> {

    @Mock
    protected JdbcAdapter jdbcAdapterMock;

    protected E operation;

    public void context(Class<E> operationClass) throws Exception {
        operation = (E) Proxy.newProxyInstance(operationClass.getClassLoader(),
                new Class<?>[]{operationClass},
                new JdbcProxy(jdbcAdapterMock, operationClass));
    }

}
