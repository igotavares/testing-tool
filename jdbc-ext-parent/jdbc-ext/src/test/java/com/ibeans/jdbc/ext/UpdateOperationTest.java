package com.ibeans.jdbc.ext;

import org.junit.Before;
import org.junit.Test;
import static com.ibeans.jdbc.ext.JdbcExtContants.*;

import static org.mockito.Mockito.verify;


public class UpdateOperationTest extends AbstractOperationTest<UpdateOperation> {

    @Before
    public void context() throws Exception {
        context(UpdateOperation.class);
    }

    @Test
    public void thenShouldGenerateSql() {
        operation.update(NAME_IS_B, CODE_IS_A);

        String sqlExpected = "UPDATE JDBC SET NAME = ? WHERE CODE = ?" ;

        verify(jdbcAdapterMock).update(sqlExpected, new Object[]{NAME_IS_B, CODE_IS_A});
    }

}
