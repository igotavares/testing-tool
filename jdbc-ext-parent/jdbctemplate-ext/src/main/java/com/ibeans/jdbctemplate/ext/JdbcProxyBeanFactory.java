package com.ibeans.jdbctemplate.ext;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.JdbcProxy;

import java.lang.reflect.Proxy;


public class JdbcProxyBeanFactory {

    private JdbcAdapter jdbcAdapter;

    public void setJdbcAdapter(JdbcAdapter jdbcAdapter) {
        this.jdbcAdapter = jdbcAdapter;
    }

    public <E> E create(Class<E> beanClass) {
        return (E) Proxy.newProxyInstance(beanClass.getClassLoader(),
                new Class<?>[]{beanClass}, new JdbcProxy(jdbcAdapter, beanClass));
    }

}
