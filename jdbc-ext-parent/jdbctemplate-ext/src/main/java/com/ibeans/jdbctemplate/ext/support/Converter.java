package com.ibeans.jdbctemplate.ext.support;

import com.ibeans.jdbc.ext.support.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


public enum Converter {

    DEFAULT(Type.DEFAULT) {
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            return resultSet.getObject(column);
        }
    },
    STRING(Type.STRING) {
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            return resultSet.getString(column);
        }
    },
    BOOLEAN(Type.BOOLEAN) {
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            Object value = resultSet.getObject(column);
            if (value == null) {
                return null;
            }
            if (value instanceof Number) {
                if (((Number) value).intValue() == 1) {
                    return true;
                }
                if (((Number) value).intValue() == 0) {
                    return false;
                }
            }
            if (value instanceof String) {
                if ("1".equals(value)) {
                    return true;
                }
                if ("0".equals(value)) {
                    return false;
                }
            }
            throw new IllegalArgumentException(column + " Column is not Boolean type");
        }
    },
    LONG(Type.LONG) {
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            Object value = resultSet.getObject(column);
            if (value == null) {
                return null;
            }
            if (value instanceof Number) {
                return ((Number) value).longValue();
            }
            if (value instanceof String) {
                return Long.valueOf((String) value);
            }
            throw new IllegalArgumentException(column + " Column is not Long type");
        }
    },
    INTEGER(Type.INTEGER){
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            Object value = resultSet.getObject(column);
            if (value == null) {
                return null;
            }
            if (value instanceof Number) {
                return ((Number) value).intValue();
            }
            if (value instanceof String) {
                return Integer.valueOf((String) value);
            }
            throw new IllegalArgumentException(column + " Column is not Integer type");
        }
    },
    BIG_DECIMAL(Type.BIG_DECIMAL){
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            return resultSet.getBigDecimal(column);
        }
    },
    DATE(Type.DATE){
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            Date date = resultSet.getDate(column);
            if (date != null) {
                return Date.from(date.toInstant());
            }
            return null;
        }
    },
    LOCAL_DATE(Type.LOCAL_DATE){
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            Date date = resultSet.getDate(column);
            if (date != null) {
                return new LocalDate(date);
            }
            return null;
        }
    },
    LOCAL_DATE_TIME(Type.LOCAL_DATE_TIME){
        @Override
        public Object convert(ResultSet resultSet, String column) throws SQLException {
            Date date = resultSet.getTimestamp(column);
            if (date != null) {
                return new LocalDateTime(date)
                        .withMillisOfSecond(0);
            }
            return null;
        }
    };

    public static final int FIRST = 0;

    private Type type;

    Converter(Type type) {
        this.type = type;
    }

    private boolean isType(Type type) {
        return this.type.equals(type);
    }

    public abstract Object convert(ResultSet resultSet, String column) throws SQLException;

    public static Object convert(Type type, String column, ResultSet resultSet) throws SQLException {
        return from(type).convert(resultSet, column);
    }

    public static Converter from(Type type) {
        for (Converter converter : values()) {
            if (converter.isType(type)) {
                return converter;
            }
        }
        throw new IllegalArgumentException("JodaConverter not found by type: " + type);
    }


}
