package com.ibeans.jdbctemplate.ext.support;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.stream.Stream;


public enum ParameterConverter {

    LOCAL_DATE(LocalDate.class) {
        @Override
        public Object toObject(Object value) {
            return ((LocalDate) value).toDate();
        }
    },
    LOCAL_DATE_TIME(LocalDateTime.class) {
        @Override
        public Object toObject(Object value) {
            return ((LocalDateTime) value).toDate();
        }
    };

    private Class type;

    ParameterConverter(Class type) {
        this.type = type;
    }

    public boolean is(Object value) {
        return value != null && type.isInstance(value);
    }

    public abstract Object toObject(Object value);

    public static Object to(Object value) {
        return Stream.of(values())
                .filter(converter -> converter.is(value))
                .findFirst()
                .map(converter -> converter.toObject(value))
                .orElse(value);
    }

    public static Object[] to(Object[] values) {
        return Stream.of(values).map(ParameterConverter::to).toArray();
    }

}
