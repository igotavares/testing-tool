package com.ibeans.jdbctemplate.ext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;

import java.util.Collection;
import java.util.stream.Stream;


public class RepositoryProxyBeansRegistration implements BeanFactoryPostProcessor {

    private String beanName;
    private ClassPathScanner classPathScanner;
    private String[] basePackages;

    public RepositoryProxyBeansRegistration() {
        classPathScanner = new ClassPathScanner( false);
        classPathScanner.addIncludeFilter(new AnnotationTypeFilter(RepositoryProxy.class));
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public void setBasePackages(String basePackages) {
        this.basePackages = basePackages.split(",");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) {
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry ) configurableListableBeanFactory;
        Stream.of(basePackages)
                    .map(classPathScanner::findCandidateComponents)
                    .flatMap(Collection::stream)
                    .forEach(beanDefinition -> {
                        registerBeanDefinition(registry, beanDefinition);
                    });
    }

    private void registerBeanDefinition(BeanDefinitionRegistry registry, BeanDefinition beanDefinition) {
         try {
            Class<?> beanClass = Class.forName(beanDefinition.getBeanClassName());

            RepositoryProxy repositoryProxy = beanClass.getAnnotation(RepositoryProxy.class);

            String beanName = !repositoryProxy.name().isEmpty()
                    ? repositoryProxy.name() : ClassUtils.getShortNameAsProperty(beanClass);

            ConstructorArgumentValues args = new ConstructorArgumentValues();
            args.addGenericArgumentValue(beanClass);

            GenericBeanDefinition proxyBeanDefinition = new GenericBeanDefinition();
            proxyBeanDefinition.setBeanClass(beanClass);
            proxyBeanDefinition.setConstructorArgumentValues(args);
            proxyBeanDefinition.setFactoryBeanName(this.beanName);
            proxyBeanDefinition.setFactoryMethodName("create");

            registry.registerBeanDefinition(beanName, proxyBeanDefinition);
        } catch (Exception cause) {
            throw new RuntimeException("Exception while createing proxy", cause);
        }
    }

}
