package com.ibeans.jdbctemplate.ext;

import com.ibeans.jdbc.ext.JdbcAdapter;
import com.ibeans.jdbc.ext.exception.EmptyResultException;
import com.ibeans.jdbc.ext.exception.IncorrectResultException;
import com.ibeans.jdbc.ext.support.Mapper;
import com.ibeans.jdbc.ext.support.RowMapper;
import com.ibeans.jdbctemplate.ext.support.Converter;
import com.ibeans.jdbctemplate.ext.support.ParameterConverter;
import com.google.common.collect.Maps;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


public class JdbcTemplateAdapterImpl implements JdbcAdapter {

    private static final String SEQUENCE = "select %s.nextval from dual";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void update(String sql) {
        jdbcTemplate.update(sql);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void update(String sql, Object[] args) {
        jdbcTemplate.update(sql, ParameterConverter.to(args));
    }

    @Override
    public List<Map<String, Object>> query(String sql, RowMapper rowMapper) {
        return jdbcTemplate.query(sql, createRowMapper(rowMapper));
    }

    @Override
    public List<Map<String, Object>> query(String sql, Object[] args, RowMapper rowMapper) {
        return jdbcTemplate.query(sql, args, createRowMapper(rowMapper));
    }

    private org.springframework.jdbc.core.RowMapper<Map<String, Object>> createRowMapper(RowMapper rowMapper) {
        return (resultSet, row) -> {
            Map<String, Object> items = Maps.newLinkedHashMap();
            for (Mapper mapper : rowMapper.getMappers()) {
                Object value = null;
                if (mapper.hasSql()) {
                    List<Object> parameters = new ArrayList<>();
                    for (String column : mapper.getColumns()) {
                        parameters.add(resultSet.getObject(column));
                    }
                    if (areNotNull(parameters)) {
                        value = queryForObject(mapper.getSql(), parameters.toArray());
                    }
                } else {
                    value = Converter.convert(mapper.getType(), mapper.getColumn(), resultSet);
                }
                items.put(mapper.getName(), mapper.getConverter().apply(value));
            }
            return items;
        };
    }

    private boolean areNotNull(Collection<?> values) {
        for (Object value : values) {
            if (value == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Long nextSequence(final String name) {
        final String sql = String.format(SEQUENCE, name);
        return jdbcTemplate.queryForObject(sql, Long.class);
    }

    @Override
    public Object queryForObject(String sql, Object[] parameters) {
        try {
            return queryForObject(sql, parameters, Object.class);
        } catch (EmptyResultDataAccessException cause) {
            throw new EmptyResultException(sql, parameters, cause);
        } catch (IncorrectResultSizeDataAccessException cause) {
            throw new IncorrectResultException(sql, parameters, cause);
        }
    }

    @Override
    public Object queryForObject(String sql) {
        try {
            return queryForObject(sql, Object.class);
        } catch (EmptyResultDataAccessException cause) {
            throw new EmptyResultException(sql, cause);
        } catch (IncorrectResultSizeDataAccessException cause) {
            throw new IncorrectResultException(sql, cause);
        }
    }

    @Override
    public List<Object> queryForList(String sql) {
        return jdbcTemplate.queryForList(sql, Object.class);
    }

    public <E> E queryForObject(String sql, Object[] parameters, Class<E> classType) {
        if (parameters != null) {
            return jdbcTemplate.queryForObject(sql, parameters, classType);
        }
        return null;
    }

    public <E> E queryForObject(String sql, Class<E> classType) {
        return jdbcTemplate.queryForObject(sql, classType);
    }

}
