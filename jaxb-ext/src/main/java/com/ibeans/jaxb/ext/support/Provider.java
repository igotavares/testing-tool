package com.ibeans.jaxb.ext.support;

public interface Provider<E> {

	E get(String name);
	
}
