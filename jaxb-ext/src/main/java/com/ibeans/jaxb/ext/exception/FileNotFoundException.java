/*
 * Copyright (c) 2015 Cardif.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Cardif
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Cardif.
 */

package com.ibeans.jaxb.ext.exception;


public class FileNotFoundException extends SystemException {

    private static final long serialVersionUID = -1271735645747592608L;
    private final String nameFile;

    public FileNotFoundException(final String nameFile) {
        this.nameFile = nameFile;
    }

    @Override
    public String getMessage() {
        return "File(" + nameFile + ") not found!";
    }
}
