package com.ibeans.jaxb.ext.annotation;

import com.ibeans.jaxb.ext.factory.support.ExtensionType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ProviderResource {

    String path();

    Class<?>[] classes();

    ExtensionType type() default ExtensionType.XML;

}
