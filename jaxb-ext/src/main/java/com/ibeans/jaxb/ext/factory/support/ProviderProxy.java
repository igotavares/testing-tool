package com.ibeans.jaxb.ext.factory.support;

import com.ibeans.jaxb.ext.annotation.ProviderResource;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


public class ProviderProxy implements InvocationHandler {

    private Class<?> providerClass;
    private ProviderResource provider;
    private XmlProvider xmlProvider;

    public ProviderProxy(final Class<?> providerClass) {
        this.providerClass = providerClass;
        this.provider = getProvider(providerClass);
        this.xmlProvider = new XmlProvider(providerClass, provider);
    }

    private ProviderResource getProvider(final Class<?> providerClass) {
        final Annotation[] annotations =  providerClass
                .getDeclaredAnnotations();
        for (final Annotation annotation : annotations) {
            if (annotation instanceof ProviderResource) {
                return (ProviderResource) annotation;
            }
        }
        throw new IllegalArgumentException("Annotation Provider not found!");
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        final Object parameter = args[0];

        if (parameter instanceof String) {
            return xmlProvider.unmarshaller((String) parameter);
        }

        return xmlProvider.marshaller(parameter);
    }



}
