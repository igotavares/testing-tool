package com.ibeans.jaxb.ext.adapter;

import org.joda.time.LocalDateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;


public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    public LocalDateTime unmarshal(String value) throws Exception {
        return new LocalDateTime(value);
    }

    public String marshal(LocalDateTime value) throws Exception {
        return value.toString();
    }

}
