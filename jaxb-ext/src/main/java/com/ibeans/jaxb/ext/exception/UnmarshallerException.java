/*
 * Copyright (c) 2015 Cardif.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Cardif
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Cardif.
 */

package com.ibeans.jaxb.ext.exception;


public class UnmarshallerException extends SystemException {

    private static final long serialVersionUID = 4344722457256508974L;

    public UnmarshallerException(final Exception cause) {
        super(cause);
    }
}
