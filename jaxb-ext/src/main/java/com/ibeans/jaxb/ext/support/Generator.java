package com.ibeans.jaxb.ext.support;

public interface Generator<E, B> {

	String generateBuilder(B value);
	
	String generate(E value);
	
}
