package com.ibeans.jaxb.ext.adapter;

import org.joda.time.LocalDate;

import javax.xml.bind.annotation.adapters.XmlAdapter;


public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    public LocalDate unmarshal(String value) throws Exception {
        return new LocalDate(value);
    }

    public String marshal(LocalDate value) throws Exception {
        return value.toString();
    }

}
