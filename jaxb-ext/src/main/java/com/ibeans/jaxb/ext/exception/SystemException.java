/*
 * Copyright (c) 2015 Cardif.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Cardif
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Cardif.
 */

package com.ibeans.jaxb.ext.exception;


public class SystemException extends RuntimeException {

    private static final long serialVersionUID = -5534640142730446033L;

    public SystemException() {
    }

    public SystemException(final String message) {
        super(message);
    }

    public SystemException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SystemException(final Throwable cause) {
        super(cause);
    }
}
