package com.ibeans.jaxb.ext.factory;

import com.ibeans.jaxb.ext.factory.support.ProviderProxy;

import java.lang.reflect.Proxy;


public class ProviderFactory {

    public static <T> T create(final Class<T> providerClass) {
        return (T) Proxy.newProxyInstance(providerClass.getClassLoader(),
                new Class<?>[]{providerClass}, new ProviderProxy(providerClass));
    }

}
