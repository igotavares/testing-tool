package com.ibeans.jaxb.ext.adapter;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;


public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

    public DateTime unmarshal(String value) throws Exception {
        return new DateTime(value);
    }

    public String marshal(DateTime value) throws Exception {
        return value.toString();
    }

}
