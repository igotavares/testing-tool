package com.ibeans.jaxb.ext.adapter;

import org.joda.time.LocalTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;


public class LocalTimeAdapter extends XmlAdapter<String, LocalTime> {

    public LocalTime unmarshal(String value) throws Exception {
        return new LocalTime(value);
    }

    public String marshal(LocalTime value) throws Exception {
        return value.toString();
    }

}
