package com.ibeans.jaxb.ext.factory.support;

import com.ibeans.jaxb.ext.annotation.ProviderResource;
import com.ibeans.jaxb.ext.exception.FileNotFoundException;
import com.ibeans.jaxb.ext.exception.SystemException;
import com.ibeans.jaxb.ext.exception.UnmarshallerException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.StringWriter;


public class XmlProvider {

    private final Class<?> providerClass;
    private final JAXBContext jaxbContext;
    private final ProviderResource provider;

    /**
     * Instantiates a new Abstract xml product builder.
     */
    public XmlProvider(final Class<?> providerClass, final ProviderResource provider) {
        this.providerClass = providerClass;
        this.provider = provider;
        this.jaxbContext = createJAXBContext(provider.classes());
    }

    private JAXBContext createJAXBContext(final Class<?>[] classes) {
        try {
            return JAXBContext.newInstance(classes);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Build product configuration.
     *
     * @return the product configuration
     */
    public Object unmarshaller(final String name) throws SystemException {
        final InputStream file = getFile(provider.path() + name + "." + provider.type());
        return unmarshller(file);
    }

    private Object unmarshller(final InputStream file) throws UnmarshallerException {
        final Unmarshaller unmarshaller;
        try {
            unmarshaller = jaxbContext.createUnmarshaller();
            return unmarshaller.unmarshal(file);
        } catch (final JAXBException cause) {
            throw new UnmarshallerException(cause);
        }
    }

    private InputStream getFile(final String name) throws FileNotFoundException {
        final InputStream resourceAsStream = providerClass
                .getClassLoader()
                .getResourceAsStream(name);
        if (resourceAsStream == null) {
            throw new FileNotFoundException(name);
        }
        return resourceAsStream;
    }

    public String marshaller(final Object value) {
        try {
            final Marshaller marshaller = jaxbContext.createMarshaller();
            final StringWriter writer = new StringWriter();
            marshaller.marshal(value, writer);
            return writer.toString();
        } catch (final JAXBException cause) {
            throw new RuntimeException(cause);
        }
    }

}
