package com.ibeans.jaxb.ext.factory;

import com.ibeans.jaxb.ext.annotation.ProviderResource;


@ProviderResource(path = "/person/", classes = {Person.class})
public interface PersonProvider {
}
