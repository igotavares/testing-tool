package com.ibeans.hamcrest.ext.core;

import com.ibeans.hamcrest.ext.matcher.AllOf;
import com.ibeans.hamcrest.ext.matcher.IsMapContaining;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableContainingInOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.ibeans.hamcrest.ext.core.Assert.assertThat;
import static org.junit.Assert.assertNotNull;


public class MapMatcher {

    private final List<Matcher<Map<?, ?>>> matchers;

    public MapMatcher() {
        this.matchers = new ArrayList<>();
    }

    public MapMatcher hasEntry(String key, Object value) {
        matchers.add(IsMapContaining.hasEntry(key, value));
        return this;
    }

    public MapMatcher hasEntry(String key, Matcher<Object> value) {
        matchers.add(IsMapContaining.hasEntry(Matchers.equalTo(key), value));
        return this;
    }

    public MapMatcher notNullValue(String key) {
        matchers.add(IsMapContaining.hasEntry(
                Matchers.equalTo(key),
                Matchers.notNullValue()));
        return this;
    }

    public MapMatcher nullOrNotNullValue(String key, Object value) {
        matchers.add(IsMapContaining.hasEntry(
                Matchers.equalTo(key),
                value != null ? Matchers.notNullValue() :
                        Matchers.nullValue()));
        return this;
    }

    public MapMatcher nullValue(String key) {
        matchers.add(IsMapContaining.hasEntry(
                Matchers.equalTo(key),
                Matchers.nullValue()));
        return this;
    }

    public void all(String message, Map<String, Object> value) {
        assertNotNull(message, value);
        assertThat(value, all());
    }

    public void all(Map<String, Object> value) {
        assertThat(value, all());
    }

    public Matcher all() {
        return AllOf.allOf((List) matchers);
    }

    public void contains(String message, List<Map<String, Object>> values) {
        assertNotNull(message, values);
        assertThat(values, contains());
    }

    public void contains(List<Map<String, Object>> values) {
        assertThat(values, contains());
    }

    public Matcher contains() {
        return IsIterableContainingInOrder.contains(all());
    }

    public static MapMatcher create() {
        return new MapMatcher();
    }

}
