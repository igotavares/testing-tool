package com.ibeans.hamcrest.ext.matcher;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Map;

import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by igotavares on 05/08/2017.
 */
public class IsMapContaining <K,V> extends TypeSafeMatcher<Map<? extends K, ? extends V>> {
    private final Matcher<? super K> keyMatcher;
    private final Matcher<? super V> valueMatcher;

    public IsMapContaining(Matcher<? super K> keyMatcher, Matcher<? super V> valueMatcher) {
        this.keyMatcher = keyMatcher;
        this.valueMatcher = valueMatcher;
    }

    @Override
    public boolean matchesSafely(Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            if (keyMatcher.matches(entry.getKey()) && valueMatcher.matches(entry.getValue())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void describeMismatchSafely(Map<? extends K, ? extends V> map, Description mismatchDescription) {
        String actual = map.entrySet().stream()
                .filter(entry -> keyMatcher.matches(entry.getKey()) && !valueMatcher.matches(entry.getValue()))
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .findFirst()
                .orElse("no property " + keyMatcher.toString());

        mismatchDescription.appendText(actual);
    }

    @Override
    public void describeTo(Description description) {
        description.appendDescriptionOf(keyMatcher)
                .appendText("=")
                .appendDescriptionOf(valueMatcher);
    }

    public static <K, V> Matcher<Map<? extends K, ? extends V>> hasEntry(Matcher<? super K> keyMatcher, Matcher<? super V> valueMatcher) {
        return new IsMapContaining<K, V>(keyMatcher, valueMatcher);
    }

    public static <K, V> Matcher<Map<? extends K, ? extends V>> hasEntry(K key, V value) {
        return new IsMapContaining<K, V>(equalTo(key), equalTo(value));
    }

    public static <K, V> Matcher<Map<? extends K, ? extends V>> hasEntry(K key, Matcher<? super V> valueMatcher) {
        return new IsMapContaining<K, V>(equalTo(key), valueMatcher);
    }

}
