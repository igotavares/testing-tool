package com.ibeans.hamcrest.ext.matcher;

import org.hamcrest.Description;

import java.util.function.Function;


public class IsEqual<T> extends org.hamcrest.core.IsEqual<T> {

    private final Object expectedValue;

    private final Function<Object, String> converter;

    public IsEqual(T equalArg, Function<Object, String> converter) {
        super(equalArg);
        this.expectedValue = equalArg;
        this.converter = converter;
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(converter.apply(expectedValue));
    }

    @Override
    public void describeMismatch(Object item, Description description) {
        description.appendText(": ").appendValue(converter.apply(item));
    }
}
