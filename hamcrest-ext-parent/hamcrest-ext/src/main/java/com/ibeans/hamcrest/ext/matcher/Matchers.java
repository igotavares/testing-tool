package com.ibeans.hamcrest.ext.matcher;

import org.hamcrest.Matcher;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;


public class Matchers {

    public static <T> Matcher<T> allOf(Collection<Matcher<? super T>> matchers) {
        return AllOf.<T>allOf(matchers);
    }

    public static <T> Matcher<T> allOf(Matcher<? super T>... matchers) {
        return AllOf.<T>allOf(matchers);
    }

    public static <K,V> Matcher<Map<? extends K,? extends V>> hasEntry(Matcher<? super K> keyMatcher, Matcher<? super V> valueMatcher) {
        return IsMapContaining.<K, V>hasEntry(keyMatcher, valueMatcher);
    }

    public static <K,V> Matcher<Map<? extends K,? extends V>> hasEntry(K key, Matcher<? super V> valueMatcher) {
        return IsMapContaining.<K,V>hasEntry(key, valueMatcher);
    }

    public static <K,V> Matcher<Map<? extends K,? extends V>> hasEntry(K key, V value) {
        return IsMapContaining.<K,V>hasEntry(key, value);
    }

    public static <T> Matcher<T> equalTo(T operand, Function<Object, String> converter) {
        return new IsEqual<T>(operand, converter);
    }

    public static <T> Matcher<T> hasProperty(String propertyName, String propertyDescription, Matcher<?> valueMatcher) {
        return new HasPropertyWithValue(propertyName, propertyDescription, valueMatcher);
    }

    public  static <T> Matcher<T> isBean(T bean) {
        return new Bean<T>(bean);
    }

    public  static <T> Matcher<T> isBean(T bean, Class<T> beanClass) {
        return new Bean<T>(bean, beanClass);
    }

    public  static <T> Matcher<T> isBean(T bean, Class<T> beanClass, Class<?> group) {
        return new Bean<T>(bean, beanClass, group);
    }

}
