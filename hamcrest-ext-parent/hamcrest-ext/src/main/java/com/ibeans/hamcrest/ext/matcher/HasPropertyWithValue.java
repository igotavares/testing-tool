package com.ibeans.hamcrest.ext.matcher;

import org.hamcrest.Condition;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.beans.PropertyUtil;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import static org.hamcrest.Condition.matched;
import static org.hamcrest.Condition.notMatched;
import static org.hamcrest.beans.PropertyUtil.NO_ARGUMENTS;


public class HasPropertyWithValue<T> extends TypeSafeDiagnosingMatcher<T> {

    private static final Condition.Step<PropertyDescriptor,Method> WITH_READ_METHOD = withReadMethod();
    private final String propertyName;
    private final String propertyDescription;
    private final Matcher<Object> valueMatcher;

    public HasPropertyWithValue(String propertyName, String propertyDescription, Matcher<?> valueMatcher) {
        this.propertyName = propertyName;
        this.propertyDescription = propertyDescription;
        this.valueMatcher = nastyGenericsWorkaround(valueMatcher);
    }

    @Override
    public boolean matchesSafely(T bean, Description mismatch) {
        return propertyOn(bean, mismatch)
                .and(WITH_READ_METHOD)
                .and(withPropertyValue(bean))
                .matching(valueMatcher, propertyDescription );
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(propertyDescription).appendText(": ")
                .appendDescriptionOf(valueMatcher);
    }

    private Condition<PropertyDescriptor> propertyOn(T bean, Description mismatch) {
        PropertyDescriptor property = PropertyUtil.getPropertyDescriptor(propertyName, bean);
        if (property == null) {
            mismatch.appendText("No property \"" + propertyName + "\"");
            return notMatched();
        }

        return matched(property, mismatch);
    }

    private Condition.Step<Method, Object> withPropertyValue(final T bean) {
        return new Condition.Step<Method, Object>() {
            @Override
            public Condition<Object> apply(Method readMethod, Description mismatch) {
                try {
                    return matched(readMethod.invoke(bean, NO_ARGUMENTS), mismatch);
                } catch (Exception cause) {
                    mismatch.appendText(cause.getMessage());
                    return notMatched();
                }
            }
        };
    }

    @SuppressWarnings("unchecked")
    private static Matcher<Object> nastyGenericsWorkaround(Matcher<?> valueMatcher) {
        return (Matcher<Object>) valueMatcher;
    }

    private static Condition.Step<PropertyDescriptor,Method> withReadMethod() {
        return new Condition.Step<PropertyDescriptor, Method>() {
            @Override
            public Condition<Method> apply(PropertyDescriptor property, Description mismatch) {
                final Method readMethod = property.getReadMethod();
                if (null == readMethod) {
                    mismatch.appendText("property \"" + property.getName() + "\" is not readable");
                    return notMatched();
                }
                return matched(readMethod, mismatch);
            }
        };
    }


}
