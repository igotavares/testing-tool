package com.ibeans.hamcrest.ext.matcher;

import static com.ibeans.hamcrest.ext.matcher.Matchers.*;
import org.hamcrest.Matcher;
import org.junit.*;

import java.lang.*;
import java.util.*;

import static com.ibeans.hamcrest.ext.core.Assert.assertThat;
import static org.junit.Assert.assertEquals;

public class IsMapContainingTest {

    @Test
    public void should() {
        Map<String, Object> actual = new LinkedHashMap<>();
        actual.put("name", "A");
        actual.put("type", "B");

        Matcher expected = allOf(
                hasEntry("name", "B"),
                hasEntry("type", "C")
        );

        try {
            assertThat(actual, expected);
        } catch (AssertionError cause) {
            assertEquals("Expected: {name=B, type=C} but: {name=A, type=B}", cause.getMessage());
        }
    }

    @Test
    public void shouldProperty() {
        Map<String, Object> actual = new LinkedHashMap<>();
        actual.put("name", "A");

        Matcher expected = allOf(
                hasEntry("type", "B")
        );

        try {
            assertThat(actual, expected);
        } catch (AssertionError cause) {
            assertEquals("Expected: {type=B} but: {no property \"type\"}", cause.getMessage());
        }
    }

}
