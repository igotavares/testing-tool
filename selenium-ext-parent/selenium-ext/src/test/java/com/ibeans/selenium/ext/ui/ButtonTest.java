package com.ibeans.selenium.ext.ui;

import com.google.common.base.MoreObjects;
import com.ibeans.selenium.ext.ServerRule;
import com.ibeans.selenium.ext.WebDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

/**
 * Created by igotavares on 09/07/2017.
 */
public class ButtonTest {

    public WebDriverRule driverRule = WebDriverRule
            .builder()
            .url("http://localhost:8080/button.html")
            .firefox()
            .build();

    public ServerRule serverRule = ServerRule
            .tomcat()
            .resourceTestBase()
            .build();

    @Rule
    public RuleChain chain = RuleChain
            .outerRule(serverRule)
            .around(driverRule);

    @Test
    public void shouldOpenPage() {
        ResultPage page = driverRule
                .init(ButtonPage.class)
                .setName("Tiger Nixon")
                .save();

        String expected = MoreObjects.toStringHelper(ResultPage.class)
                .add("name", "Tiger Nixon")
                .toString();

        assertEquals(expected, page.toString());
    }

    public static class ButtonPage {

        @FindBy(name = "name")
        private EditText name;

        @FindBy(id = "save")
        private Button save;

        public String getName() {
            return this.name.getValue();
        }

        public ButtonPage setName(String name) {
            this.name.setValue(name);
            return this;
        }

        public ResultPage save() {
            return save.open(ResultPage.class);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", getName())
                    .toString();
        }
    }

    public static class ResultPage {

        @FindBy(id = "name")
        private TextView name;

        public String getName() {
            return name.getValue();
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", getName())
                    .toString();
        }
    }


}
