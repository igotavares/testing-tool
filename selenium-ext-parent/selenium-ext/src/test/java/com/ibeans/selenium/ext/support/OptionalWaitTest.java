package com.ibeans.selenium.ext.support;

import org.junit.Test;

import java.util.Optional;
import java.util.function.Supplier;

import static org.junit.Assert.*;


public class OptionalWaitTest {


    private static final long TIMEOUT_IN_SECONDS_IS_30 = 30;

    @Test
    public void shouldGetNullValueAt30Seconds() throws Exception {
        Counter counter = new Counter();

        new OptionalWait(counter, TIMEOUT_IN_SECONDS_IS_30).orElse(null);

        Integer expected = 62;

        assertEquals(expected, counter.getCount());
    }

    public static class Counter implements Supplier<Optional<?>>  {

        private Integer count = 0;

        public Integer getCount() {
            return count;
        }

        @Override
        public Optional<?> get() {
            count++;
            return Optional.empty();
        }


    }


}