package com.ibeans.selenium.ext;

import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.StandardRoot;
import org.junit.rules.ExternalResource;

import javax.servlet.ServletException;
import java.io.File;

/**
 * Created by igotavares on 26/05/2017.
 */
public class ServerRule extends ExternalResource {

    private Server server;

    public ServerRule(Server server) {
        this.server = server;
    }

    @Override
    protected void before() throws Throwable {
        server.start();
    }

    @Override
    protected void after() {
        try {
            server.stop();
        } catch (Exception cause) {
            cause.printStackTrace();
        }
    }

    public static TomcatBuilder tomcat() {return new TomcatBuilder();}

    public static class TomcatBuilder {

        private static final String RESOURCE_TEST_BASE = "/src/test/resources";
        private static final String DEFAULT_CONTEXT = "/";
        private static final Integer DEFAULT_PORT = 8080;

        private Integer port = DEFAULT_PORT;
        private String contextPath = DEFAULT_CONTEXT;
        private String resourceBase;

        public TomcatBuilder resourceTestBase() {
            this.resourceBase = System.getProperty("user.dir") + RESOURCE_TEST_BASE;
            return this;
        }

        public TomcatBuilder resourceBase(final String resourceBase) {
            this.resourceBase = System.getProperty("user.dir") + resourceBase;
            return this;
        }

        public TomcatBuilder contextPath(final String contextPath) {
            this.contextPath = contextPath;
            return this;
        }

        public ServerRule build() {
            Tomcat tomcat = new Tomcat();

            tomcat.setPort(port);

            StandardContext context = null;
            try {
                context = (StandardContext)
                        tomcat.addWebapp(contextPath, new File(resourceBase).getAbsolutePath());
            } catch (ServletException cause) {
                throw new RuntimeException(cause);
            }

            context.setWorkDir("/target/tomcat");
            WebResourceRoot resources = new StandardRoot(context);
            context.setResources(resources);

            return new ServerRule(new Server() {

                @Override
                public void start() throws Exception {
                    tomcat.start();
                }

                @Override
                public void stop() throws Exception {
                    tomcat.stop();
                }
            });
        }
    }

    public interface Server {

        void start() throws Exception;

        void stop() throws Exception;

    }

}
