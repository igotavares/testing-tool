package com.ibeans.selenium.ext.driver;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

/**
 * Created by igotavares on 27/05/2017.
 */
public class Factory {

    public static WebDriverFactory internetExplorer() {
        System.setProperty("webdriver.ie.driver", System.getenv("IE_DRIVER_HOME") + "/IEDriverServer.exe");

        InternetExplorerDriver driver = new InternetExplorerDriver();

        return () -> new WebDriver(driver);
    }

    public static WebDriverFactory firefox() {
        System.setProperty("webdriver.gecko.driver", "C:/development/opt/firefox-portable/App/Firefox64/geckodriver.exe");

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.BINARY, "C:/development/opt/firefox-portable/App/Firefox64/firefox.exe");
        capabilities.setCapability(FirefoxDriver.MARIONETTE, true);

        final FirefoxDriver driver = new FirefoxDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(1200, TimeUnit.MILLISECONDS);

        return () -> new WebDriver(driver);
    }

}
