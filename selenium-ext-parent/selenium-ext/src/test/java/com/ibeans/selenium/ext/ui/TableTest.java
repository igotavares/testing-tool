package com.ibeans.selenium.ext.ui;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.ibeans.selenium.ext.ServerRule;
import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.WebDriverRule;
import com.ibeans.selenium.ext.wait.WebWait;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

/**
 * Created by igotavares on 09/07/2017.
 */
public class TableTest {

    public WebDriverRule driverRule = WebDriverRule
            .builder()
            .url("http://localhost:8080/table.html")
            .firefox()
            .build();

    public ServerRule serverRule = ServerRule
            .tomcat()
            .resourceTestBase()
            .build();

    @Rule
    public RuleChain chain = RuleChain
            .outerRule(serverRule)
            .around(driverRule);

    @Test
    public void shouldLoadItems() {
        TablePage page = driverRule
                .init(TablePage.class)
                .load();

        String expected = MoreObjects.toStringHelper(TablePage.class)
                .addValue(MoreObjects.toStringHelper(TestTable.class)
                        .addValue(Lists.newArrayList(
                                MoreObjects.toStringHelper(Item.class)
                                        .add("name", "Tiger Nixon")
                                        .add("Position", "System Architect"),
                                MoreObjects.toStringHelper(Item.class)
                                        .add("name", "Garrett Winters")
                                        .add("Position", "Accountant")
                        )))
                .toString();

        assertEquals(expected, page.toString());
    }

    public static class TablePage {

        @FindBy(id = "testTable")
        private TestTable table;

        public TablePage load() {
            this.table.onLoad();
            return this;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .addValue(table)
                    .toString();
        }
    }

    public static class TestTable extends Table<Item> {

        public TestTable(WebDriver driver, By tableBy, WebWait webWait) {
            super(driver, tableBy, webWait);
        }
    }

    public static class Item {

        @FindBy(className = "name-data")
        private TextView name;

        @FindBy(className = "position-data")
        private TextView Position;

        public String getName() {
            return name.getValue();
        }

        public String getPosition() {
            return Position.getValue();
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", getName())
                    .add("Position", getPosition())
                    .toString();
        }
    }

}
