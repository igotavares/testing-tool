package com.ibeans.selenium.ext.ui;

import com.google.common.base.MoreObjects;
import com.ibeans.selenium.ext.ServerRule;
import com.ibeans.selenium.ext.WebDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

/**
 * Created by igotavares on 09/07/2017.
 */
public class EditTextTest {

    public WebDriverRule driverRule = WebDriverRule
            .builder()
            .url("http://localhost:8080/edittext.html")
            .firefox()
            .build();

    public ServerRule serverRule = ServerRule
            .tomcat()
            .resourceTestBase()
            .build();

    @Rule
    public RuleChain chain = RuleChain
            .outerRule(serverRule)
            .around(driverRule);

    @Test
    public void shouldSetName() {
        EditTextPage page = driverRule
                .init(EditTextPage.class)
                .setName("Tiger Nixon");

        String expected = MoreObjects.toStringHelper(EditTextPage.class)
                .add("name", "Tiger Nixon")
                .toString();

        assertEquals(expected, page.toString());
    }

    public static class EditTextPage {

        @FindBy(name = "name")
        private EditText name;

        public String getName() {
            return this.name.getValue();
        }

        public EditTextPage setName(String name) {
            this.name.setValue(name);
            return this;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", getName())
                    .toString();
        }
    }

}
