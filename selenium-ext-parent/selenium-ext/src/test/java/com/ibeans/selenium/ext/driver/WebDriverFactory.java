package com.ibeans.selenium.ext.driver;

import com.ibeans.selenium.ext.WebDriver;

/**
 * Created by igotavares on 27/05/2017.
 */
public interface WebDriverFactory {

    WebDriver create();

}
