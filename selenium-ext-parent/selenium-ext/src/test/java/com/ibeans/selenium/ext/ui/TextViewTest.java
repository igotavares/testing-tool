package com.ibeans.selenium.ext.ui;

import com.google.common.base.MoreObjects;
import com.ibeans.selenium.ext.ServerRule;
import com.ibeans.selenium.ext.WebDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

/**
 * Created by igotavares on 22/06/2017.
 */
public class TextViewTest {

    public WebDriverRule driverRule = WebDriverRule
            .builder()
            .url("http://localhost:8080/textview.html")
            .firefox()
            .build();

    public ServerRule serverRule = ServerRule
            .tomcat()
            .resourceTestBase()
            .build();

    @Rule
    public RuleChain chain = RuleChain
            .outerRule(serverRule)
            .around(driverRule);

    @Test
    public void shouldGetName() {
        TextViewPage page = driverRule
                .init(TextViewPage.class);

        String expected = MoreObjects.toStringHelper(TextViewPage.class)
                .add("name", "Tiger Nixon")
                .toString();

        assertEquals(expected, page.toString());
    }

    public static class TextViewPage {

        @FindBy(id = "name")
        private TextView name;

        public String getName() {
            return name.getValue();
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", getName())
                    .toString();
        }
    }

}
