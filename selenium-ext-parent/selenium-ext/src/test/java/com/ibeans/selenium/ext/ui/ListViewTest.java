package com.ibeans.selenium.ext.ui;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.ibeans.selenium.ext.ServerRule;
import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.WebDriverRule;
import com.ibeans.selenium.ext.wait.WebWait;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

/**
 * Created by igotavares on 13/07/2017.
 */
public class ListViewTest {

    public WebDriverRule driverRule = WebDriverRule
            .builder()
            .url("http://localhost:8080/listview.html")
            .firefox()
            .build();

    public ServerRule serverRule = ServerRule
            .tomcat()
            .resourceTestBase()
            .build();

    @Rule
    public RuleChain chain = RuleChain
            .outerRule(serverRule)
            .around(driverRule);

    @Test
    public void shouldLoadItems() {
        ListViewPage page = driverRule
                .init(ListViewPage.class)
                .load();

        String expected = MoreObjects.toStringHelper(ListViewPage.class)
                .addValue(MoreObjects.toStringHelper(Items.class)
                        .addValue(Lists.newArrayList(
                                MoreObjects.toStringHelper(Item.class)
                                        .add("name", "Tiger Nixon")
                                        .add("Position", "System Architect"),
                                MoreObjects.toStringHelper(Item.class)
                                        .add("name", "Garrett Winters")
                                        .add("Position", "Accountant")
                        )))
                .toString();

        assertEquals(expected, page.toString());
    }

    public static class ListViewPage {

        @FindBy(className = "item")
        private Items items;

        public ListViewPage load() {
            this.items.onLoad();
            return this;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .addValue(items)
                    .toString();
        }
    }

    public static class Items extends ListView<Item> {

        public Items(WebDriver driver, By itemsBy, WebWait webWait) {
            super(driver, itemsBy, webWait);
        }
    }

    public static class Item {

        @FindBy(className = "name-data")
        private TextView name;

        @FindBy(className = "position-data")
        private TextView  position;

        public String getName() {
            return name.getValue();
        }

        public String getPosition() {
            return position.getValue();
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", getName())
                    .add("Position", getPosition())
                    .toString();
        }
    }


}
