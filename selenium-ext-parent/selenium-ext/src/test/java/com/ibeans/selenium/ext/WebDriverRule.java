package com.ibeans.selenium.ext;

import com.ibeans.selenium.ext.driver.Factory;
import com.ibeans.selenium.ext.driver.WebDriverFactory;
import org.junit.rules.ExternalResource;

import java.util.concurrent.TimeUnit;

/**
 * Created by igotavares on 26/05/2017.
 */
public class WebDriverRule extends ExternalResource {

    private final WebDriverFactory factory;
    private WebDriver driver;
    private final String url;

    public WebDriverRule(WebDriverFactory factory, String url) {
        this.factory = factory;
        this.url = url;
    }

    @Override
    protected void before() {
        this.driver = this.factory.create();
        this.driver.get(url);
        driver.manage().timeouts().implicitlyWait(1200, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(1200, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    public WebDriver get() {
        return this.driver;
    }

    public <T> T init(Class<T> pageClass) {
        return driver.page()
                .init(pageClass);
    }

    @Override
    protected void after() {
        if (driver != null) {
            driver.close();
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private static final String LOCALHOST = "";

        private String url;
        private WebDriverFactory factory;

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder internetExplorer() {
            return factory(Factory.internetExplorer());
        }

        public Builder firefox() {
            return factory(Factory.firefox());
        }

        public Builder factory(WebDriverFactory factory) {
            this.factory = factory;
            return this;
        }

        public WebDriverRule build() {
            return new WebDriverRule(factory, url);
        }

    }

}
