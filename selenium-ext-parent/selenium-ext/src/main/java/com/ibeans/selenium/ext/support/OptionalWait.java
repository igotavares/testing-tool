package com.ibeans.selenium.ext.support;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.ibeans.selenium.ext.execption.CompositeException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.Clock;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.SystemClock;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;


public class OptionalWait<E> {

    private static final String TIMEOUT_MESSAGE = "Expected condition failed: waiting for (tried for %d second(s) with %s interval)";

    public static final Duration FIVE_HUNDRED_MILLIS = new Duration(500, MILLISECONDS);

    private final Supplier<Optional<E>> input;
    private final Clock clock;
    private final Sleeper sleeper;

    private final Duration interval;
    private final Duration timeout;

    public OptionalWait(Supplier<Optional<E>> input, long timeOutInSeconds) {
        this(input, new SystemClock(), Sleeper.SYSTEM_SLEEPER, timeOutInSeconds);
    }

    public OptionalWait(Supplier<Optional<E>> input, Clock clock, Sleeper sleeper, long timeOutInSeconds) {
        this.input = checkNotNull(input);
        this.clock = checkNotNull(clock);
        this.sleeper = checkNotNull(sleeper);
        this.interval = FIVE_HUNDRED_MILLIS;
        this.timeout = new Duration(timeOutInSeconds, SECONDS);
    }

    public boolean isPresent() {
        return until(Optional::isPresent).isPresent();
    }

    public<U> Optional<U> map(Function<? super E, ? extends U> mapper) {
        return until(Optional::isPresent).map(mapper);
    }

    public void ifPresent(Consumer<? super E> consumer) {
        until(Optional::isPresent).ifPresent(consumer);
    }

    public E get() {
        return until(Optional::isPresent).get();
    }

    public Optional<E> filter(Predicate<? super E> predicate) {
        return until(Optional::isPresent).filter(predicate);
    }

    public E orElse(E other) {
        return until(Optional::isPresent).orElse(other);
    }

    public <X extends Throwable> E orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        try {
            return until(Optional::isPresent).get();
        } catch (CompositeException cause) {
            cause.addException(exceptionSupplier.get());
            throw cause;
        }
    }

    private Optional<E> until(Predicate<Optional<E>> isTrue) {
        long end = clock.laterBy(timeout.in(MILLISECONDS));
        List<Throwable> exceptions = new ArrayList<>();
        while (true) {
            Optional<E> optional = input.get();
            try {
                if (isTrue.test(optional)) {
                    return optional;
                }
            } catch (NoSuchElementException cause) {
            } catch (StaleElementReferenceException cause) {
                exceptions.add(cause);
            }
            if (!clock.isNowBefore(end)) {
                if (!exceptions.isEmpty()) {
                    return throwTimeoutException(exceptions);
                }
                return input.get();
            }

            try {
                sleeper.sleep(interval);
            } catch (InterruptedException cause) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private Optional<E> throwTimeoutException(List<Throwable> exceptions) {
        String message = String.format(TIMEOUT_MESSAGE, timeout.in(SECONDS), interval);
        throw new CompositeException(message, exceptions);
    }

}
