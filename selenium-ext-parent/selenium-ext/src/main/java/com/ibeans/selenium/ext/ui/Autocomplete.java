package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;


public class Autocomplete extends EditText {

    private static final boolean NOT_WAIT_ELEMENT_VISIBILITY = false;

    private Select autoCompleteSelect;

    public Autocomplete(WebDriver driver, WebElement autoCompleteSelect, WebElement autoCompleteText, WebWait webWait) {
        super(driver, autoCompleteText, webWait);
        this.autoCompleteSelect = new Select(driver, autoCompleteSelect, webWait);
    }

    @Override
    public void setValue(String value) {
        if (value != null) {
            super.setValue(value);
            selectText(value);
        }
    }

    public void selectText(String value) {
        autoCompleteSelect.selectByText(value, NOT_WAIT_ELEMENT_VISIBILITY);
    }

}
