package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.TextView;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Predicate;


public class DefaultTextViewsFactory extends DefaultElementLocatorFactory<List<TextView>, List<WebElement>> {

    private final Predicate<Field> test;

    public DefaultTextViewsFactory() {
        super(new DefaultElementsDecorator());

        this.test = new ElementsPredicate(TextView.class);
    }

    @Override
    public boolean test(Field field) {
        return test.test(field);
    }

    @Override
    public List<TextView> create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, elementLocator);
    }

    @Override
    public List<TextView> create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, elementLocator);
    }

    public List<TextView> create(WebDriver webDriver, Object page, ElementLocator elementLocator) {
        List<WebElement> elements = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        return new LocatingElementsDecorator()
                .decorate(page.getClass().getClassLoader(),
                        new LocatingElementsHandler<>(element -> {
                    WebWait webWait = WebWaitFactory.create(page);

                    return new TextView(webDriver, element, webWait);
                }, elements));
    }

}
