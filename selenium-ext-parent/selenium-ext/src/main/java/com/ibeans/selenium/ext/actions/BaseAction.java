package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;


public abstract class BaseAction implements Action {

    private final WebElement element;
    private final WebDriver driver;

    protected BaseAction(WebDriver driver) {
        this(driver, null);
    }

    protected BaseAction(WebDriver driver, WebElement element) {
        this.driver = driver;
        this.element = element;
    }

    protected WebElement getElement() {
        return element;
    }

    protected WebDriver getDriver() {
        return driver;
    }

}
