package com.ibeans.selenium.ext.ui.attribute;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class StyleAttribute extends Attribute {

    private static final String STYLE = "style";

    public StyleAttribute(WebDriver driver, WebElement element) {
        super(driver, element, STYLE);
    }

}
