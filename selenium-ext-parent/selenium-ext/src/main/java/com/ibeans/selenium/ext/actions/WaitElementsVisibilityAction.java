package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class WaitElementsVisibilityAction {

    private static final long DEFAULT_TIMEOUT = 25;

    private final WebDriver driver;
    private final List<WebElement> elements;
    private final WebDriverWait driverWait;

    public WaitElementsVisibilityAction(WebDriver driver, List<WebElement> elements) {
        this(driver, elements, DEFAULT_TIMEOUT);
    }

    public WaitElementsVisibilityAction(WebDriver driver, List<WebElement> elements, long timeout) {
        this.driver = driver;
        this.elements = elements;
        this.driverWait = new WebDriverWait(driver, timeout);
    }

    public void perform() {
        waitElementVisibility();
    }

    private void waitElementVisibility() {
        driverWait.until(ExpectedConditions
                .visibilityOfAllElements(elements));
    }
}
