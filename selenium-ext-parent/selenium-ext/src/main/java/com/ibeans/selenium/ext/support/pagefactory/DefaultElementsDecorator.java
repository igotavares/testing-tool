package com.ibeans.selenium.ext.support.pagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementListHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;


public class DefaultElementsDecorator implements ElementDecorator<List<WebElement>> {

    @Override
    public List<WebElement> decorate(ClassLoader loader, ElementLocator locator) {
        InvocationHandler handler = new LocatingElementListHandler(locator);

        return (List<WebElement>) Proxy.newProxyInstance(
                loader, new Class[] {List.class}, handler);
    }

}
