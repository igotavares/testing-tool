package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;
import org.openqa.selenium.WebElement;

import java.util.Collection;


public class WaitOnlyElementPresentAction extends WaitConditionAction {

    public WaitOnlyElementPresentAction(WebDriver driver, Collection<WebElement> elements, long timeout) {
        super(driver, timeout, createCondition(elements));
    }

    private static Predicate<WebDriver> createCondition(Collection<WebElement> elements) {
        return (driver) -> !elements.isEmpty();
    }

}
