package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;


public class WaitConditionAction extends BaseWaitAction {

    private final Predicate condition;

    public WaitConditionAction(WebDriver driver, long timeout, Predicate<WebDriver> condition) {
        super(driver, timeout);
        this.condition = condition;
    }

    @Override
    public void perform() {
        waitCondition();
    }

    private void waitCondition() {
        if (condition != null) {
            getDriverWait().until((driver) -> condition.apply(driver));
        }
    }

}
