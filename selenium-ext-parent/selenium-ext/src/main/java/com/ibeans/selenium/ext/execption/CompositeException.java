package com.ibeans.selenium.ext.execption;

import java.util.List;


public class CompositeException extends RuntimeException {

    private final List<Throwable> exceptions;

    public CompositeException(String message, List<Throwable> exceptions) {
        super(message);
        this.exceptions = exceptions;
    }

    public CompositeException(List<Throwable> exceptions) {
        this.exceptions = exceptions;
    }

    public void addException(Throwable exception) {
        exceptions.add(exception);
    }


}
