package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import com.google.common.base.Function;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.WebElement;


public class TextView extends Component {

    public TextView(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    public TextView(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

    public String getValue() {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .waitElemenVisibility(getWebWait().getTimeout())
                .highlight();

        return getElement().getText();
    }

    public LocalDateTime toDateTime() {
        return new ToDateTime().apply(this);
    }

    public LocalDate toDate() {
        return new ToDate().apply(this);
    }

    public static class ToDate implements Function<TextView, LocalDate> {

        public static final DateTimeFormatter DEFAULT_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd");

        @Override
        public LocalDate apply(TextView textView) {
            String dateTime = textView.getValue();
            if (!dateTime.isEmpty() && dateTime != null) {
                return LocalDate.parse(dateTime, DEFAULT_FORMAT);
            }
            return null;
        }
    }

    public static class ToDateTime implements Function<TextView, LocalDateTime> {

        public static final DateTimeFormatter DEFAULT_FORMAT
                = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        @Override
        public LocalDateTime apply(TextView textView) {
            String dateTime = textView.getValue();
            if (!dateTime.isEmpty() && dateTime != null) {
                return LocalDateTime.parse(dateTime, DEFAULT_FORMAT);
            }
            return null;
        }
    }

}
