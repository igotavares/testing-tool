package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.ListView;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.Annotations;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;


public class DefaultListViewFactory extends DefaultElementLocatorFactory<ListView, WebElement> {

    public DefaultListViewFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return ListView.class.isAssignableFrom(field.getType());
    }

    @Override
    public ListView create(WebDriver driver, Object page, Field field) {
        Annotations annotations = new Annotations(field);

        By by = annotations.buildBy();

        WebWait webWait = WebWaitFactory.create(page);

        ListView view = (ListView) instantiateTable
                (driver, by, webWait, field.getType());

        driver.init(view);

        return view;
    }

    @Override
    public ListView create(WebDriver driver, Object page, Field field, SearchContext searchContext) {
        Annotations annotations = new Annotations(field);

        By by = annotations.buildBy();

        WebWait webWait = WebWaitFactory.create(page);

        ListView view = (ListView) instantiateTable
                (driver, searchContext, by, webWait, field.getType());

        driver.init(view);

        return view;
    }


    private static <T> T instantiateTable(WebDriver driver, By by, WebWait webWait, Class<T> tableClass) {
        try {
            Constructor<T> constructor = tableClass.getConstructor(WebDriver.class, By.class, WebWait.class);
            return constructor.newInstance(driver, by, webWait);
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

    private static <T> T instantiateTable(WebDriver driver, SearchContext searchContext, By by, WebWait webWait, Class<T> tableClass) {
        try {
            Constructor<T> constructor = tableClass.getConstructor(WebDriver.class,
                    SearchContext.class, By.class, WebWait.class);
            return constructor.newInstance(driver, searchContext, by, webWait);
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

}

