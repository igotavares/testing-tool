package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;


public class CloseWindowsAction extends BaseAction {

    public CloseWindowsAction(WebDriver driver) {
        super(driver);
    }

    @Override
    public void perform() {
        closeWindows();
    }

    private void closeWindows() {
        final String currentWindowHandle = getDriver().getWindowHandle();
        for (String windowName : getDriver().getWindowHandles()) {
            getDriver().switchTo().window(windowName);
            getDriver().close();
        }
    }

}
