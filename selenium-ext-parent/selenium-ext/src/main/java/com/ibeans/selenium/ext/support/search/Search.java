package com.ibeans.selenium.ext.support.search;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;


public interface Search {

    String search(WebDriver webDriver, SearchContext search, WebWait webWait, By by);

}
