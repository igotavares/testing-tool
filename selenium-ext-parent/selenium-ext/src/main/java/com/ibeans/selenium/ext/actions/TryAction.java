package com.ibeans.selenium.ext.actions;

import org.openqa.selenium.interactions.Action;


public class TryAction implements Action {

    private final Action compositeAction;
    private final Integer times;

    public TryAction(Action compositeAction, Integer times) {
        this.compositeAction = compositeAction;
        this.times = times;
    }

    @Override
    public void perform() {
        RuntimeException caughtThrowable = null;

        for (int i = 0; i < times; i++) {
            try {
                compositeAction.perform();
                return;
            } catch (RuntimeException cause) {
                caughtThrowable = cause;
            }
        }

        if (caughtThrowable != null) {
            throw caughtThrowable;
        }
    }

}
