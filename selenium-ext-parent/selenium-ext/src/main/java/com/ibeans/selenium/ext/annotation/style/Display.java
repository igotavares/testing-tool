package com.ibeans.selenium.ext.annotation.style;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Display{

    String target() default "arguments[0]";

    Type value() default Type.BLOCK;

    enum Type {
        BLOCK,
        NONE
    }

}
