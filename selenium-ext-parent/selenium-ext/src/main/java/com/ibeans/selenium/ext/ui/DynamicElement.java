package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;


public class DynamicElement extends Component implements HasValue {

    private static final String SELECT_TAG = "select";

    private HasValue element;

    public DynamicElement(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

    public void setValue(String value) {
        HasValue element = getElementInstance();
        element.setValue(value);
    }

    public String getValue() {
        HasValue element = getElementInstance();
        return element.getValue();
    }

    private HasValue getElementInstance() {
        if (this.element == null) {
            this.element = createHasValue();
        }
        return this.element;
    }

    private HasValue createHasValue() {
        if (isSelect()) {
            return new Select(getDriver(), getElement(), getWebWait());
        }
        return new EditText(getDriver(), getElement(), getWebWait());
    }

    private boolean isSelect() {
        String tagName = getElement().getTagName();
        return tagName != null
                && SELECT_TAG.equals(tagName.toLowerCase());
    }

}
