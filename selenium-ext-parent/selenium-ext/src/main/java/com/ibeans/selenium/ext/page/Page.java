package com.ibeans.selenium.ext.page;

import com.ibeans.selenium.ext.WebDriver;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Page {

	private final WebDriver driver;

	public String getPageTitle() {
		return driver.getTitle();
	}

	protected String getValue(String value) {
		if (value != null && !value.isEmpty()) {
			return value;
		}
		return null;
	}

}
