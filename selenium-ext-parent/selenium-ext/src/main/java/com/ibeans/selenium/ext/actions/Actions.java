package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.style.Style;
import com.google.common.base.Predicate;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.MoveMouseAction;
import org.openqa.selenium.internal.Locatable;

import java.util.Collection;
import java.util.List;


public class Actions implements Action {

    private static final Integer TRY_TWICE = 2;
    private final WebDriver driver;
    private final WebElement element;
    private final CompositeAction actions;

    public Actions(WebDriver driver) {
        this(driver, null);
    }

    public Actions(WebDriver driver, WebElement element) {
        this(driver, element, new CompositeAction());
    }

    public Actions(WebDriver driver, WebElement element, CompositeAction actions) {
        this.driver = driver;
        this.element = element;
        this.actions = actions;
    }

    public Actions highlight() {
        actions.addAction(new HighlightAction(driver, element));
        return this;
    }

    public Actions enableEdit() {
        actions.addAction(new EnableEditAction(driver, element));
        return this;
    }

    public Actions clearNumber() {
        actions.addAction(new ClearNumberAction(driver, element));
        return this;
    }

    public Actions clear() {
        actions.addAction(new ClearAction(driver, element));
        return this;
    }

    public Actions onClick() {
        actions.addAction(new OnClickAction(driver, element));
        return this;
    }

    public Actions click() {
        actions.addAction(new ClickAction(driver, element));
        return this;
    }

    public Actions onCheck() {
        actions.addAction(new CheckAction(driver, element));
        return this;
    }

    public Actions click(WebElement element) {
        actions.addAction(new ClickAction(driver, element));
        return this;
    }

    public Actions onAddStyles(Collection<WebElement> elements, List<Style> styles, long timeout) {
        actions.addAction(new StyleAdditionAction(driver, elements, styles, timeout));
        return this;
    }

    public Actions clickAndSelectNewWindow(long timeout) {
        actions.addAction(new ClickAndSelectNewWindowAction(driver, element, timeout));
        return this;
    }

    public Actions submitAndSelectNewWindow() {
        actions.addAction(new SubmitAndSelectNewWindowAction(driver, element));
        return this;
    }

    public Actions onFocus() {
        actions.addAction(new FocusAction(driver, element));
        return this;
    }

    public Actions moveToElement() {
        return moveToElement(this.element);
    }

    public Actions moveToElement(Collection<WebElement> elements) {
        for (WebElement element : elements) {
            moveToElement(element);
        }
        return this;
    }

    public Actions moveToElement(WebElement element) {
        actions.addAction(
                new MoveMouseAction(((HasInputDevices)driver).getMouse(),
                        (Locatable) element));
        return this;
    }

    public Actions onBlur() {
        actions.addAction(new BlurAction(driver, element));
        return this;
    }

    public Actions waitValueOptionPresent(final String value, long timeout) {
        actions.addAction(new WaitValueOptionPresentAction(driver, element, value, timeout));
        return this;
    }

    public Actions waitTextPresent(final String value, long timeout) {
        actions.addAction(new WaitTextPresentAction(driver, element, value, timeout));
        return this;
    }

    public Actions waitElemenClickable(long timeout) {
        waitElemenClickable(element, timeout);
        return this;
    }

    public Actions sleep(long time) {
        actions.addAction(new SleepAction(time));
        return this;
    }

    public Actions waitElemenClickable(WebElement element, long timeout) {
        actions.addAction(new WaitElementClickableAction(driver, element, timeout));
        return this;
    }

    public Actions waitElemenVisibility(long timeout) {
        waitElemenVisibility(element, timeout);
        return this;
    }

    public Actions waitElemenVisibility(WebElement element, long timeout) {
        actions.addAction(new WaitElementVisibilityAction(driver, element, timeout));
        return this;
    }

    public Actions tryTwice(Action action) {
        return tryAgain(action, TRY_TWICE);
    }

    public Actions tryAgain(Action action, Integer times) {
        actions.addAction(new TryAction(action, times));
        return this;
    }

    public Actions tryUntil(Predicate<WebDriver> isTrue) {
        return tryUntil(actions.clone(), isTrue);
    }

    public Actions tryUntil(CompositeAction actions, Predicate<WebDriver> isTrue) {
        this.actions.addAction(new AgainAction(driver, actions, isTrue));
        return this;
    }

    public Actions waitConditions(Predicate<WebDriver>[] conditions, long timeout) {
        for (Predicate<WebDriver> condition : conditions) {
            waitCondition(condition, timeout);
        }
        return this;
    }

    public Actions waitCondition(Predicate<WebDriver> condition, long timeout) {
        actions.addAction(new WaitConditionAction(driver, timeout, condition));
        return this;
    }

    public Actions sendKeys(CharSequence... values) {
        actions.addAction(new SendKeysAction(driver, element, values));
        return this;
    }

    public Actions onSubmit() {
        actions.addAction(new SubmitAction(driver, element));
        return this;
    }

    public void perform() {
        this.actions.perform();
    }

    public Actions waitOnlyElementPresent(Collection<WebElement> elements, Long timeout) {
        actions.addAction(new WaitOnlyElementPresentAction(driver, elements, timeout));
        return this;
    }
}
