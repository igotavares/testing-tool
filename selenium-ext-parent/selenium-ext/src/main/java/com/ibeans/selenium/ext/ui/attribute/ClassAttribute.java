package com.ibeans.selenium.ext.ui.attribute;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class ClassAttribute extends Attribute {

    private static final String CLASS = "class";

    public ClassAttribute(WebDriver driver, WebElement element) {
        super(driver, element, CLASS);
    }

}
