package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.pagefactory.ByChained;


public abstract class Table<E> extends ListView<E> {

    private static final String ROW_XPATH = "./tbody/tr";

    private By tableBy;

    public Table(WebDriver driver, By tableBy, WebWait webWait) {
        this(driver, tableBy, new ByChained(tableBy, By.xpath(ROW_XPATH)), webWait);
    }

    public Table(WebDriver driver, By tableBy, By rowBy, WebWait webWait) {
        super(driver, rowBy, webWait);
        this.tableBy = tableBy;
    }

    public Table(WebDriver driver, SearchContext searchContext, By tableBy, WebWait webWait) {
        this(driver, searchContext, tableBy, new ByChained(tableBy, By.xpath(ROW_XPATH)), webWait);
    }

    public Table(WebDriver driver, SearchContext searchContext, By tableBy, By rowBy, WebWait webWait) {
        super(driver, searchContext, rowBy, webWait);
        this.tableBy = tableBy;
    }

    public By getTableBy() {
        return this.tableBy;
    }

    public By getRowBy() {
        return getBy();
    }



}
