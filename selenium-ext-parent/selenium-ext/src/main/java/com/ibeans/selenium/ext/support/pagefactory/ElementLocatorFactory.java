package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.SearchContext;

import java.lang.reflect.Field;
import java.util.function.Predicate;


public interface ElementLocatorFactory<E> extends Predicate<Field> {

    E create(WebDriver webDriver, Object page, Field field);

    E create(WebDriver webDriver, Object page, Field field, SearchContext searchContext);

}
