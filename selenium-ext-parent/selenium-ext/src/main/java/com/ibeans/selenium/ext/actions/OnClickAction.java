package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class OnClickAction extends BaseAction {

    private static final String ON_CLICK = "arguments[0].click();";

    public  OnClickAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        onClick();
    }

    public void onClick() {
        getDriver().executeScript(ON_CLICK, getElement());
    }

}
