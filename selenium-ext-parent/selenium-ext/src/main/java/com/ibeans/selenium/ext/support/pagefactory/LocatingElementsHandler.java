package com.ibeans.selenium.ext.support.pagefactory;

import org.openqa.selenium.WebElement;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class LocatingElementsHandler<E> implements InvocationHandler{

    private final Function<WebElement, E> converter;
    private final List<WebElement> elements;

    public LocatingElementsHandler(Function<WebElement, E> converter, List<WebElement> elements) {
        this.converter = converter;
        this.elements = elements;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
             return method.invoke(((List) elements)
                        .stream()
                        .map(converter)
                        .collect(Collectors.toList()), args);
        } catch (InvocationTargetException cause) {
            throw cause.getCause();
        }
    }
}
