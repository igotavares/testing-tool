package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class ClickAction extends BaseAction {

    public ClickAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        onClick();
    }

    public void onClick() {
        getElement().click();
    }

}
