package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import org.joda.time.LocalDate;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Datepicker extends Component {

    private static final String VALUE = "value";
    private String pattern;

    public Datepicker(WebDriver driver, WebElement element, String pattern, WebWait webWait) {
        super(driver, element, webWait);
        this.pattern = pattern;
    }

    public void setDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        if (date != null) {
            setDate(dateFormat.format(date));
        }
    }

    public void setDate(LocalDate date) {
        if (date != null) {
            setDate(date.toString(pattern));
        }
    }

    public void setDate(String date) {
        if (date != null) {
            new Actions(getDriver(), getElement())
                    .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                    .onFocus()
                    .enableEdit()
                    .highlight()
                    .clear()
                    .waitElemenVisibility(getWebWait().getTimeout())
                    .sendKeys(date, Keys.TAB)
                    .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout())
                    .perform();
        }
    }

    public String getValue() {
        return getDate();
    }

    public String getDate() {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .waitElemenVisibility(getWebWait().getTimeout())
                .highlight()
                .perform();

        return getElement()
                .getAttribute(VALUE);
    }


}
