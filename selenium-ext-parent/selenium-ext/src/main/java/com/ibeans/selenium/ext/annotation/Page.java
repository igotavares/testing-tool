package com.ibeans.selenium.ext.annotation;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Page {

    String hostName() default "";
    String port() default "";
    String host() default "";
    String protocol() default "";
    String pathName() default "";
    String url() default "";

    Class<? extends Predicate<WebDriver>> load() default NoWaiting.class;

}
