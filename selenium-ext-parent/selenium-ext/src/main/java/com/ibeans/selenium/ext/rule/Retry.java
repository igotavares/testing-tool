package com.ibeans.selenium.ext.rule;

import com.ibeans.selenium.ext.execption.PageException;
import com.ibeans.selenium.ext.execption.SeleniumException;
import lombok.RequiredArgsConstructor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.WebDriverException;


@RequiredArgsConstructor
public class Retry implements TestRule {

    private static final String ERROR_SELENIUM = "ERROR SELENIUM {0} TIMES";
    private static final int TWICE = 2;
    private final int retryCount;

    public Statement apply(Statement base, Description description) {
        return statement(base, description);
    }

    private Statement statement(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                for (int i = 0; i < retryCount; i++) {
                    try {
                        base.evaluate();
                        return;
                    } catch (WebDriverException | PageException | SeleniumException cause) {
                        throw cause;
                    }
                }
            }
        };
    }

    public static Retry tryTwice() {
        return new Retry(TWICE);
    }

}
