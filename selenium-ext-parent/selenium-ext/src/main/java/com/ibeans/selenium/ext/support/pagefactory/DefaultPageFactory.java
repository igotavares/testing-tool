package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.annotation.SubType;
import com.ibeans.selenium.ext.annotation.TypeInfo;
import com.ibeans.selenium.ext.support.search.Search;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;

import java.lang.reflect.Constructor;
import java.util.Arrays;


public class DefaultPageFactory<T> {

    private WebDriver driver;
    private SearchContext searchContext;
    private Class<T> pageClass;

    public DefaultPageFactory(WebDriver driver, Class<T> pageClass) {
        this(driver, driver, pageClass);
    }

    public DefaultPageFactory(WebDriver driver, SearchContext searchContext, Class<T> pageClass) {
        this.driver = driver;
        this.searchContext = searchContext;
        this.pageClass = pageClass;
    }

    public T create() {
        Class<?> currentPageClass = this.pageClass;
        while (currentPageClass  != null) {
            TypeInfo typeInfo = currentPageClass.getDeclaredAnnotation(TypeInfo.class);
            if (typeInfo != null) {
                return create(typeInfo);
            }
            currentPageClass = currentPageClass.getSuperclass();
        }
        return create(driver, pageClass);
    }

    protected T create(WebDriver driver, Class<T> pageClass) {
        try {
            try {
                Constructor<T> constructor = pageClass.getConstructor(WebDriver.class);
                return constructor.newInstance(driver);
            } catch (NoSuchMethodException e) {
                return pageClass.newInstance();
            }
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

    protected T create(TypeInfo typeInfo) {
        By by = new Annotations().toBy(typeInfo);
        Search search = createSearch(typeInfo.search());
        WebWait webWait = WebWaitFactory.create(pageClass);
        String nameClass = search.search(driver, searchContext, webWait, by);
        return create(typeInfo.subTypes(), nameClass);
    }

    protected T create(SubType[] subTypes, String nameClass) {
        Class<T> pageClass = (Class<T>) Arrays.stream(subTypes)
                .filter(subType -> subType.name().equalsIgnoreCase(nameClass))
                .map(subType -> subType.page())
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Page not found by name:" + nameClass));

        return create(driver, pageClass);
    }

    protected <T> T createSearch(Class<T> searchClass) {
        try {
            return searchClass.newInstance();
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

}
