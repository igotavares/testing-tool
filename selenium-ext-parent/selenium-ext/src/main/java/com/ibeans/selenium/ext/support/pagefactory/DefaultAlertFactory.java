package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.Alert;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Field;


public class DefaultAlertFactory extends DefaultElementLocatorFactory<Alert, WebElement> {

    public DefaultAlertFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Alert.class.isAssignableFrom(field.getType());
    }

    @Override
    public Alert create(WebDriver webDriver, Object page, Field field) {
        WebWait webWait = WebWaitFactory.create(page);

        return new Alert(webDriver, webWait);
    }

    @Override
    public Alert create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        return create(webDriver, page, field);
    }

}