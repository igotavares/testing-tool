package com.ibeans.selenium.ext.support.search;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;


public abstract class TagSearch implements Search {

    private String tag;

    public TagSearch(String tag) {
        this.tag = tag;
    }

    @Override
    public String search(WebDriver webDriver, SearchContext search, WebWait webWait, By by) {
        WebElement element = search.findElement(by);

        new Actions(webDriver, element)
                .waitElemenVisibility(webWait.getTimeout())
                .perform();

        return element.getAttribute(tag);
    }

}
