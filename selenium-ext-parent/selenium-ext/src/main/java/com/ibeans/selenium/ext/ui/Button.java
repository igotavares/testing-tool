package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import com.google.common.base.Predicate;
import org.openqa.selenium.WebElement;


public class Button extends Component {

    private Actions submitAndHoldAction;
    private long sleepTime;

    public Button(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    public Button(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

    public Button(WebDriver driver, WebElement element, WebWait webWait, long sleepTime) {
        this(driver, element, webWait);
        this.sleepTime = sleepTime;
    }

    public void onSubmitAndHold() {
        createSubmitAndHoldAction().perform();
    }

    private Actions createSubmitAndHoldAction() {
        if (this.submitAndHoldAction == null) {
            this.submitAndHoldAction = new Actions(getDriver(), getElement())
                    .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                    .waitElemenClickable(getWebWait().getTimeout())
                    .onFocus()
                    .highlight()
                    .sleep(sleepTime)
                    .onSubmit()
                    .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout());
        }
        return this.submitAndHoldAction;
    }

    public void onClickAndHold() {
        createClickAndHoldAction()
                .perform();
    }

    public void onClickAndHold(Predicate<WebDriver> condition) {
        createClickAndHoldAction()
                .tryUntil(condition)
                .perform();
    }

    protected Actions createClickAndHoldAction() {
        return new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .waitElemenClickable(getWebWait().getTimeout())
                .onFocus()
                .highlight()
                .sleep(sleepTime)
                .onClick()
                .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout());
    }

    public <E> E open(Class<E> page, Error error) {
        createClickAndHoldAction()
                .perform();

        error.apply(getDriver());

        return newPage(page);
    }

    public <E> E open(Class<E> page) {
        createClickAndHoldAction()
                .perform();

        return newPage(page);
    }

    public <E> E onSubmit(Class<E> page) {
        createSubmitAndHoldAction()
                .perform();

        return newPage(page);
    }

    public <E> E openNewWindow(Class<E> page) {
        Actions actions = new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .waitElemenClickable(getWebWait().getTimeout())
                .onFocus()
                .highlight()
                .sleep(sleepTime)
                .clickAndSelectNewWindow(getWebWait().getTimeout());

        new Actions(getDriver(), getElement())
                .tryTwice(actions)
                .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout())
                .perform();

        return newPage(page);
    }

    private <E> E newPage(Class<E> pageClass) {
        return getDriver().page().init(pageClass);
    }

}
