package com.ibeans.selenium.ext.execption;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.OutputType;


public class PageException extends com.ibeans.jbehave.ext.exception.PageException {

	private static final long serialVersionUID = 1091583381436860669L;

    public PageException(Exception cause, String url, String sourcePage, byte[] screenshot) {
        super(url, sourcePage, screenshot, cause);
    }

    public static void throwException(WebDriver driver, Exception cause) {
        throw create(driver, cause);
    }

    public static PageException create(WebDriver driver, Exception cause) {
        String url = driver.getCurrentUrl();
        String sourcePage = driver.getPageSource();
        byte[] page = driver.getScreenshotAs(OutputType.BYTES);
        return new PageException(cause, url, sourcePage, page);
    }

}
