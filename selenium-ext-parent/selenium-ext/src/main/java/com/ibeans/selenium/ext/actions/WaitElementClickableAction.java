package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class WaitElementClickableAction extends BaseWaitAction {

    public WaitElementClickableAction(WebDriver driver, WebElement element, long timeout) {
        super(driver, element, timeout);
    }

    @Override
    public void perform() {
        waitElementClickable();
    }

    private void waitElementClickable() {
        getDriverWait().until(ExpectedConditions
                .elementToBeClickable(getElement()));
    }

}
