package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class WaitTextPresentAction extends BaseWaitAction {

    private String value;

    public WaitTextPresentAction(final WebDriver driver, final  WebElement element,
                                 final String value, final long timeout) {
        super(driver, element, timeout);
        this.value = value;
    }

    @Override
    public void perform() {
        waitValuePresent();
    }

    private void waitValuePresent() {
        getDriverWait().until(ExpectedConditions
                .textToBePresentInElementValue(getElement(), value));
    }


}
