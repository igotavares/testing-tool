package com.ibeans.selenium.ext.page;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Processing implements Predicate<WebDriver> {

    interface Constants {
        String LONG_TASK_CONTENT = "long-task-content";
        String PROCESSING = "processing";
    }

    @Override
    public boolean apply(WebDriver driver) {
        return isNotElementPresent(driver, By.className(Constants.LONG_TASK_CONTENT))
                && isNotElementPresent(driver, By.id(Constants.PROCESSING));
    }

    public boolean isNotElementPresent(WebDriver driver, By by) {
        try {
            WebElement element = driver.findElement(by);
            return element == null;
        } catch (Exception cause) {
            return true;
        }
    }

}
