package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;


public class SubmitAndSelectNewWindowAction  extends BaseAction {

    public final Actions actions;

    public SubmitAndSelectNewWindowAction(WebDriver driver, WebElement element) {
        super(driver, element);
        this.actions = new Actions(driver, element);
    }

    @Override
    public void perform() {
        selectNewWindow();
    }

    private void selectNewWindow() {
        Set<String> otherWindows = getDriver().getWindowHandles();

        actions.onSubmit().perform();

        Set<String> currentWindows = getDriver().getWindowHandles();

        for (String windowName : currentWindows) {
            if (!otherWindows.contains(windowName)) {
                getDriver().switchTo().window(windowName)
                        .manage().window()
                        .maximize();
                return;
            }
        }

        throw new RuntimeException("New Window Not Found");
    }
}
