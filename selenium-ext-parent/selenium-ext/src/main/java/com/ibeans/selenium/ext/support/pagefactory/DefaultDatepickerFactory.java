package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.annotation.DateFormat;
import com.ibeans.selenium.ext.execption.AnnotationRequiredException;
import com.ibeans.selenium.ext.ui.Datepicker;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;


public class DefaultDatepickerFactory extends DefaultElementLocatorFactory<Datepicker, WebElement> {

    public DefaultDatepickerFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Datepicker.class.isAssignableFrom(field.getType());
    }

    @Override
    public Datepicker create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, field, elementLocator);
    }

    @Override
    public Datepicker create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, field, elementLocator);
    }

    protected Datepicker create(WebDriver webDriver, Object page, Field field, ElementLocator elementLocator) {
        WebElement element = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        WebWait webWait = WebWaitFactory.create(page);

        String pattern = getPattern(field);

        return new Datepicker(webDriver, element, pattern, webWait);
    }

    protected String getPattern(Field field) {
        DateFormat dateFormat = field.getAnnotation(DateFormat.class);
        if (dateFormat == null) {
            throw new AnnotationRequiredException("@DateFormat is required by datepicker element");
        }
        return dateFormat.value();
    }

}