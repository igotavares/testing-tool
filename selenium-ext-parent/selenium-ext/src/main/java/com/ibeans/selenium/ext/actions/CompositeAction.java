package com.ibeans.selenium.ext.actions;

import org.openqa.selenium.interactions.Action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CompositeAction implements Action {

    private List<Action> actionsList = new ArrayList();

    public CompositeAction() {
    }

    public void perform() {
        Iterator i$ = this.actionsList.iterator();

        while(i$.hasNext()) {
            Action action = (Action)i$.next();
            action.perform();
        }

    }

    public CompositeAction addAction(Action action) {
        this.actionsList.add(action);
        return this;
    }

    public int getNumberOfActions() {
        return this.actionsList.size();
    }


    @Override
    protected CompositeAction clone() {
        CompositeAction compositeAction = new CompositeAction();
        compositeAction.actionsList.addAll(this.actionsList);
        return compositeAction;
    }
}
