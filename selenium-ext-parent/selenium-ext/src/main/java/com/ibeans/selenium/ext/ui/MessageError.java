package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Optional;
import java.util.stream.Collectors;


public class MessageError implements Error {

    private By by;
    private Function<WebElement, String> converter;

    public MessageError(By by, Function<WebElement, String> converter) {
        this.by = by;
        this.converter = converter;
    }

    @Override
    public void apply(WebDriver driver) {
        Optional.ofNullable(driver.findElements(by).stream()
                    .filter(WebElement::isDisplayed)
                    .map(converter)
                    .collect(Collectors.toList()))
                .filter(errors -> !errors.isEmpty())
                .map(MessageErrorException::new)
                .ifPresent(exception -> {throw exception;});
    }

}
