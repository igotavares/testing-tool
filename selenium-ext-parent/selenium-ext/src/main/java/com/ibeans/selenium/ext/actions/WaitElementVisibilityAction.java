package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class WaitElementVisibilityAction extends BaseWaitAction {

    public WaitElementVisibilityAction(WebDriver driver, WebElement element, long timeout) {
        super(driver, element, timeout);
    }

    @Override
    public void perform() {
        waitElementVisibility();
    }

    private void waitElementVisibility() {
        getDriverWait().until(ExpectedConditions
                .visibilityOf(getElement()));
    }

}
