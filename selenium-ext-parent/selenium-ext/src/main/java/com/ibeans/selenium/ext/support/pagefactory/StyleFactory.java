package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.annotation.style.Display;
import com.ibeans.selenium.ext.annotation.style.Opacity;
import com.ibeans.selenium.ext.annotation.style.Visibility;
import com.ibeans.selenium.ext.ui.style.Style;
import com.google.common.collect.Lists;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;


public enum StyleFactory {

    STYLE(com.ibeans.selenium.ext.annotation.style.Style.class) {
        @Override
        public Style create(Annotation annotation) {
            com.ibeans.selenium.ext.annotation.style.Style style
                    = (com.ibeans.selenium.ext.annotation.style.Style) annotation;
            return new Style(style.target(), style.name(), style.value());
        }
    },
    DISPLAY(Display.class) {
        @Override
        public Style create(Annotation annotation) {
            Display display = (Display) annotation;
            return new Style(display.target(), "display", display
                    .value()
                    .name()
                    .toLowerCase());
        }
    },
    VISIBILITY(Visibility.class) {
        @Override
        public Style create(Annotation annotation) {
            Visibility visibility = (Visibility) annotation;
            return new Style(visibility.target(), "visibility", visibility
                    .value()
                    .name()
                    .toLowerCase());
        }
    },
    OPACITY(Opacity.class) {
        @Override
        public Style create(Annotation annotation) {
            Opacity opacity = (Opacity) annotation;
            return new Style(opacity.target(), "opacity", String.valueOf(opacity.value()));
        }
    };

    private final Class<? extends Annotation> type;

    StyleFactory(Class<? extends Annotation> type) {
        this.type = type;
    }

    private Class<? extends Annotation> getType() {
        return this.type;
    }

    private boolean isType(Annotation annotation) {
        return type.isInstance(annotation);
    }

    public abstract Style create(Annotation annotation);

    public static List<Style> cretea(Field field) {
        List<Style> styles = Lists.newArrayList();
        for (StyleFactory factory : values()) {
            Annotation annotation = field
                    .getAnnotation(factory.getType());
            if (annotation != null) {
                Style style = factory.create(annotation);
                styles.add(style);
            }
        }
        return styles;
    }

}
