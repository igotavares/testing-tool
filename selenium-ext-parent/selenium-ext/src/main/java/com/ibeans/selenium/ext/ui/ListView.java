/*
 * Copyright (c) @2017 Cardif.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Cardif
 * ("Confidential Information"). You shall not disclose such Confidential 
 * Information and shall use it only in accordance with the terms of the 
 * license agreement you entered into with Cardif.
 */
package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.support.OptionalWait;
import com.ibeans.selenium.ext.support.proxy.IndexedPageFactory;
import com.ibeans.selenium.ext.wait.WebWait;
import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * List View component.
 * 
 * @author Andre Dias (andre.dias@summa.com.br)
 * @since 2017-07-04
 */
public abstract class ListView<E> {
	
    private Class<E> elementClass;

    @Getter private final WebDriver driver;
    private final SearchContext searchContext;
    private final By by;
    private final List<E> items;
    @Getter private final WebWait webWait;

    public ListView(WebDriver driver, By by, WebWait webWait) {
        this(driver, driver, by, webWait);
    }

    public ListView(WebDriver driver, SearchContext searchContext, By by, WebWait webWait) {
        this.driver = driver;
        this.searchContext = searchContext;
        this.by = by;
        this.webWait = webWait;
        this.elementClass = getElementClass();
        this.items = Lists.newArrayList();
    }

    private Class<E> getElementClass() {
        Type type = getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] typeArguments = parameterizedType.getActualTypeArguments();
            return (Class<E>) typeArguments[0];
        }
        throw new RuntimeException();
    }


    public void onLoadAndHold() {
        new Actions(this.driver)
                .waitConditions(webWait.preUnit(), webWait.getTimeout())
                .perform();

        onLoad();
    }

    public void onLoadAndHold(Predicate<WebDriver>... isTrue) {
        new Actions(this.driver)
                .waitConditions(isTrue, webWait.getTimeout())
                .perform();

        onLoad();
    }

    public void onLoad() {
        items.clear();
        if (hasResult()) {
            mapResult(driver.findElements(by));
        }
    }

    protected void mapResult(List<WebElement> elements) {
        for (int index = 0; index < elements.size(); index++) {
            WebElement element = elements.get(index);
            if (isElementExpected(element)) {
                E value = mapRow(element);
                if (value != null) {
                    value = (E) IndexedPageFactory.create(driver, searchContext, by, index, value);
                    items.add(value);
                }
            }
        }
        sort();
    }

    protected void sort() { }

    protected boolean isElementExpected(WebElement element) {
        return true;
    }

    protected E mapRow(WebElement row) {
        return this.driver.page().init(row, elementClass);
    }

    public boolean hasResult() {
        return hasElement(by);
    }

    protected boolean hasElement(By by) {
        return hasElement(searchContext, by);
    }

    protected boolean hasElement(SearchContext context, By by) {
        try {
            List<WebElement> elements = context.findElements(by);
            return elements != null
                    && !elements.isEmpty();
        } catch (NoSuchElementException cause) {
            return false;
        }
    }

    public List<E> getItems() {
        new Actions(this.driver)
                .waitConditions(webWait.preUnit(), webWait.getTimeout())
                .perform();

        return this.items;
    }

    protected OptionalWait<E> getFirst(Predicate<E> search) {
        return createOptionWait(() -> {
            onLoad();
            return getItems().stream()
                    .filter(search)
                    .findFirst();
        });
    }

    private OptionalWait<E> createOptionWait(Supplier<Optional<E>> supplier) {
        return new OptionalWait<E>(supplier, getWebWait().getTimeout());
    }

    protected <G> List<G> to(Function<E, G> map) {
        return getItems().stream().map(map)
                .collect(Collectors.toList());
    }

    protected void action(Predicate<E> search, Consumer<E> action) {
        getItems().stream().filter(search)
                .forEach(action);
    }

    protected By getBy() {
        return this.by;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .addValue(items)
                .toString();
    }

}