package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.execption.NewWindowNotFoundException;
import org.openqa.selenium.WebElement;

import java.util.LinkedHashSet;
import java.util.Set;


public class ClickAndSelectNewWindowAction extends BaseAction {

    public final long timeout;
    public final Actions actions;

    public ClickAndSelectNewWindowAction(WebDriver driver, WebElement element, long timeout) {
        super(driver, element);
        this.actions = new Actions(driver, element);
        this.timeout = timeout;
    }

    @Override
    public void perform() {
        selectNewWindow();
    }

    private void selectNewWindow() {
        final Set<String> otherWindows = new LinkedHashSet<>(getDriver().getWindowHandles());

        actions.click().waitCondition(
                (driver) -> otherWindows.size() != getDriver().getWindowHandles().size(), timeout)
                .perform();

        Set<String> currentWindows = getDriver().getWindowHandles();

        for (String windowName : currentWindows) {
            if (!otherWindows.contains(windowName)) {
                getDriver().switchTo().window(windowName)
                        .manage().window()
                        .maximize();
                return;
            }
        }

        throw new NewWindowNotFoundException();
    }

}
