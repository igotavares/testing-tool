package com.ibeans.selenium.ext.support.pagefactory;

import org.openqa.selenium.support.pagefactory.ElementLocator;


public interface ElementDecorator<E> {

    E decorate(ClassLoader loader, ElementLocator locator);

}
