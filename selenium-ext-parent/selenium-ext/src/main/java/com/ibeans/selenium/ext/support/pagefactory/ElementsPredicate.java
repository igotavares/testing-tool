package com.ibeans.selenium.ext.support.pagefactory;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Predicate;


@RequiredArgsConstructor
public class ElementsPredicate implements Predicate<Field> {

    private final Class<?> expected;

    @Override
    public boolean test(Field field) {
        if (!List.class.isAssignableFrom(field.getType())) {
            return false;
        }

        Type genericType = field.getGenericType();
        if (!(genericType instanceof ParameterizedType)) {
            return false;
        }

        Type listType = ((ParameterizedType) genericType).getActualTypeArguments()[0];
        return expected.equals(listType);
    }
}
