package com.ibeans.selenium.ext.annotation;

import com.ibeans.selenium.ext.support.search.Search;
import com.ibeans.selenium.ext.support.search.SelectSearch;
import org.openqa.selenium.support.How;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TypeInfo {

    How how() default How.UNSET;

    String using() default "";

    String id() default "";

    String name() default "";

    String className() default "";

    String css() default "";

    String tagName() default "";

    String linkText() default "";

    String partialLinkText() default "";

    String xpath() default "";

    SubType[] subTypes();

    Class<? extends Search> search() default SelectSearch.class;

}
