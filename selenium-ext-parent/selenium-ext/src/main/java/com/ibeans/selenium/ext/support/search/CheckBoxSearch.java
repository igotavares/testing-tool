package com.ibeans.selenium.ext.support.search;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.Checkbox;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

/**
 * Created by b83795 on 10/10/2017.
 */
public class CheckBoxSearch  implements Search {

    @Override
    public String search(WebDriver webDriver, SearchContext search, WebWait webWait, By by) {
        WebElement element = search.findElement(by);
        return String.valueOf(new Checkbox(webDriver, element, webWait).isChecked());
    }
}
