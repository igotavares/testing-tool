package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Predicate;


public class DefaultWebElementsFactory extends DefaultElementLocatorFactory<List<WebElement>, List<WebElement>>  {

    private final Predicate<Field> test;

    public DefaultWebElementsFactory() {
        super(new DefaultElementsDecorator());
        this.test = new ElementsPredicate(WebElement.class);
    }

    @Override
    public boolean test(Field field) {
        return test.test(field);
    }

    @Override
    public List<WebElement> create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return decorate(page, elementLocator);
    }

    @Override
    public List<WebElement> create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return decorate(page, elementLocator);
    }

    protected List<WebElement> decorate(Object page, ElementLocator elementLocator) {
        return fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);
    }

}
