package com.ibeans.selenium.ext.support.proxy;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.annotation.Refresh;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;

import java.lang.reflect.Method;


public class IndexedPageProxy implements MethodInterceptor {

    private WebDriver driver;
    private SearchContext searchContext;
    private By by;
    private By indexBy;
    private Object target;

    public IndexedPageProxy(WebDriver driver, SearchContext searchContext, By by, Integer index, Object target) {
        this(driver, searchContext, by, By.xpath(".[" + index + "]"), target);
    }

    public IndexedPageProxy(WebDriver driver, SearchContext searchContext, By by, By indexBy, Object target) {
        this.driver = driver;
        this.searchContext = searchContext;
        this.by = by;
        this.indexBy = indexBy;
        this.target = target;
    }

    @Override
    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        Refresh refresh = method.getDeclaredAnnotation(Refresh.class);
        Object value = method.invoke(target, args);
        if (refresh == null) {
            return value;
        }
        return refresh();
    }

    public Object refresh() {
        SearchContext searchContext = this.searchContext.findElement(by);
        Object page = driver.page().init(searchContext, target.getClass());
        return Enhancer.create(page.getClass(), create(page));
    }

    public IndexedPageProxy create(Object page) {
        return new IndexedPageProxy(driver, searchContext, by, indexBy, page);
    }


}
