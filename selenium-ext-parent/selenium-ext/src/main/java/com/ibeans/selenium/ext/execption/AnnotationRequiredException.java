package com.ibeans.selenium.ext.execption;


public class AnnotationRequiredException extends SeleniumException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2656315309280538085L;

	public AnnotationRequiredException(String message) {
        super(message);
    }

}
