package com.ibeans.selenium.ext.support;

import com.google.common.collect.Iterables;
import org.openqa.selenium.*;

import java.util.Collection;
import java.util.List;


public class LastWebElement implements WebElement {

    private Collection<WebElement> elements;

    public LastWebElement(Collection<WebElement> elements) {
        this.elements = elements;
    }

    @Override
    public void click() {
        last().click();
    }

    @Override
    public void submit() {
        last().submit();
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        last().sendKeys(charSequences);
    }

    @Override
    public void clear() {
        last().clear();
    }

    @Override
    public String getTagName() {
        return last().getTagName();
    }

    @Override
    public String getAttribute(String name) {
        return last().getAttribute(name);
    }

    @Override
    public boolean isSelected() {
        return last().isSelected();
    }

    @Override
    public boolean isEnabled() {
        return last().isEnabled();
    }

    @Override
    public String getText() {
        return last().getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return last().findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return last().findElement(by);
    }

    @Override
    public boolean isDisplayed() {
        return last().isDisplayed();
    }

    @Override
    public Point getLocation() {
        return last().getLocation();
    }

    @Override
    public Dimension getSize() {
        return last().getSize();
    }

    @Override
    public Rectangle getRect() {
        return last().getRect();
    }

    @Override
    public String getCssValue(String s) {
        return last().getCssValue(s);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return last().getScreenshotAs(outputType);
    }

    private WebElement last() {
        return Iterables.getLast(elements);
    }

}
