package com.ibeans.selenium.ext.support;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.support.pagefactory.DefaultPageFactory;
import com.ibeans.selenium.ext.support.pagefactory.Factory;
import org.openqa.selenium.SearchContext;

import java.lang.reflect.Field;


public class PageFactory {

    public static <T> T initElements(WebDriver driver, Class<T> componentClass, SearchContext searchContext) {
        T page = new DefaultPageFactory<T>(driver, searchContext, componentClass).create();
        initElements(driver, page, searchContext);
        return page;
    }

    public static void initElements(WebDriver driver, Object page, SearchContext searchContext) {
        Class<?> proxyIn = page.getClass();
        while (proxyIn != Object.class) {
            proxyFields(driver, page, proxyIn, searchContext);
            proxyIn = proxyIn.getSuperclass();
        }
    }

    private static void proxyFields(WebDriver driver, Object page, Class<?> proxyIn, SearchContext searchContext) {
        Field[] fields = proxyIn.getDeclaredFields();
        for (Field field : fields) {
            Object value = Factory.getInstance().create(driver, page, field, searchContext);
            if (value != null) {
                try {
                    field.setAccessible(true);
                    field.set(page, value);
                } catch (IllegalAccessException cause) {
                    throw new RuntimeException(cause);
                }
            }
        }
    }

    public static <T> T initElements(WebDriver driver, Class<T> pageClass) {
        T page = new DefaultPageFactory<T>(driver, pageClass).create();
        initElements(driver, page);
        return page;
    }

    public static void initElements(WebDriver driver, Object page) {
        Class<?> proxyIn = page.getClass();
        while (proxyIn != Object.class) {
            proxyFields(driver, page, proxyIn);
            proxyIn = proxyIn.getSuperclass();
        }
    }

    private static void proxyFields(WebDriver driver, Object page, Class<?> proxyIn) {
        Field[] fields = proxyIn.getDeclaredFields();
        for (Field field : fields) {
            Object value = Factory.getInstance().create(driver, page, field);
            if (value != null) {
                try {
                    field.setAccessible(true);
                    field.set(page, value);
                } catch (IllegalAccessException cause) {
                    throw new RuntimeException(cause);
                }
            }
        }
    }
}
