package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.page.Page;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;


public class Checkbox extends Component {

    public Checkbox(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    public Checkbox(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

    public void onCheck() {
        new Actions(getDriver(), getElement())
                .tryTwice(new Actions(getDriver(), getElement())
                            .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                            .onFocus()
                            .highlight()
                            .onCheck()
                            .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout()))
                .perform();
    }

    public <P extends Page> P setValue(Boolean value, Class<P> pageClass) {
        if (isChecked() != value) {
            onClick();
        }

        return getDriver().init(pageClass);
    }

    public void setValue(Boolean value) {
        if (isChecked() != value) {
            onClick();
        }
    }

    public void onClick() {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .onFocus()
                .highlight()
                .click()
                .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout())
                .perform();
    }

    public boolean isChecked() {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .onFocus()
                .highlight()
                .perform();

        return getElement().isSelected();
    }


}
