package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.Radio;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;


public class DefaultRadioFactory extends DefaultElementLocatorFactory<Radio, WebElement> {

    public DefaultRadioFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Radio.class.isAssignableFrom(field.getType());
    }

    @Override
    public Radio create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, elementLocator);
    }

    @Override
    public Radio create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, elementLocator);
    }


    public Radio create(WebDriver webDriver, Object page, ElementLocator elementLocator) {
        WebElement element = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        WebWait webWait = WebWaitFactory.create(page);

        return new Radio(webDriver, element, webWait);
    }

}
