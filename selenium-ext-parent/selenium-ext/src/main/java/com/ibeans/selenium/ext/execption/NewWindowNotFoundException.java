package com.ibeans.selenium.ext.execption;


public class NewWindowNotFoundException extends SeleniumException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -376293463613569089L;

	public NewWindowNotFoundException() {
        super("New Window Not Found");
    }
}
