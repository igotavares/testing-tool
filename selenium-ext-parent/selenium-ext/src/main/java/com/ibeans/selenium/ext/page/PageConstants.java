package com.ibeans.selenium.ext.page;

public interface PageConstants {

	int LOAD_PAGE_TIMEOUT = 60 * 4;
	int IMPLICITLY_WAIT = 1000;

}
