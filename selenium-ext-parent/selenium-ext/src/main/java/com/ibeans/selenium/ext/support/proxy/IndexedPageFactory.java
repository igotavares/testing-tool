package com.ibeans.selenium.ext.support.proxy;

import com.ibeans.selenium.ext.WebDriver;
import net.sf.cglib.proxy.Enhancer;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;


public class IndexedPageFactory {

    public static Object create(WebDriver driver, SearchContext searchContext, By by, Integer index, Object target) {
        IndexedPageProxy indexedPageProxy = new IndexedPageProxy(driver, searchContext, by, index, target);
        try {
            return Enhancer.create(target.getClass(), indexedPageProxy);
        } catch (IllegalArgumentException cause) {
            try {
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(target.getClass());
                enhancer.setCallback(indexedPageProxy);
                return enhancer.create(new Class[]{WebDriver.class}, new Object[]{driver});
            } catch (Exception ex) {
                return target;
            }
        }

    }

}
