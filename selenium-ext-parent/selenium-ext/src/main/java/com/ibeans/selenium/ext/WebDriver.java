package com.ibeans.selenium.ext;

import com.ibeans.selenium.ext.support.PageFactory;
import org.openqa.selenium.*;

import java.util.List;
import java.util.Set;


public class WebDriver implements org.openqa.selenium.WebDriver, TakesScreenshot, JavascriptExecutor {

    private org.openqa.selenium.WebDriver driver;

    public WebDriver(org.openqa.selenium.WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public void get(String url) {
        driver.get(url);
    }

    @Override
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    @Override
    public String getPageSource() {
        return driver.getPageSource();
    }

    @Override
    public void close() {
        driver.close();
    }

    @Override
    public void quit() {
        driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return driver.navigate();
    }

    @Override
    public Options manage() {
        return driver.manage();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        if (driver instanceof TakesScreenshot) {
            return ((TakesScreenshot) driver).getScreenshotAs(outputType);
        }
        throw new IllegalArgumentException("Driver is not TakesScreenshot");
    }

    public <T> T init(Class<T> pageClass) {
        return page().init(pageClass);
    }

    public void init(Object page) {
        page().init(page);
    }

    public Page page() {
        return new Page() {
            @Override
            public <T> T init(Class<T> pageClass) {
                return PageFactory.initElements(WebDriver.this, pageClass);
            }

            @Override
            public <T> T init(SearchContext search, Class<T> componentClass) {
                return PageFactory.initElements(WebDriver.this, componentClass, search);
            }

            @Override
            public void init(Object page) {
                PageFactory.initElements(WebDriver.this, page);
            }

        };
    }

    @Override
    public Object executeScript(String script, Object... objects) {
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript(script, objects);
        }
        return null;
    }

    @Override
    public Object executeAsyncScript(String script, Object... objects) {
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeAsyncScript(script, objects);
        }
        return null;
    }


    public interface Page {
        <T> T init(Class<T> pageClass);

        <T> T init(SearchContext search, Class<T> componentClass);

        void init(Object page);
    }

    public org.openqa.selenium.WebDriver getDriver() {
        return driver;
    }

    public void navigateTo(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }


}
