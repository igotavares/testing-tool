package com.ibeans.selenium.ext.ui.attribute;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;


public class Attribute {

    private static final String SET_ATTRIBUTE= "arguments[0].setAttribute(arguments[1], arguments[2]);";

    private final WebDriver driver;
    private final WebElement element;
    private final String name;

    public Attribute(WebDriver driver, WebElement element, String name) {
        this.driver = driver;
        this.name = name;
        this.element = element;
    }

    public void setValue(String value) {
        if (driver instanceof JavascriptExecutor) {
            JavascriptExecutor javascript = (JavascriptExecutor) driver;

            javascript.executeScript(SET_ATTRIBUTE, element, name, value);
        }
    }

    public String getValue() {
        return element.getAttribute(name);
    }

    public Boolean equals(String value) {
        if (value != null) {
            String valueAttribute = getValue();
            return valueAttribute != null
                    && !valueAttribute.isEmpty()
                    && valueAttribute.equalsIgnoreCase(value);
        }
        return Boolean.FALSE;
    }

    public Boolean contains(String value) {
        if (value != null) {
            String valueAttribute = getValue();
            return valueAttribute != null
                    && !valueAttribute.isEmpty()
                    && valueAttribute.contains(value);
        }
        return Boolean.FALSE;
    }

}
