package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.Table;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.Annotations;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;


public class DefaultTableFactory extends DefaultElementLocatorFactory<Table, WebElement> {

    public DefaultTableFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Table.class.isAssignableFrom(field.getType());
    }

    @Override
    public Table create(WebDriver driver, Object page, Field field) {
        Annotations annotations = new Annotations(field);

        By by = annotations.buildBy();

        WebWait webWait = WebWaitFactory.create(page);

        Table table = (Table) instantiateTable
                (driver, by, webWait, field.getType());

        driver.init(table);

        return table;
    }

    @Override
    public Table create(WebDriver driver, Object page, Field field, SearchContext searchContext) {
        Annotations annotations = new Annotations(field);

        By by = annotations.buildBy();

        WebWait webWait = WebWaitFactory.create(page);

        Table table = (Table) instantiateTable
                (driver, by, webWait, field.getType());

        driver.init(table);

        return table;
    }


    private static <T> T instantiateTable(WebDriver driver, By by, WebWait webWait, Class<T> tableClass) {
        try {
            Constructor<T> constructor = tableClass.getConstructor(WebDriver.class, By.class, WebWait.class);
            return constructor.newInstance(driver, by, webWait);
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }

}

