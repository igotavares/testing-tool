package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AgainAction implements Action {

    private static final long DEFAULT_TIMEOUT = 3 * 60;
    private static final long DEFAULT_SLEEP =  1000; 
    
    private final CompositeAction compositeAction;
    private final Predicate isTrue;
    private final WebDriverWait driverWait;
    
    public AgainAction(WebDriver driver, CompositeAction compositeAction, Predicate<WebDriver> isTrue) {
        this(driver, compositeAction, DEFAULT_TIMEOUT, isTrue);
    }

    public AgainAction(WebDriver driver, CompositeAction compositeAction, long timeout, Predicate<WebDriver> isTrue) {
        this.compositeAction = compositeAction;
        this.isTrue = isTrue;
        this.driverWait = new WebDriverWait(driver, timeout, DEFAULT_SLEEP);
    }

    public void perform() {
        tryAgain();
    }

    private void tryAgain() {
        driverWait.until((driver) -> {
            boolean result = isTrue.apply(driver);
            if (!result) {
                compositeAction.perform();
            }
            return result;
        });
    }



}
