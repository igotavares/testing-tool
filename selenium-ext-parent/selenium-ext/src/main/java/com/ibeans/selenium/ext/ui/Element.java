package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;


public class Element extends Component {

    public Element(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

}
