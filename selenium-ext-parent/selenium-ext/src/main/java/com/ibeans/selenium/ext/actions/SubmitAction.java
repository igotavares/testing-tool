package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class SubmitAction extends BaseAction {

    protected SubmitAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        submit();
    }

    private void submit() {
        getElement().submit();
    }

}
