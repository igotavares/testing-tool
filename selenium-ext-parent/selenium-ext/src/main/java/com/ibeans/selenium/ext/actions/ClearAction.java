package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class ClearAction extends BaseAction {

    public ClearAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        clear();
    }

    private void clear() {
        getElement().clear();
    }

}
