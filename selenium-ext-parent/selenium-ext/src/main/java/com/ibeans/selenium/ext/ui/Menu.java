package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.support.LastWebElement;
import com.ibeans.selenium.ext.ui.style.Style;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;

import java.util.Collection;
import java.util.List;


public class Menu extends Component {

    private Collection<WebElement> items;

    public Menu(WebDriver driver, Collection<WebElement> items, List<Style> styles,
                WebWait webWait) {
        super(driver, new LastWebElement(items), styles, webWait);
        this.items = items;
    }

    public Menu(WebDriver driver, Collection<WebElement> items, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
        this.items = items;
    }

    public <E> E openPage(Class<E> page) {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .waitOnlyElementPresent(items, getWebWait().getTimeout())
                .onAddStyles(items, getStyles(), getWebWait().getTimeout())
                .waitElemenClickable(getWebWait().getTimeout())
                .click()
                .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout())
                .perform();

        return newPage(page);
    }

    public <E> E openNewWindow(Class<E> page) {
        Actions actions =  new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout());

        Actions menuActions =  new Actions(getDriver(), getElement())
                .onAddStyles(items, getStyles(), getWebWait().getTimeout())
                .waitElemenClickable(getWebWait().getTimeout())
                .clickAndSelectNewWindow(getWebWait().getTimeout());

        actions.tryTwice(menuActions)
                .perform();

        return newPage(page);
    }

    private <E> E newPage(Class<E> page) {
        return getDriver().page().init(page);
    }

}
