package com.ibeans.selenium.ext.execption;


public class SeleniumException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -766998021171143404L;

	public SeleniumException() {
    }

    public SeleniumException(String message) {
        super(message);
    }

    public SeleniumException(String message, Exception cause) {
        super(message, cause);
    }
}
