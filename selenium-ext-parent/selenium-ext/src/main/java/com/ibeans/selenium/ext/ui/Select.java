package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;


public class Select implements HasValue {

    private static final String VALUE = "value";
    private final WebDriver driver;
    private final WebElement element;
    private final WebWait webWait;
    private org.openqa.selenium.support.ui.Select select;
    private boolean waitingValue;

    public Select(WebDriver driver, WebElement element, WebWait webWait) {
        this(driver, element, webWait, Boolean.FALSE);
    }

    public Select(WebDriver driver, WebElement element, WebWait webWait, boolean waitingValue) {
        this.driver = driver;
        this.element = element;
        this.webWait = webWait;
        this.waitingValue = waitingValue;
    }

    public List<String> getAllValues() {
        return Lists.transform(getInstance().getAllSelectedOptions(), new Function<WebElement, String>() {
            @Override
            public String apply(WebElement element) {
                return element.getText();
            }
        });
    }

    public void selectByValue(String value) {
        if (value != null) {
            Actions actions = new Actions(driver, element)
                    .waitConditions(webWait.preUnit(), webWait.getTimeout())
                    .onFocus()
                    .highlight()
                    .waitElemenVisibility(webWait.getTimeout());

            if (waitingValue) {
                actions.waitValueOptionPresent(value, webWait.getTimeout());
            }

            actions.perform();

            getInstance().selectByValue(value);

            new Actions(driver, element)
                    .waitConditions(webWait.postUnit(), webWait.getTimeout())
                    .perform();
        }
    }

    public void selectByText(String value) {
        selectByText(value, Boolean.TRUE);
    }

    public void selectByText(String value, boolean waitElemntVisibility) {
        if (value != null) {
            Actions actions = new Actions(driver, element)
                    .waitConditions(webWait.preUnit(), webWait.getTimeout());

            if (waitElemntVisibility) {
                actions.waitElemenVisibility(webWait.getTimeout());
            }

            actions.onFocus()
                    .highlight()
                    .perform();

            org.openqa.selenium.support.ui.Select select = getInstance();

            try {
                select.selectByVisibleText(value);
            } catch (NoSuchElementException cause) {
                throw new IllegalArgumentException("Cannot locate option with value: " + value
                        + ", but there are the values: " + getTextOptions());
            }

            new Actions(driver, element)
                    .waitConditions(webWait.postUnit(), webWait.getTimeout())
                    .perform();
        }
    }

    public String getTextOptions() {
        return getInstance().getOptions()
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.joining(","));
    }

    public String getFirstSelectedText() {
        new Actions(driver, element)
                .waitConditions(webWait.preUnit(), webWait.getTimeout())
                .onFocus()
                .highlight()
                .waitElemenVisibility(webWait.getTimeout())
                .perform();

        org.openqa.selenium.support.ui.Select select = getInstance();

        try {
            return select.getFirstSelectedOption()
                    .getText();
        } catch (NoSuchElementException cause) {
            return null;
        }
    }

    private org.openqa.selenium.support.ui.Select getInstance() {
        if (this.select == null) {
            this.select = new org.openqa.selenium.support.ui.Select(this.element);
        }
        return this.select;
    }

    @Override
    public String getValue() {
        return getFirstSelectedText();
    }

    public String getFirstSelectedValue() {
        new Actions(driver, element)
                .waitConditions(webWait.preUnit(), webWait.getTimeout())
                .waitElemenVisibility(webWait.getTimeout())
                .onFocus()
                .highlight()
                .perform();

        return getInstance()
                .getFirstSelectedOption()
                .getAttribute(VALUE);
    }


    @Override
    public void setValue(String value) {
        selectByValue(value);
    }
}
