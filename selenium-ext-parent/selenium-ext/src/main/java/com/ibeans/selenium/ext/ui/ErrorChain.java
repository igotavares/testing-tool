package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;

import java.util.Arrays;
import java.util.List;


public class ErrorChain implements Error {

    private final List<Error> errors;

    public ErrorChain(Error... errors) {
        this.errors = Arrays.asList(errors);
    }

    @Override
    public void apply(WebDriver driver) {
        for (Error error : errors) {
            error.apply(driver);
        }
    }


}
