package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


public class ClearNumberAction extends BaseAction {

    private static final String VALUE_ATTRIBUTE = "value";

    public ClearNumberAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        clear();
    }

    private void clear() {
        String value = getElement().getAttribute(VALUE_ATTRIBUTE);
        for (int index = 0; index < value.length(); index++) {
            getElement().sendKeys(Keys.BACK_SPACE);
        }
    }
}
