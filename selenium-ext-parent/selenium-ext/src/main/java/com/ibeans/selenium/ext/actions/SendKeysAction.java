package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class SendKeysAction extends BaseAction {

    private final CharSequence[] values;

    public SendKeysAction(WebDriver driver, WebElement element, CharSequence... values) {
        super(driver, element);
        this.values = values;
    }

    @Override
    public void perform() {
        sendKeys();
    }

    private void sendKeys() {
        getElement().sendKeys(values);
    }

}
