package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class HighlightAction extends BaseAction {

    private static final String SET_STYLE = "arguments[0].setAttribute('style', arguments[1]);";
    private static final String HIGHLIGHT_STYLE = "background-color: yellow;  border: 1px solid red;";

    public HighlightAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        highlight();
    }

    private void highlight() {
        getDriver().executeScript(SET_STYLE, getElement(), HIGHLIGHT_STYLE);
    }

}
