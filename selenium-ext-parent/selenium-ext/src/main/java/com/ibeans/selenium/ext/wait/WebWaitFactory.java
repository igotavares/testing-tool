package com.ibeans.selenium.ext.wait;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.annotation.NoWaiting;
import com.ibeans.selenium.ext.annotation.Wait;
import com.ibeans.selenium.ext.execption.SeleniumException;
import com.google.common.base.Predicate;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class WebWaitFactory {

    private WebWaitFactory() {}

    public static WebWait create(Predicate<WebDriver>[] unit) {
        return new WebWait(25L, new Predicate[]{new NoWaiting()}, unit);
    }

    public static WebWait create() {
        return new WebWait(25L, new Predicate[]{new NoWaiting()}, new Predicate[]{new NoWaiting()});
    }

    public static WebWait create(Object page) {
        return Optional.ofNullable(page)
                .map(Object::getClass)
                .map(WebWaitFactory::create)
                .orElseGet(() -> create());
    }

    public static WebWait create(Class<?> pageClass) {
        if (pageClass != null) {
            Wait wait = pageClass.getAnnotation(Wait.class);
            return create(wait);
        }
        return create();
    }

    public static WebWait create(Wait wait) {
        if (wait != null) {
            Predicate<WebDriver>[] preUnit = instanceUnits(wait.preUnit());
            Predicate<WebDriver>[] unit = instanceUnits(wait.unit());
            return new WebWait(wait.timeout(), preUnit,  unit);
        }
        return create();
    }

    private static Predicate<WebDriver>[] instanceUnits(Class<? extends Predicate<WebDriver>>[] predicateClasses) {
        return Stream.of(predicateClasses)
                .map(WebWaitFactory::instanceUnit)
                .collect(Collectors.toList())
                .toArray(new Predicate[]{});
    }

    private static Predicate<WebDriver> instanceUnit(Class<? extends Predicate<WebDriver>> predicateClass) {
        if (predicateClass != null) {
            try {
                return predicateClass.newInstance();
            } catch (InstantiationException cause) {
                throw new SeleniumException("class (" + predicateClass + ") cannot be instantiated", cause);
            } catch (IllegalAccessException cause) {
                throw new SeleniumException(predicateClass + " constructor does not have access", cause);
            }
        }
        return null;
    }

}
