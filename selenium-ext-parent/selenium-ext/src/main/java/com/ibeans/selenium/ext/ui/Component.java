package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.style.Style;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import com.google.common.collect.Lists;
import org.openqa.selenium.WebElement;

import java.util.List;


public class Component {

    private static final String ID = "id";
    private static final String CLASS = "class";

    private final WebDriver driver;
    private final WebElement element;
    private final WebWait webWait;
    private final List<Style> styles;

    public Component(WebDriver driver, WebElement element, List<Style> styles, WebWait webWait) {
        this.driver = driver;
        this.element = element;
        this.styles = styles;
        this.webWait = webWait;
    }

    public Component(WebDriver driver, WebElement element, List<Style> styles) {
        this(driver, element, styles, WebWaitFactory.create());
    }

    public Component(WebDriver driver, WebElement element, WebWait webWait) {
        this(driver, element, Lists.<Style>newArrayList(), webWait);
    }

    public Component(WebDriver driver, WebElement element) {
        this(driver, element, Lists.<Style>newArrayList(), WebWaitFactory.create());
    }

    public Boolean containsValueClass(String value) {
        if (value != null) {
            String valueClass = getClassAttribute();
            return valueClass != null
                    && !valueClass.isEmpty()
                    && valueClass.contains(value);
        }
        return Boolean.FALSE;
    }

    public String getId() {
        return element.getAttribute(ID);
    }

    public String getClassAttribute() {
        return element.getAttribute(CLASS);
    }

    protected boolean hasStyles() {
        return !styles.isEmpty();
    }

    protected List<Style> getStyles() {
        return this.styles;
    }

    protected WebDriver getDriver() {
        return driver;
    }

    protected WebElement getElement() {
        return element;
    }

    public WebWait getWebWait() {
        return webWait;
    }
}
