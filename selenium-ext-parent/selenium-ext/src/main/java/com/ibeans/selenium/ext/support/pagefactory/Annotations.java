package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.annotation.TypeInfo;
import org.openqa.selenium.By;


public class Annotations {

    protected By toBy(TypeInfo typeInfo) {
        if (!typeInfo.className().isEmpty())
            return By.className(typeInfo.className());

        if (!typeInfo.css().isEmpty())
            return By.cssSelector(typeInfo.css());

        if (!"".equals(typeInfo.id()))
            return By.id(typeInfo.id());

        if (!"".equals(typeInfo.linkText()))
            return By.linkText(typeInfo.linkText());

        if (!"".equals(typeInfo.name()))
            return By.name(typeInfo.name());

        if (!"".equals(typeInfo.partialLinkText()))
            return By.partialLinkText(typeInfo.partialLinkText());

        if (!"".equals(typeInfo.tagName()))
            return By.tagName(typeInfo.tagName());

        if (!"".equals(typeInfo.xpath()))
            return By.xpath(typeInfo.xpath());

        return null;
    }

}
