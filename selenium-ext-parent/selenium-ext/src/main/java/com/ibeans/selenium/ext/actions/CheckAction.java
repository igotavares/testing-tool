package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class CheckAction extends BaseAction {

    private static final String SET_CHECKED = "arguments[0].setAttribute('checked', '');";

    public CheckAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }


    @Override
    public void perform() {
        highlight();
    }

    private void highlight() {
        getDriver().executeScript(SET_CHECKED, getElement());
    }

}
