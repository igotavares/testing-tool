package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.annotation.Sleep;
import com.ibeans.selenium.ext.ui.Button;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.Optional;


public class DefaultButtonFactory extends DefaultElementLocatorFactory<Button, WebElement> {

    private static final Long NO_SLEEP = 0L;

    public DefaultButtonFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Button.class.isAssignableFrom(field.getType());
    }

    @Override
    public Button create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, elementLocator, getSleepTime(field));
    }

    @Override
    public Button create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, elementLocator, getSleepTime(field));
    }

    protected Button create(WebDriver webDriver, Object page, ElementLocator elementLocator, long sleepTime) {
        WebElement element = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        WebWait webWait = WebWaitFactory.create(page);

        return new Button(webDriver, element, webWait, sleepTime);
    }

    private long getSleepTime(Field field) {
        return Optional.of(Sleep.class)
                .map(field::getAnnotation)
                .map(Sleep::value)
                .orElse(NO_SLEEP);
    }

}
