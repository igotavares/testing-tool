package com.ibeans.selenium.ext.ui;


public interface HasValue {

    String getValue();

    void setValue(String value);

}
