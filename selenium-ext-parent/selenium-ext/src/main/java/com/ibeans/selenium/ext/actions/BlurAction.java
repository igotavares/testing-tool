package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class BlurAction extends BaseAction {

    private static final String BLUR_SCRIPT = "arguments[0].blur()";

    public BlurAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
       onFocus();
    }

    public void onFocus() {
        getDriver().executeScript(BLUR_SCRIPT, getElement());
    }
}
