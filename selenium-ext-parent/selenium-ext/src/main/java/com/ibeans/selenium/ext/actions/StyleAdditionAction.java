package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.style.Style;
import org.openqa.selenium.WebElement;

import java.util.Collection;


public class StyleAdditionAction extends BaseAction {

    private final Collection<WebElement> elements;
    private final Collection<Style> styles;
    private final long timeout;

    public StyleAdditionAction(WebDriver driver, Collection<WebElement> elements, Collection<Style> styles, long timeout) {
        super(driver);
        this.elements = elements;
        this.styles = styles;
        this.timeout = timeout;
    }

    @Override
    public void perform() {
        elements.stream().forEach(
                element -> styles.stream().forEach(style -> {
                    new WaitElementVisibilityAction(getDriver(), element, timeout).perform();
                    getDriver().executeScript(style.toScript(), element);
                }));
    }

}
