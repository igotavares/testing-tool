package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.text.MessageFormat;
import java.util.List;


public class WaitValueOptionPresentAction extends BaseWaitAction {

    private static final String VALUE_XPATH = ".//option[@value = {0}]";
    private String value;

    public WaitValueOptionPresentAction(final WebDriver driver, final WebElement element,
                                        final String value, final long timeout) {
        super(driver, element, timeout);
        this.value = value;
    }

    @Override
    public void perform() {
        waitValuePresent();
    }

    private void waitValuePresent() {
        getDriverWait()
                .until(new ExpectedCondition<List<WebElement>>() {
                    @Override
                    public List<WebElement> apply(org.openqa.selenium.WebDriver driver) {
                        By by = By.xpath(MessageFormat.format(VALUE_XPATH, value));
                        List<WebElement> elements = getElement().findElements(by);
                        return elements.size() > 0 ? elements : null;
                    }

                    @Override
                    public String toString() {
                        return "value present of " + getElement();
                    }
                });
    }
}
