package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;


public class EditText extends Component implements HasValue {

    private static final String VALUE = "value";

    public EditText(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    public EditText(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

    public void setValue(String value) {
        if (value != null) {
            new Actions(getDriver(), getElement())
                    .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                    .onFocus()
                    .waitElemenVisibility(getWebWait().getTimeout())
                    .highlight()
                    .clear()
                    .sendKeys(value)
                    .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout())
                    .perform();
        }
    }

    public void clear() {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .onFocus()
                .highlight()
                .clear()
                .perform();
    }

    @Override
    public String getValue() {
        new Actions(getDriver(), getElement())
                .waitConditions(getWebWait().preUnit(), getWebWait().getTimeout())
                .onFocus()
                .waitElemenVisibility(getWebWait().getTimeout())
                .highlight()
                .perform();

        return getElement()
                .getAttribute(VALUE);
    }

}
