package com.ibeans.selenium.ext.ui;

import java.util.List;


public class MessageErrorException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7928599402195699070L;
	private List<String> messages;

    public MessageErrorException(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }

    @Override
    public String getMessage() {
        return messages.toString();
    }
}
