package com.ibeans.selenium.ext.actions;

import org.openqa.selenium.interactions.Action;


public class SleepAction implements Action {

    private final long time;

    public SleepAction(long time) {
        this.time = time;
    }

    @Override
    public void perform() {
        try {
            Thread.sleep(time);
        } catch (InterruptedException cause) {

        }
    }
}
