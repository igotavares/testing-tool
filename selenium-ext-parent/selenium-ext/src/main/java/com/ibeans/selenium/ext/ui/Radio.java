package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.WebElement;


public class Radio extends Component {

    private static final String VALUE_ATTRIBUTE = "value";

    public Radio(WebDriver driver, WebElement element, WebWait webWait) {
        super(driver, element, webWait);
    }

    public void check() {
        new Actions(getDriver(), getElement())
                .waitElemenClickable(getWebWait().getTimeout())
                .onFocus()
                .click()
                .waitConditions(getWebWait().postUnit(), getWebWait().getTimeout())
                .perform();
    }

    public boolean isChecked() {
        return getElement().isSelected();
    }

    public boolean isValue(String expected) {
        String value = getValue();
        return value != null
                && value.equalsIgnoreCase(expected);
    }

    public String getValue() {
        return getElement().getAttribute(VALUE_ATTRIBUTE);
    }

}
