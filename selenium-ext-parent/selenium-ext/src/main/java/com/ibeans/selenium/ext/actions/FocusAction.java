package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class FocusAction extends BaseAction {

    private static final String FOCUS_SCRIPT = "arguments[0].focus()";

    public FocusAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        onFocus();
    }

    public void onFocus() {
        getDriver().executeScript(FOCUS_SCRIPT, getElement());
    }

}
