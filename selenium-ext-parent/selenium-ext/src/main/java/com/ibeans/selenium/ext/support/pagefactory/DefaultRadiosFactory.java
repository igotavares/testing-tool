package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.Radios;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.List;


public class DefaultRadiosFactory extends DefaultElementLocatorFactory<Radios, List<WebElement>> {

    public DefaultRadiosFactory() {
        super(new DefaultElementsDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Radios.class.isAssignableFrom(field.getType());
    }

    @Override
    public Radios create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, elementLocator);
    }

    @Override
    public Radios create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, elementLocator);
    }

    public Radios create(WebDriver webDriver, Object page, ElementLocator elementLocator) {
        List<WebElement> elements = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        WebWait webWait = WebWaitFactory.create(page);

        return new Radios(webDriver, elements, webWait);
    }

}
