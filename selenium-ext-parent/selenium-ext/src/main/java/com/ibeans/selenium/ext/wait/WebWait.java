package com.ibeans.selenium.ext.wait;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import java.util.List;


public class WebWait {

    private Long timeout;
    private Predicate<WebDriver>[] preUnit;
    private Predicate<WebDriver>[] unit;

    public WebWait(Long timeout, Predicate<WebDriver>[] preUnit,  Predicate<WebDriver>[] unit) {
        this.timeout = timeout;
        this.preUnit = preUnit;
        this.unit = unit;
    }

    public Long getTimeout() {
        return timeout;
    }

    public Predicate<WebDriver>[] preUnit() {
        List<Predicate<WebDriver>> preUnit = Lists.newArrayList();

        for (Predicate<WebDriver> unit : this.preUnit) {
            preUnit.add(unit);
        }

        for (Predicate<WebDriver> unit : this.unit) {
            preUnit.add(unit);
        }
        return preUnit.toArray(new Predicate[]{});
    }

    public Predicate<WebDriver>[] postUnit() {
        return this.unit;
    }

}
