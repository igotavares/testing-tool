package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;


public interface Error{

    void apply(WebDriver driver);

}
