package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.actions.Actions;
import com.ibeans.selenium.ext.wait.WebWait;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

import java.util.List;


public class Radios {

    private static final String VALUE_ATTRIBUTE = "value";
    private static final String TYPE_ATTRIBUTE = "type";
    private static final String TYPE_RADIO = "radio";

    private final WebDriver driver;
    private final List<WebElement> radios;
    private final WebWait webWait;

    public Radios(WebDriver driver, List<WebElement> radios, WebWait webWait) {
        this.driver = driver;
        this.radios = radios;
        this.webWait = webWait;
    }

    public void selectByValue(String value) {
        validate(radios);
        for (WebElement radio : radios) {
            String valueRadio = radio.getAttribute(VALUE_ATTRIBUTE);
            if (valueRadio != null && valueRadio.equalsIgnoreCase(value)) {
                new Actions(driver, radio)
                        .click()
                        .waitConditions(webWait.postUnit(), webWait.getTimeout())
                        .perform();
                return;
            }
        }
        throw new NoSuchElementException("Cannot locate radio with value: " + value);
    }

    protected void validate(List<WebElement> radios) {
        for (WebElement radio : radios) {
            String attribute = radio.getAttribute(TYPE_ATTRIBUTE);
            if (attribute == null || !TYPE_RADIO.equals(attribute.toLowerCase())) {
                throw new UnexpectedTagNameException(TYPE_RADIO, attribute);
            }
        }
    }

    public String getSelectedValue() {
        for (WebElement radio : radios) {
            if (radio.isSelected()) {
                return radio.getAttribute(VALUE_ATTRIBUTE);
            }
        }
        return null;
    }

}
