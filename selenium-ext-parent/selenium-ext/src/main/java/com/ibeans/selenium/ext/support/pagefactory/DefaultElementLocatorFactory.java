package com.ibeans.selenium.ext.support.pagefactory;


public abstract class DefaultElementLocatorFactory<E, D> implements ElementLocatorFactory<E> {

    protected final ElementDecorator<D> fieldDecorator;

    public DefaultElementLocatorFactory(ElementDecorator<D> fieldDecorator) {
        this.fieldDecorator = fieldDecorator;
    }

}
