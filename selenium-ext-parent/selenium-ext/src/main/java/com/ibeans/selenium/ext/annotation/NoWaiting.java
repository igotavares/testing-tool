package com.ibeans.selenium.ext.annotation;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;


public class NoWaiting implements Predicate<WebDriver> {

    @Override
    public boolean apply(WebDriver input) {
        return true;
    }

}
