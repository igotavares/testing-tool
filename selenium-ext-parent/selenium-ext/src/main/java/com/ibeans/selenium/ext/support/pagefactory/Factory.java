package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.SearchContext;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class Factory {

    private static Factory factory;
    private List<ElementLocatorFactory> factories;

    private Factory() {
        this.factories = new ArrayList<>();
        factories.add(new DefaultWebElementFactory());
        factories.add(new DefaultWebElementsFactory());
        factories.add(new DefaultEditTextFactory());
        factories.add(new DefaultButtonFactory());
        factories.add(new DefaultCheckBoxFactory());
        factories.add(new DefaultDatepickerFactory());
        factories.add(new DefaultTextViewFactory());
        factories.add(new DefaultSelectFactory());
        factories.add(new DefaultRadiosFactory());
        factories.add(new DefaultTableFactory());
        factories.add(new DefaultAlertFactory());
        factories.add(new DefaultRadioFactory());
        factories.add(new DefaultHasValueFactory());
        factories.add(new DefaultMenuFactory());
        factories.add(new DefaultListViewFactory());
        factories.add(new DefaultTextViewsFactory());
    }

    public static Factory getInstance() {
        if (factory == null) {
            factory = new Factory();
        }
        return factory;
    }

    public Object create(WebDriver webDriver, Object page, Field field) {
        return getFactory(field)
                .map(factory -> factory.create(webDriver, page, field))
                .orElse(null);
    }

    public Object create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        return getFactory(field)
                .map(factory -> factory.create(webDriver, page, field, searchContext))
                .orElse(null);
    }

    protected Optional<ElementLocatorFactory> getFactory(Field field) {
        return factories.stream()
                .filter(factory -> factory.test(field))
                .findFirst();
    }
}
