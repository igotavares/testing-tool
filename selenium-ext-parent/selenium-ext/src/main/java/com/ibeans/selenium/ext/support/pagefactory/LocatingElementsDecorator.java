package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.ui.TextView;

import java.lang.reflect.Proxy;
import java.util.List;


public class LocatingElementsDecorator {

    public List<TextView> decorate(ClassLoader loader, LocatingElementsHandler<?> handler) {
        return (List<TextView>) Proxy.newProxyInstance(
                loader, new Class[] {List.class}, handler);
    }

}
