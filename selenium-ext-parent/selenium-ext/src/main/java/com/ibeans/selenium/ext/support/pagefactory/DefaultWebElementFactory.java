package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;


public class DefaultWebElementFactory extends DefaultElementLocatorFactory<WebElement, WebElement> {

    public DefaultWebElementFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return WebElement.class.isAssignableFrom(field.getType());
    }

    @Override
    public WebElement create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(page, elementLocator);
    }

    @Override
    public WebElement create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(page, elementLocator);
    }

    protected WebElement create(Object page, ElementLocator elementLocator) {
        return fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);
    }

}
