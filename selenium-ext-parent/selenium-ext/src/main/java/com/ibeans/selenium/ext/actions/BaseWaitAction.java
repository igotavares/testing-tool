package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class BaseWaitAction extends BaseAction {

    private final WebDriverWait driverWait;

    protected BaseWaitAction(WebDriver driver, long timeout) {
        this(driver, null, new WebDriverWait(driver, timeout));
    }

    protected BaseWaitAction(WebDriver driver, WebElement element, long timeout) {
        this(driver, element, new WebDriverWait(driver.getDriver(), timeout));
    }

    protected BaseWaitAction(WebDriver driver, WebElement element, WebDriverWait driverWait) {
        super(driver, element);
        this.driverWait = driverWait;
    }

    public WebDriverWait getDriverWait() {
        return driverWait;
    }
}
