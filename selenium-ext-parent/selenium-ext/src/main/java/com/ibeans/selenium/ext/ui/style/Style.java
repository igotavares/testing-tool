package com.ibeans.selenium.ext.ui.style;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Getter
public class Style {

    private static final String SCRIPT_FORMAT = "%s.style.%s='%s';";

    private final String target;
    private final String name;
    private final String value;

    public String toScript() {
        return String.format(SCRIPT_FORMAT, target, name, value);
    }

}
