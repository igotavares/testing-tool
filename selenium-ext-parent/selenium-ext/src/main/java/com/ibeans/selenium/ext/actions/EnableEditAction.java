package com.ibeans.selenium.ext.actions;

import com.ibeans.selenium.ext.WebDriver;
import org.openqa.selenium.WebElement;


public class EnableEditAction extends BaseAction {

    private static final String REMOVE_READ_ONLY = "arguments[0].removeAttribute('readOnly');";
    private static final String READ_ONLY = "readOnly";

    public EnableEditAction(WebDriver driver, WebElement element) {
        super(driver, element);
    }

    @Override
    public void perform() {
        enableEdit();
    }

    public boolean isReadOnly() {
        return getElement().getAttribute(READ_ONLY) != null;
    }

    private void enableEdit() {
        getDriver().executeScript(REMOVE_READ_ONLY, getElement());
    }

}
