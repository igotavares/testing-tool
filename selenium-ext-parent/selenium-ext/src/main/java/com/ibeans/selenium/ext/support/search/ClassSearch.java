package com.ibeans.selenium.ext.support.search;


public class ClassSearch extends TagSearch {

    private static final String CLASS_NAME = "class";

    public ClassSearch() {
        super(CLASS_NAME);
    }

}
