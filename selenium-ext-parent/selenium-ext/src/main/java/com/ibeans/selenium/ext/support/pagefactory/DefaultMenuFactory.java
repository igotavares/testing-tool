package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.ui.Menu;
import com.ibeans.selenium.ext.ui.style.Style;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.List;


public class DefaultMenuFactory extends DefaultElementLocatorFactory<Menu, List<WebElement>> {

    public DefaultMenuFactory() {
        super(new DefaultElementsDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Menu.class.isAssignableFrom(field.getType());
    }

    @Override
    public Menu create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, field, elementLocator);
    }

    @Override
    public Menu create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, field, elementLocator);
    }

    public Menu create(WebDriver webDriver, Object page, Field field, ElementLocator elementLocator) {
        List<WebElement> elements = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        WebWait webWait = WebWaitFactory.create(page);

        List<Style> styles = StyleFactory.cretea(field);

        return new Menu(webDriver, elements, styles, webWait);
    }

}
