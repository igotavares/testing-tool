package com.ibeans.selenium.ext.support.pagefactory;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.annotation.WaitValue;
import com.ibeans.selenium.ext.ui.Select;
import com.ibeans.selenium.ext.wait.WebWait;
import com.ibeans.selenium.ext.wait.WebWaitFactory;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;


public class DefaultSelectFactory extends DefaultElementLocatorFactory<Select, WebElement> {

    public DefaultSelectFactory() {
        super(new DefaultElementDecorator());
    }

    @Override
    public boolean test(Field field) {
        return Select.class.isAssignableFrom(field.getType());
    }

    @Override
    public Select create(WebDriver webDriver, Object page, Field field) {
        ElementLocator elementLocator = new DefaultElementLocator(webDriver, field);

        return create(webDriver, page, field, elementLocator);
    }

    @Override
    public Select create(WebDriver webDriver, Object page, Field field, SearchContext searchContext) {
        ElementLocator elementLocator = new DefaultElementLocator(searchContext, field);

        return create(webDriver, page, field, elementLocator);
    }

    protected Select create(WebDriver webDriver, Object page, Field field, ElementLocator elementLocator) {
        WebElement element = fieldDecorator
                .decorate(page.getClass().getClassLoader(), elementLocator);

        WebWait webWait = WebWaitFactory.create(page);

        return new Select(webDriver, element, webWait, isWaitValue(field));
    }

    protected boolean isWaitValue(Field field) {
        WaitValue dateFormat = field.getAnnotation(WaitValue.class);
        return dateFormat != null;
    }

}
