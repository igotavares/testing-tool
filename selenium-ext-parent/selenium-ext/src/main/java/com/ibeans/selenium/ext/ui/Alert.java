package com.ibeans.selenium.ext.ui;

import com.ibeans.selenium.ext.WebDriver;
import com.ibeans.selenium.ext.wait.WebWait;


public class Alert {

    private WebDriver driver;
    private WebWait webWait;

    public Alert(WebDriver driver, WebWait webWait) {
        this.driver = driver;
        this.webWait = webWait;
    }

    public void accept() {
        driver.switchTo()
                .alert()
                .accept();
    }


}
