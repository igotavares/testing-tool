package com.ibeans.selenium.ext.annotation;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Wait {

    Class<? extends Predicate<WebDriver>>[] preUnit() default NoWaiting.class;

    Class<? extends Predicate<WebDriver>>[] unit() default NoWaiting.class;

    long timeout() default 25;

}
