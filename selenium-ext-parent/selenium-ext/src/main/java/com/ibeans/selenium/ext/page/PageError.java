package com.ibeans.selenium.ext.page;

import com.ibeans.selenium.ext.WebDriver;
import com.google.common.base.Predicate;


public class PageError implements Predicate<WebDriver> {
    
    interface PageErrorConstants {
        String SERVER_ERROR_STRING = "server error";
        String NETWORK_ERROR_STRING = "network error";
        String EXCEPTION_COLON_STRING = "exception:";
        String EXCEPTION_WITHOUT_TRIM_STRING = " exception ";
        String MEMORY_EXHAUSTED_STRING = "memory exhausted";
        String ERRO_EXCLAMATION_STRING = "erro!";
        String CONTACT_SUPPORT_STRING = "contact support";
        String ERROR_500_STRING = "error 500";
    }
    
    @Override
    public boolean apply(WebDriver driver) {
        return (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.SERVER_ERROR_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.NETWORK_ERROR_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.EXCEPTION_COLON_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.EXCEPTION_WITHOUT_TRIM_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.MEMORY_EXHAUSTED_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.ERRO_EXCLAMATION_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.CONTACT_SUPPORT_STRING))
                || (driver.getPageSource().toLowerCase()
                .contains(PageErrorConstants.ERROR_500_STRING));
    }
    
}
